import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from './modules/common/common.module';
import { LoginComponent } from './modules/common/login/login.component';
import { HomeComponent } from './modules/common/home/home.component';
import { AuthGuard } from './helpers/auth.guard';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CategoryListComponent } from './modules/category/category-list/category-list.component';
import {AuthenticationComponent} from './modules/common/authentication/authentication.component';
import {ResetComponent} from './modules/common/reset/reset.component';

const routes: Routes = [
  { path: '', component: HomeComponent, canActivate: [AuthGuard] },
  { path : 'login', component : LoginComponent },
  { path : 'authentication-login', component : AuthenticationComponent },
  { path : 'reset-password', component : ResetComponent },
  // { path: '**', redirectTo: '' },
  // { path: 'food-category', component: CategoryListComponent},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
