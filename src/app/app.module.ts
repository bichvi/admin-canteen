import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HeaderComponent } from './modules/common/header/header.component';
import { FooterComponent } from './modules/common/footer/footer.component';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './modules/common/login/login.component';
import { UserModule } from './modules/user/user.module';
import { AuthenticationService } from './services';
import { UserService } from './services';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthGuard } from './helpers';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptor } from './helpers';
import { ErrorInterceptor } from './helpers';
import { CategoryModule } from './modules/category/category.module';
import { FoodModule } from './modules/food/food.module';
import { CategoryService } from './services/category.service';
import { FoodService } from './services/food.service';
import { HttpModule } from '@angular/http';
import { FormlyModule } from '@ngx-formly/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { EventService } from './services/event.service';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { CoreModule } from './core/core.module';
import { FoodTypeModule } from './modules/food-type/food-type.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import {BankModule} from './modules/bank/bank.module';
import {CardModule} from './modules/card/card.module';
import {PostModule} from './modules/post/post.module';
import {RoleModule} from './modules/role/role.module';
import {DashboardModule} from './modules/dashboard/dashboard.module';
import {MaterialModule} from './modules/material/material.module';
import {MaterialTypeModule} from './modules/material-type/material-type.module';
import {OrderModule} from './modules/order/order.module';
import {AuthenticationComponent} from './modules/common/authentication/authentication.component';
import {ResetComponent} from './modules/common/reset/reset.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    AuthenticationComponent,
    ResetComponent
  ],
  imports: [
    NgbModule.forRoot(),
    BrowserModule,
    AppRoutingModule,
    CommonModule,
    UserModule,
    DashboardModule,
    CategoryModule,
    FoodModule,
    FoodTypeModule,
    MaterialModule,
    MaterialTypeModule,
    RoleModule,
    BankModule,
    OrderModule,
    PostModule,
    CardModule,
    CoreModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    HttpModule,
    FormlyModule.forRoot({
      validationMessages: [
        { name: 'required', message: 'Trường này là bắt buộc' },
      ],
    }),
    ConfirmationPopoverModule.forRoot(),
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot() // ToastrModule added
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    UserService, AuthenticationService, CategoryService, FoodService, EventService, AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
