export class TemplateOptionsConstants {
  public static userStatusData = [
    {label: 'Kích hoạt', value: 1},
    {label: 'Không kích hoạt', value: 0},
  ];
  public static bankType = [
    {label: 'Debit card', value: 1},
    {label: 'Credit card', value: 2},
  ];
}
