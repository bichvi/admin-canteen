import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CPipeModule } from './pipes/c-pipe.module';
import { UiModule } from './ui/ui.module';

import { PipeService } from './services/pipe.service';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    CPipeModule,
    UiModule,
  ],
  exports: [
    CPipeModule,
    UiModule,
  ],
  providers: [
    PipeService
  ],
  entryComponents: [],
  declarations: [
  ]
})
export class CoreModule {
//   static forRoot(): ModuleWithProviders {
//     return {
//       ngModule: CoreModule,
//       providers: [AppService, LocalStorageService]
//     };
//   }
}
