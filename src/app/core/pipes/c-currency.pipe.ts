import { Pipe, PipeTransform } from '@angular/core';
import { PipeConfig } from './pipe-config';
import { formatNumber } from './pipe-helper';

/**
 * Pipe Currency format
 */
@Pipe({name: 'cCurrency'})
export class CCurrencyPipe implements PipeTransform {
  public transform(input?: any): string {
    if (isNaN(input)) {
      return input;
    }
    input = +input;

    let myCurrencyFormat = PipeConfig.currencyFormat || '#,###.##';
    const currencyRegex = /(\#{1,2})([\s,.]?)(\#*)([\s,.]?)(\#*)/;
    let search, lengthSection, lengthDecimal, sectionsDelimiter, decimaDelimiter;

    myCurrencyFormat = currencyRegex.test(myCurrencyFormat) ? myCurrencyFormat : '#,###.##';
    search = currencyRegex.exec(myCurrencyFormat);
    if (search[2].length === 0 && search[4].length) {
      lengthSection = 0;
      sectionsDelimiter = '';
      lengthDecimal = search[5].length;
      decimaDelimiter = search[4];
    }else if (search[2].length === 0 && search[4].length === 0) {
      lengthSection = 3;
      sectionsDelimiter = ' ';
      lengthDecimal = 0;
      decimaDelimiter = '';
    } else {
      lengthSection = 3;
      lengthDecimal = search[5].length;
      sectionsDelimiter = search[2];
      decimaDelimiter = search[4];
    }

    input = formatNumber(input, lengthDecimal, lengthSection, sectionsDelimiter, decimaDelimiter, true);

    const prefix = PipeConfig.currencyPrefix ? PipeConfig.currencyPrefix + ' ' : '',
      postfix = PipeConfig.currencyPostfix ? ' ' + PipeConfig.currencyPostfix : '';

    return prefix + (input ? input : 0) + postfix;
  }
}
