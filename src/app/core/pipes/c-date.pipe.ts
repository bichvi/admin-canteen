import { Pipe, PipeTransform } from '@angular/core';
import { PipeConfig } from './pipe-config';
import { CMDatePipe } from './c-mdate.pipe';

/**
 * Pipe Date format
 */
@Pipe({name: 'cDate'})
export class CDatePipe implements PipeTransform {
  transform(input: string, format: string = PipeConfig.dateFormat, supportTimezone: boolean = false): string {

    if (!input) {
      return PipeConfig.emptyInput;
    }
    if (input === '0001-01-01T00:00:00Z') {
      return PipeConfig.notAvailable;
    }
    console.log(input);


    // if (!supportTimezone && input.match(/[\+|-]\d{2}:\d{2}$/) != null) {
    //   input = input.replace(/[\+|-]\d{2}:\d{2}$/, '');
    // }
    // var output = new DatePipe('en-Gb').transform(input, format);
    const output = new CMDatePipe().transform(input, format);

    return (output !== null) ? output : PipeConfig.notAvailable;
  }
}
