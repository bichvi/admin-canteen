import { Pipe, PipeTransform } from '@angular/core';
import { PipeConfig } from './pipe-config';
import { CMDatePipe } from './c-mdate.pipe';

/**
 * Pipe DateTime format
 */
@Pipe({name: 'cDateTime'})
export class CDateTimePipe implements PipeTransform {
  transform(input: string, format: string, supportTimezone: boolean = false): string {
    if (input === '0001-01-01T00:00:00Z') {
      return PipeConfig.notAvailable;
    }
    // if (!supportTimezone && input.match(/[\+|-]\d{2}:\d{2}$/) != null) {
    //   input = input.replace(/[\+|-]\d{2}:\d{2}$/, '');
    // }

    format = format ? format : PipeConfig.dateTimeFormat;
    // var output = new DatePipe('en-Gb').transform(input, format);
    const output = new CMDatePipe().transform(input, format);
    return output ? output : PipeConfig.notAvailable;
  }
}
