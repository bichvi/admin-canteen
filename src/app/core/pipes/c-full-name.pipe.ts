import { Pipe, PipeTransform } from '@angular/core';
import { PipeConfig } from './pipe-config';

@Pipe({name: 'cFullName'})
export class CFullNamePipe implements PipeTransform {
  transform(input: string): string {
    return input['firstName'] ? input['firstName'] + ' ' + input['lastName'] :
      (input['FirstName'] ? input['FirstName'] + ' ' + input['LastName'] : PipeConfig.notAvailable);
  }
}
