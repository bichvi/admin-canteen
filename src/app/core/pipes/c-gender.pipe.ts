import { Pipe, PipeTransform } from '@angular/core';
import { PipeConfig } from './pipe-config';

@Pipe({name: 'cGender'})
export class CGenderPipe implements PipeTransform {
  genderObj = {
    'female': 'Female',
    'male': 'Male'
  };

  transform(input: string): string {
    return input ? this.genderObj[input] : PipeConfig.notAvailable;
  }
}
