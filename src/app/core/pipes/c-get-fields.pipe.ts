import { Pipe, PipeTransform } from '@angular/core';
import { PipeConfig } from './pipe-config';
import * as _ from 'underscore';

@Pipe({name: 'cGetFields'})
export class CGetFieldsPipe implements PipeTransform {
  transform(input: string, field: string): any {
    console.log(input);
    if (input) {
      const arrStr = _.pluck(input, field);
      return arrStr.length ? arrStr.join(', ') : PipeConfig.emptyInput;
    }
    return PipeConfig.emptyInput;
  }
}
