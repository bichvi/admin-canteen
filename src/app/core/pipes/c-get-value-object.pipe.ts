import { Pipe, PipeTransform } from '@angular/core';
import { getObjectData, parseBoolean } from './pipe-helper';
import * as _ from 'underscore';

@Pipe({name: 'cGetValueObject'})
export class CGetValueObjectPipe implements PipeTransform {
  transform(ob: any, key, isSplit): string {
    key = typeof key === 'undefined' ? 'name' : key;
    isSplit = parseBoolean(isSplit);

    let _object = null, value = null;

    if (Array.isArray(ob)) {
      return null;
    }
    if (_.isObject(ob)) {
      _object = ob;
    } else {
      try {
        _object = JSON.parse(ob);
      } catch (ex) {
        console.warn(ex.message);
        _object = null;
      }
    }

    if (_object !== null) {
      value = getObjectData(_object, key, isSplit);
    }

    return value;
  }
}
