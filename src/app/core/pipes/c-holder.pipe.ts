import {Pipe, PipeTransform} from '@angular/core';
import * as cPipe from './pipes';
import {PipeService} from '../services/pipe.service';

@Pipe({
  name: 'cHolder'
})
export class CHolderPipe implements PipeTransform {
  transform(input?: any, pipeName?: string): any {
    if (pipeName) {
      try {
        if (pipeName === 'translate') {
          pipeName = 'cTranslate';
        }
        const params = pipeName.replace(/ |\"|\'/gi, '').split(':');
        if (params.length) {
          pipeName = params[0];
          params.shift();
          params.join();
        }
        pipeName = pipeName.charAt(0).toUpperCase() + pipeName.slice(1) + 'Pipe';
        if (cPipe[pipeName]) {
          input = params.length ? new cPipe[pipeName]().transform(input, ...params) : new cPipe[pipeName]().transform(input);
        } else {
          input = params.length ? new PipeService.pipes[pipeName]().transform(input, ...params) :
            new PipeService.pipes[pipeName]().transform(input);
        }

      } catch (err) {
        console.warn(pipeName, 'Pipe name class not exist');
      }
    }
    return input;
  }
}
