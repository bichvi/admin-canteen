import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'cHtmlToText'})
export class CHtmlToTextPipe implements PipeTransform {
  transform(text: string): string {
    return text ? String(text).replace(/<[^>]+>/gm, '') : '';
  }
}
