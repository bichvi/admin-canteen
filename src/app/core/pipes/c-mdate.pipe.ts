import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

/**
 * Pipe Date format using moment
 */
@Pipe({name: 'cMDate'})
export class CMDatePipe implements PipeTransform {
  transform(value: string, format: string = ''): string {
    if (!value || value === '') {
      return '';
    }
    return moment(value).format(format);
  }
}
