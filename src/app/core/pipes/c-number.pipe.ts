import { Pipe, PipeTransform } from '@angular/core';
import { PipeConfig } from './pipe-config';
import { formatNumber } from './pipe-helper';

/**
 * Pipe Number format
 */
@Pipe({name: 'cNumber'})
export class CNumberPipe implements PipeTransform {
  transform(input?: number): any {
    if (input === 0) {
      return input;
    }
    if (!input) {
      return 0;
    }
    input = Number(input);
    let numberFt = PipeConfig.numberFormat || '#,###.##';
    const numberRegex = /(\#*)([,.\s]?)(\#*)([,.\s]?)(\#*)/;
    let search, lengthSection;

    numberFt = numberRegex.test(numberFt) ? numberFt : '#,###.##';
    numberFt = numberFt.split('').reverse().join('');

    // search[1] => length decimal
    // search[3] => length section
    // search[4] => section separate
    // search[2] => decimal separate
    search = numberRegex.exec(numberFt);

    lengthSection = (search[4].length > 0 && search[2].length > 0) ?
      search[3].length : input.toString().split('.')[0].length;
    input = (search[4].length > 0 || search[2].length > 0) ?
      formatNumber(input, search[1].length, lengthSection, search[4], search[2], false) : input;

    return input;
  }
}
