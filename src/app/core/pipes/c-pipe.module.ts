import { NgModule } from '@angular/core';
import { CTextPipe } from './c-text.pipe';
import { CTranslatePipe } from './c-translate/c-translate.pipe';
import { CHolderPipe } from './c-holder.pipe';
import { CommonModule } from '@angular/common';
import { CTranslateService } from './c-translate/c-translate.service';
import { CSafeUrlPipe } from './c-safe-url.pipe';
import { CGetFieldsPipe } from './c-get-fields.pipe';
import { CCurrencyPipe } from './c-currency.pipe';
import { CMDatePipe } from './c-mdate.pipe';
import { CDatePipe } from './c-date.pipe';
import { CDateTimePipe } from './c-datetime.pipe';
import { CSanitizeHtmlPipe } from './c-sanitize-html.pipe';
import { CNumberPipe } from './c-number.pipe';
import { CHtmlToTextPipe } from './c-html-to-text.pipe';
import { CGetValueObjectPipe } from './c-get-value-object.pipe';
import { CFullNamePipe } from './c-full-name.pipe';
import { CGenderPipe } from './c-gender.pipe';
import { CYesNoPipe } from './pipes';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [CTextPipe, CHolderPipe, CCurrencyPipe, CMDatePipe, CDatePipe, CDateTimePipe,
    CSanitizeHtmlPipe, CNumberPipe, CSafeUrlPipe, CGetFieldsPipe, CHtmlToTextPipe, CGetValueObjectPipe,
    CFullNamePipe, CGenderPipe, CTranslatePipe,
    CYesNoPipe
  ],
  providers: [CTranslateService],
  exports: [CTextPipe, CHolderPipe, CCurrencyPipe, CMDatePipe, CDatePipe, CDateTimePipe,
    CSanitizeHtmlPipe, CNumberPipe, CSafeUrlPipe, CGetFieldsPipe, CHtmlToTextPipe, CGetValueObjectPipe,
    CFullNamePipe, CGenderPipe, CTranslatePipe,
    CYesNoPipe
  ]
})
export class CPipeModule {
}
