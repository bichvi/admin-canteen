import { Pipe, PipeTransform } from '@angular/core';
import { PipeConfig } from './pipe-config';

@Pipe({name: 'cText'})
export class CTextPipe implements PipeTransform {
  transform(input: string): string {
    return input ? input : PipeConfig.notAvailable;
  }
}
