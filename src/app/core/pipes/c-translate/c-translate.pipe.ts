import { Pipe, PipeTransform } from '@angular/core';
import { CTranslateService } from './c-translate.service';

/**
 * Pipe translate
 */
@Pipe({name: 'translate'})
export class CTranslatePipe implements PipeTransform {
  constructor(private translateService: CTranslateService) {

  }

  transform(input: string): string {
    return this.translateService.translate(input);
  }
}
