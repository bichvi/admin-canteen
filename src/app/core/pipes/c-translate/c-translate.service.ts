import { Injectable } from '@angular/core';
import { LocalStorageService } from '../../services/local-storage.service';

@Injectable()
export class CTranslateService {
  langObj: any = {};

  constructor(private localStorage: LocalStorageService) {
    try {
      this.langObj = require('../../../../../i18n/' + this.getLang() + '/global.json');
    } catch (error) {
      console.error(error);
    }

    if (!this.langObj) {
      try {
        const lang = this.getLang(true);
        this.langObj = require('../../../../../i18n/' + lang + '/global.json');
        this.setLang(lang);
      } catch (error) {
        console.error(error);
      }
    }
  }

  public setLang(lang) {
    this.localStorage.setItem('lang', lang);
  }

  public getLang(defaults?: boolean) {
    if (!defaults) {
      const value = this.localStorage.getItem('lang');
      if (value && value !== null) {
        return value;
      }
    }

    return 'en-gb';
  }

  public translate(input: string): string {
    if (this.langObj[input]) {
      return this.langObj[input];
    }
    return input;
  }
}
