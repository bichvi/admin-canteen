import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'cYesNo'})
export class CYesNoPipe implements PipeTransform {
  transform(input: string): string {
    return input ? 'Yes' : 'No';
  }
}
