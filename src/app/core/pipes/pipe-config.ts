export const PipeConfig = {
  numberFormat: '#,###.##',
  dateTimeFormat: 'DD-MM-YYYY HH:mm',
  dateFormat: 'DD-MM-YYYY',
  emptyInput: 'N/A',
  currencyPrefix: '',
  currencyPostfix: '',
  currencyFormat: '##.### ',
  notAvailable: 'N/A',
};
