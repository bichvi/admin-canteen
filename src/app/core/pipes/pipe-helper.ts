import * as _ from 'underscore';

export function formatNumber(value, n, x, s, c, nd = false) {
  const re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
    num = value.toFixed(Math.max(0, ~~n));

  return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (nd ? s : s || ','));
}

export function parseBoolean(obj) {
  if (_.isBoolean(obj)) {
    return obj;
  }

  if (typeof obj === 'string') {
    switch (obj.toLowerCase()) {
      case 'true':
      case 'yes':
      case '1':
        return true;
      default:
        return false;
    }
  }

  return Boolean(obj);
}

export function getObjectData(obj, key, notSplit) {
  if (notSplit === false) {
    return obj[key];
  } else {
    let tmp = obj;
    const keys = key.split('.');
    while (keys.length > 0) {
      key = keys.shift();
      if (keys.length === 0) {
        return tmp[key];
      } else if (_.isObject(tmp[key])) {
        tmp = tmp[key];
      } else {
        break;
      }
    }
  }
}
