export { CTextPipe } from './c-text.pipe';
// export { CTranslatePipe } from './c-translate/c-translate.pipe';
export { CSafeUrlPipe } from './c-safe-url.pipe';
export { CGetFieldsPipe } from './c-get-fields.pipe';
export { CCurrencyPipe } from './c-currency.pipe';
export { CMDatePipe } from './c-mdate.pipe';
export { CDatePipe } from './c-date.pipe';
export { CDateTimePipe } from './c-datetime.pipe';
export { CSanitizeHtmlPipe } from './c-sanitize-html.pipe';
export { CNumberPipe } from './c-number.pipe';
export { CHtmlToTextPipe } from './c-html-to-text.pipe';
export { CGetValueObjectPipe } from './c-get-value-object.pipe';
export { CFullNamePipe } from './c-full-name.pipe';
export { CGenderPipe } from './c-gender.pipe';
export { CYesNoPipe } from './c-yes-no.pipe';
