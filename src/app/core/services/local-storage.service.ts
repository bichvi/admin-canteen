import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  private localStorage;
  private isMock = false;


  constructor() {
    if (typeof(Storage) !== 'undefined') {
      this.localStorage = localStorage;
    } else {
      this.isMock = true;
      this.localStorage = new Map<string, any>();
      console.warn('Local storage is not running use mock localstorage instead !!!');
    }
  }

  /**
   * Get Item
   *
   * @param key
   */
  getItem(key: string) {

    const rawData = this.isMock ? this.localStorage.get(key) : this.localStorage.getItem(key);

    return (rawData !== undefined) ? rawData : null;

  }

  /**
   * Sets an item in local storage
   * @param key The item's key
   * @param data The item's value, must NOT be null or undefined
   * @returns {boolean}
   */
  setItem(key: string, data: any) {
    if (this.isMock) {
      this.localStorage.set(key, data);
    } else {
      this.localStorage.setItem(key, data);
    }

    return true;

  }

  /**
   * Deletes an item in local storage
   * @param key The item's key
   * @returns {boolean}
   */
  removeItem(key: string) {
    if (this.isMock) {
      this.localStorage.delete(key);
    } else {
      this.localStorage.removeItem(key);
    }

    return true;

  }

  /**
   * Deletes all items from local storage
   * @returns {boolean}
   */
  clear() {

    this.localStorage.clear();

    return true;

  }

}
