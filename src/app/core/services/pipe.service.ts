import { Injectable } from '@angular/core';
import * as _ from 'underscore';

@Injectable()
export class PipeService {

  static pipes = {};

  constructor() {
  }

  /**
   * Use this function to register your pipe to core module
   * So you can use it on cTable cellPipe or on cHolder pipe
   * @param input
   */
  public static register(input) {
    if (_.isArray(input)) {
      input.forEach((pipe) => {
        if (pipe['pipeName']) {
          PipeService.pipes[pipe['pipeName']] = pipe;
        } else {
          console.warn('Cannot register pipe ' + pipe.name + '. Because missing static pipeName property.');
        }
      });
    } else {
      if (input['pipeName']) {
        PipeService.pipes[input['pipeName']] = input;
      } else {
        console.warn('Cannot register pipe ' + input.name + '. Because missing static pipeName property.');
      }
    }
  }
}
