import { Directive, Input, ElementRef, AfterViewInit } from '@angular/core';

@Directive({
  selector: '[cClass]'
})
export class CClassDirective implements AfterViewInit{
  @Input() cClass = '';

  constructor(private elementRef: ElementRef) {}

  ngAfterViewInit() {
    // console.log(this.elementRef.nativeElement.children, this.elementRef.nativeElement.children[0]);
    if (this.elementRef.nativeElement.children && this.elementRef.nativeElement.children[0]) {
      // this.elementRef.nativeElement.children[0].classList.add(this.cClass);
      // console.log( this.elementRef.nativeElement.children[0].classList);
      this.addClass(this.elementRef.nativeElement.children[0], this.cClass);
    }
  }

  addClass(element, name) {
    const arr = element.className.split(' ');
    if (arr.indexOf(name) === -1) {
      element.className += ' ' + name;
    }
  }
}
