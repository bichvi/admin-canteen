import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CClassDirective } from './c-class.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [CClassDirective],
  providers: [],
  exports: [CClassDirective]
})
export class CClassModule {
}
