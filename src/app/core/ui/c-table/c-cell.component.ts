import {
  Component, ComponentFactoryResolver,
  Input, OnInit,
  ViewContainerRef
} from '@angular/core';
import { CDynamicRowBuilder } from './c-dynamic-row-builder' ;


@Component({
  selector: 'c-cell',
  template: ''
})
export class CCellComponent implements OnInit {
  // Table values
  @Input() public row: any = {};
  @Input() public column: any = {};
  @Input() public extraData: any = {};

  constructor(private resolver: ComponentFactoryResolver, private vcRef: ViewContainerRef, private builder: CDynamicRowBuilder) {
  }

  ngOnInit() {
    let factory;

    if (this.column.cellComponent) {
      factory = this.resolver.resolveComponentFactory(this.column.cellComponent);
    }

    if (this.column.cellTemplate) {
      factory = this.builder.getComponentFactory(this.column.cellTemplate);
    }

    if (factory) {
      const component = this.vcRef.createComponent(factory);
      component.instance['row'] = this.row;
      component.instance['column'] = this.column;
      component.instance['extraData'] = this.extraData;
    }
  }
}
