import { Compiler, ComponentFactory, Injectable, NgModule } from '@angular/core';
import { CPipeModule } from '../../pipes/c-pipe.module';
import { CommonModule } from '@angular/common';

@Injectable()
export class CDynamicRowBuilder {

  constructor(private compiler: Compiler) {
  }

  // this object is singleton - so we can use this as a cache
  private _cacheOfFactories: { [id: string]: ComponentFactory<any> } = {};

  public getComponentFactory(id: any) {
    return this._cacheOfFactories[id];
  }

  public createComponentFactory(componentType: any, ids: Array<string>, dLength: number = 0): Promise<any> {
    const module = this.createModule(componentType);

    return new Promise((resolve) => {
      try {
        this.compiler
          .compileModuleAndAllComponentsAsync(module)
          .then((moduleWithFactories) => {
            for (const i in ids) {
              this._cacheOfFactories[ids[i]] = moduleWithFactories.componentFactories[Number(dLength) + Number(i)];
            }
            resolve(true);
          });
      } catch (e) {
        console.warn(e);
        resolve(true);
      }

    });
  }

  private createModule(componentType: any) {
    @NgModule({
      imports: [CommonModule, CPipeModule],
      declarations: [
        componentType
      ]
    })
    class DynamicRowTableModule {
    }

    return DynamicRowTableModule;
  }
}
