import {
    Component, ComponentFactoryResolver,
    Input,
    ViewContainerRef
} from '@angular/core';
import { CDynamicRowBuilder } from './c-dynamic-row-builder' ;
import { CTableComponent } from './c-table.component';


@Component({
  selector: 'c-header-cell',
  template: ''
})
export class CHeaderCellComponent {
  // Table values
  @Input() public column: any = {};
  @Input() public extraData: any = {};

  constructor(private vcRef: ViewContainerRef, private builder: CDynamicRowBuilder, private cTable: CTableComponent,
              private resolver: ComponentFactoryResolver) {
  }

  ngOnInit() {
      this.cTable.waitCreateCellComponent(() => {
      let factory = this.builder.getComponentFactory(this.column.headerCellTemplate);
        if (this.column.headerCellComponent) {
          factory = this.resolver.resolveComponentFactory(this.column.headerCellComponent);
      }

      if (factory) {
        const component = this.vcRef.createComponent(factory);
        component.instance['column'] = this.column;
        component.instance['extraData'] = this.extraData;
        component.instance['cTable'] = this.cTable;
      }
    });
  }
}
