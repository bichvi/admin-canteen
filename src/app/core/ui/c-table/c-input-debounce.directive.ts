import { Directive, Input, Output, EventEmitter, ElementRef } from '@angular/core';
import { fromEvent } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { distinctUntilChanged } from 'rxjs/internal/operators';

@Directive({
  selector: '[cInputDebounce]'
})
export class CInputDebounceDirective {
  @Input() cInputDebounce = 300;
  @Output() inputKeyup: EventEmitter<any> = new EventEmitter();


  constructor(private elementRef: ElementRef) {
    const eventStream = fromEvent(elementRef.nativeElement, 'keyup').pipe(
      debounceTime(this.cInputDebounce),
      distinctUntilChanged()
    );

    eventStream.subscribe(event => this.inputKeyup.emit({
      value: elementRef.nativeElement.value,
      keyupEvent: event
    }));
  }
}
