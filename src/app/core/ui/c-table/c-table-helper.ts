export class CTableHelper {
  constructor() {
  }

  public static isRowValidOnDateRange(row, columns): boolean {
    for (const column of columns) {
      const from = column.filterDateOptions.fromDate,
        to = column.filterDateOptions.toDate,
        date = new Date(CTableHelper.getFieldValue(row, column.field));
      date.setHours(0, 0, 0, 0);
      const fieldDate = date.toISOString();

      if (from && to && (fieldDate < from.toISOString() || fieldDate > to.toISOString())) {
        return false;
      }
      if (from && fieldDate < from.toISOString()) {
        return false;
      }
      if (to && fieldDate > to.toISOString()) {
        return false;
      }
    }
    return true;
  }

  public static isRowContainSearchText(row, columns): boolean {
    for (const column of columns) {
      if (CTableHelper.getFieldValue(row, column.field).toLowerCase().indexOf(column.searchText) === -1) {
        return false;
      }
    }
    return true;
  }

  public static isRowValidFilterData(row, columns): boolean {
    for (const column of columns) {
      if (column.filterOptions.values.indexOf(CTableHelper.getFieldValue(row, column.field)) === -1) {
        return false;
      }
    }
    return true;
  }

  public static isRowContainFilterText(row, searchOptions, filterText) {
    let content = '';
    for (const i in row['entity']) {
      if (searchOptions.fields.length) {
        if (searchOptions.fields.indexOf(i) !== -1 && typeof row['entity'][i] !== 'object') {
          content += row['entity'][i] + ' ';
        }
      } else {
        if (typeof row['entity'][i] !== 'object') {
          content += row['entity'][i] + ' ';
        }
      }
    }
    return content.toLowerCase().search(filterText) !== -1;
  }

  public static getColumnsByFilter(columns, filter) {
    const tmpListFilterColumns = [];
    for (const column of columns) {
      if (filter === 'useFilter' && column.useFilter && column.filterOptions.values.length) {
        tmpListFilterColumns.push(column);
      }

      if (filter === 'useSearch' && column.useSearch && column.searchText) {
        tmpListFilterColumns.push(column);
      }

      if (filter === 'useFilterDate' && column.useFilterDate &&
        (column.filterDateOptions.from || column.filterDateOptions.to)) {
        tmpListFilterColumns.push(column);
      }
    }
    return tmpListFilterColumns;
  }

  public static toggleAllCheckbox(event, checked) {
    let parent = null,
      element = event.target;
    while (element) {
      parent = element.parentElement;
      if (parent && (' ' + element.className + ' ').indexOf(' ' + 'dropdown-menu' + ' ') > -1) {
        break;
      }
      element = parent;
    }
    if (parent) {
      const inputs = parent.getElementsByTagName('input');
      for (const input of inputs) {
        if (input.type === 'checkbox') {
          input.checked = checked;
        }
      }
    }
  }

  public static getFieldValue(row, fieldName: string) {
    const parts = fieldName.split('.');
    if (parts.length) {
      let value = row['entity'][parts[0]];
      for (const i in parts) {
        if (Number(i) === 0) {
          continue;
        }
        value = value[parts[i]];
      }
      return value;
    }
    return row['entity'][fieldName];
  }
}
