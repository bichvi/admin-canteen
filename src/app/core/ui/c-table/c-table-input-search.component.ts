import { EventEmitter, ElementRef } from '@angular/core';
import { Component, Input, Output } from '@angular/core';

@Component({
  selector: 'c-table-input-search',
  templateUrl: './c-table-input-search.html'
})
export class CTableInputSearchComponent {
  @Input() placeHolder = '';
  @Input() title = '';
  @Input() debounceTime = 500;
  @Output() valueChange: EventEmitter<any> = new EventEmitter();

  constructor(private elementRef: ElementRef) {
  }

  search(value: string) {
    this.valueChange.emit({
      value: value
    });
  }
}
