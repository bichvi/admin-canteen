import {
  AfterViewChecked,
  ChangeDetectorRef, Component,
  DoCheck, Input, IterableDiffer, IterableDiffers, OnChanges, OnInit, SimpleChanges
} from '@angular/core';
import { CTableColumnConfig, CTableConfig } from './c-table-config';
import { CTableHelper } from './c-table-helper';
import { CDynamicRowBuilder } from './c-dynamic-row-builder';
import * as _ from 'underscore';

class CTableRow {
  constructor(public entity: any, public _index: number | string,
              public _expand: boolean = false, public model: any = null, public rowClass: string = '') {
  }
}

@Component({
  selector: 'c-table',
  styleUrls: ['./c-table.component.scss'],
  templateUrl: './c-table.html'
})
export class CTableComponent implements OnInit, OnChanges, DoCheck, AfterViewChecked {
  // Table inputs
  @Input()
  public options: any = {};
  private defaultConfig: CTableConfig = {
    data: [],
    useLocalTable: true,
    useBackboneModel: false,
    tableClass: '',
    columnDefs: [],
    usePaging: true,
    pagingOptions: {
      showTotalRecords: true,
      pageSizes: [10, 20, 30],
      pageSize: 10,
      currentPage: 1
    },
    useSearch: false,
    searchOptions: {
      inputVisible: true,
      fields: [],
      placeHolderText: 'C_TABLE_SEARCH_PHRASE'
    },
    useExpandRow: false,
    expandRowOptions: {
      tableOptions: {},
      field: ''
    },
    showTotalItems: true,
    showTableFooter: true,
    extraData: {},
    rowClass: (row) => {
      if (this.options.useExpandRow && row._expand) {
        return 'active';
      }
      return 'c-row';
    },
    rowClick: (row) => {
      if (this.checkExpandRow(row)) {
        this.toggleExpandRow(row);
      }
    },
    sortClass: {
      asc: 'fa-sort-asc',
      desc: 'fa-sort-desc',
      default: 'fa-sort'
    },
    isAutoload: true
  };
  @Input()
  public data: any;
  @Input()
  public loading = false;
  public tableData = [];
  public checkAll = false;
  // TODO just fix for AOT will be remove on some next angular update
  public row;
  public _totalItems = 0;
  private _expandRows = false;
  private _page = 1;
  private _filterText = '';
  private _pageSize;
  private _totalPages = 1;
  private _initialData = [];
  private _finalData = [];
  private filterColumns: Array<CTableColumnConfig> = [];
  private useColumnSort = false;
  private useColumnFilter = false;
  private useColumnFilterDate = false;
  private useColumnSearch = false;
  private createdCellTemplates = false;
  private createdCellComponents = false;
  private defaultDateConfig = {
    format: 'yyyy-MM-dd',
    readOnly: true,
    dateOnly: true
  };

  private _trackByFn = () => {
  };

  // Check differ on data input change
  private _differ: IterableDiffer<any> = null;

  constructor(private builder: CDynamicRowBuilder, private _differs: IterableDiffers, private _cdr: ChangeDetectorRef) {
  }

  ngOnInit() {
    // mix config
    if (this.options.searchOptions) {
      this.options.searchOptions = Object.assign(this.defaultConfig.searchOptions, this.options.searchOptions);
    }

    this.options = Object.assign(this.defaultConfig, this.options);
    if (this.data) {
      this.options.data = this.data;
    }

    this.prepareTable().then((factory) => {
      if (factory) {
        this.createdCellComponents = true;
      }
      this._page = 1;
      this._pageSize = this.options.pagingOptions.pageSize;

      this.generateData(this._pageSize, this._page);
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if ('data' in changes) {
      // React on data changes only once all inputs have been initialized
      const value = changes['data'].currentValue;
      if (!this._differ && value) {
        try {
          this._differ = this._differs.find(value).create(this._trackByFn);
        } catch (e) {
          throw new Error(
            `Cannot find a differ supporting object '${value}' of your data type. 
            Input[data] only supports binding to Iterables such as Arrays.`);
        }
      }
    }
  }

  ngDoCheck() {
    if (this._differ) {
      const changes = this._differ.diff(this.data);
      if (changes && this.options.useLocalTable) {
        this.setData(this.data);
      }
    }
  }

  ngAfterViewChecked() {
    this._cdr.detectChanges();
  }

  public getFieldValue(row, fieldName: string) {
    return CTableHelper.getFieldValue(row, fieldName);
  }

  public waitCreateCellComponent(callback) {
    const wait = setInterval(() => {
      if (this.createdCellComponents) {
        callback();
        clearInterval(wait);
      }
    }, 100);
  }

  /**
   * Public api function setData
   * @param data
   * @param totalItems
   */
  public setData(data: any, totalItems?: number) {
    this.options.data = data;
    if (this.options.useLocalTable) {
      const callback = () => {
        this.prepareTable().then((factory) => {
          this.generateData(this._pageSize, this._page);
          if (this.options.afterBuildRows) {
            this.options.afterBuildRows();
          }
        });
      };

      if (!this.createdCellComponents) {
        this.waitCreateCellComponent(callback);
      } else {
        callback();
      }
    } else {
      this._totalItems = (typeof totalItems !== 'undefined') ? totalItems : 1;
      if (this.options.usePaging) {
        this.calculatePaging();
      }
      this.tableData = this.buildRows();
      if (this.options.afterBuildRows) {
        this.options.afterBuildRows();
      }
    }
  }

  /**
   * Public api set extraData
   * @param field
   * @param value
   */
  public setExtraData(field: string, value: any) {
    this.options.extraData[field] = value;
    this.generateData(this._pageSize, this._page);
  }

  public getPageContainItem(item, highlight: boolean = true) {
    let index = -1;
    if (this.options.useBackboneModel) {
      index = _.findIndex(this._finalData, {model: item});
    } else {
      index = _.findIndex(this._finalData, {entity: item});
    }

    if (index !== -1) {
      if (highlight) {
        const rowItem = this._finalData[index];
        rowItem.rowClass = 'c-row-highlight';
        setTimeout(() => {
          rowItem.rowClass = '';
        }, 5000);
      }
      return Math.floor(index / this._pageSize) + 1;
    }

    return 0;
  }

  public selectPageContainItem(item: any, highlight: boolean = true) {
    const page = this.getPageContainItem(item, highlight);
    if (page) {
      this.selectPage(page);
    }
  }

  /**
   * Public api calculate paging when you add or remove item from options.data
   */
  public calculatePaging() {
    this._totalPages = Math.ceil(this._totalItems / this._pageSize);
    if (this._totalPages === 0 || Number.isNaN(this._totalPages)) {
      this._totalPages = 1;
    }
    if (this._totalPages < this._page) {
      this._page = 1;
    }
  }

  public refresh() {
    this.options.refresh(this.getSearchModel(), this);
  }

  /**
   * Public api select page
   * @param page
   * @param event
   */
  public selectPage(page: any, event?: MouseEvent): void {
    if (isNaN(page)) {
      page = 1;
    }

    page = parseInt(page);

    if (event) {
      event.preventDefault();
    }

    if (event && event.target) {
      const target: any = event.target;
      target.blur();
    }

    if (page > 0 && page <= this._totalPages) {
      this._page = page;
    } else if (page > this._totalPages) {
      this._page = this._totalPages;
    } else {
      this._page = 1;
    }

    this.generateData(this._pageSize, this._page, false, false);
  }

  /**
   * Public api change page size
   * @param pageSize
   */
  public changePageSize(pageSize: number) {
    this._pageSize = pageSize;
    this._totalPages = Math.ceil(this._totalItems / this._pageSize);

    if (this._page > this._totalPages) {
      this._page = 1;
    }

    this.generateData(this._pageSize, this._page, false, false);
  }

  public noNext(): boolean {
    return this._page === this._totalPages;
  }

  public noPrevious(): boolean {
    return this._page === 1;
  }

  /**
   * API search function
   * @param keyword
   */
  public search(keyword: string = null) {
    if (keyword || keyword === '') this._filterText = keyword;
    this.generateData(this._pageSize, this._page);
  }

  public sortOnColumn(column: CTableColumnConfig) {
    column.sort = column.sort === 'asc' ? 'desc' : (column.sort == 'desc' ? '' : 'asc');

    // reset all column sort
    this.options.columnDefs.forEach(function (col) {
      if (col !== column) {
        col.sort = '';
      }
    });

    this.generateData(this._pageSize, this._page);
  }

  /**
   * Use to generateData
   * @param pageSize
   * @param page
   * @param cloneData
   * @param sortAndFilter
   */
  private generateData(pageSize: number, page: number, cloneData: boolean = true, sortAndFilter: boolean = true) {
    // in case you use local data
    if (this.options.useLocalTable) {
      // clone data
      if (cloneData) {
        this._finalData = this._initialData.slice(0);
      }
      // sort and filter
      if (sortAndFilter) {
        this.filterData(this._finalData);
        this.filterDataOnColumns(this._finalData, 'useSearch');
        this.filterDataOnColumns(this._finalData, 'useFilterDate');
        this.filterDataOnColumns(this._finalData, 'useFilter');
        this.filterDataOnColumns(this._finalData, 'useSort');
      }

      this._totalItems = this._finalData.length;

      if (this.options.usePaging) {
        this.calculatePaging();
        // check slice index is exist if not move to first index
        let sliceIndex = (page - 1) * pageSize;
        sliceIndex = this._finalData[sliceIndex] ? sliceIndex : 0;
        this.tableData = this._finalData.slice(sliceIndex, page * pageSize);
      } else {
        this.tableData = this._finalData;
      }
    } else {
      // in case you use remote data
      if (this.defaultConfig.isAutoload === true) {
        this.options.refresh(this.getSearchModel(), this);
      }
    }
  }

  private buildRows() {
    const data = [];
    for (const i in this.options.data) {
      const row = this.options.useBackboneModel ?
        new CTableRow(this.options.data[i]['attributes'], i, false, this.options.data[i]) :
        new CTableRow(this.options.data[i], i);
      data.push(row);
    }
    return data;
  }

  private prepareTable(): Promise<any> {
    // build rows
    this._initialData = this.buildRows();

    // set Fags for table
    for (const col of this.options.columnDefs) {
      if (col.useSort) {
        this.useColumnSort = true;
      }

      if (col.useSearch) {
        this.useColumnSearch = true;
      }

      if (col.useFilter) {
        this.useColumnFilter = true;
        if (!col.filterOptions.values) {
          col.filterOptions.values = [];
        }
        if (col.filterOptions.autoGenerateData) {
          col.filterOptions.data = [];
          this.filterColumns.push(col);
        }
      }

      if (col.useFilterDate) {
        this.useColumnFilterDate = true;
        if (!col.filterDateOptions) {
          col.filterDateOptions = {};
        }

        col.filterDateOptions.fromOptions = Object.assign({}, this.defaultDateConfig, {
          // format: col.filterDateOptions.format ? col.filterDateOptions.format :
          //   this.defaultDateConfig.format,
          onDateChange: (date) => {
            col.filterDateOptions.fromDate = date;
            col.filterDateOptions.toOptions.minDate = date;
            this.generateData(this._pageSize, this._page);
          }
        }, col.filterDateOptions);

        col.filterDateOptions.toOptions = Object.assign({}, this.defaultDateConfig, {
          // format: col.filterDateOptions.format ? col.filterDateOptions.format :
          //   this.defaultDateConfig.format,
          onDateChange: (date) => {
            col.filterDateOptions.toDate = date;
            col.filterDateOptions.fromOptions.maxDate = date;
            this.generateData(this._pageSize, this._page);
          }
        }, col.filterDateOptions);
      }

      // check and create dynamic component for column
      this.createCellTemplate(col);
    }

    // generate filter data for columns
    this.generateFilterDataOnColumns();

    // after loop all column set generate dynamicComponent
    this.createdCellTemplates = true;

    return this.createdCellComponents ? Promise.resolve(false) : this.createComponentForCells();
  }

  /**
   * Handle pre create component for column has cell template
   * @param col
   */
  private createCellTemplate(col: CTableColumnConfig) {
    if (col.cellTemplate && !this.createdCellTemplates && !this.builder.getComponentFactory(col.cellTemplate)) {
      const metaData = new Component({
        selector: 'c-cell-html',
        template: col.cellTemplate
      });
      col.dynamicComponent = Component(metaData)(class DynamicComponent {
      });
    }

    if (col.headerCellTemplate && !this.createdCellTemplates && !this.builder.getComponentFactory(col.headerCellTemplate)) {
      const metaData = new Component({
        selector: 'c-cell-header-html',
        template: col.headerCellTemplate
      });
      col.dynamicHeaderComponent = Component(metaData)(class DynamicComponent {
      });
    }
  }

  private createComponentForCells() {
    const cols = [],
      colNames = [];
    let colDirectives = [];
    for (const col of this.options.columnDefs) {
      // check again in builder to make sure
      if (col.dynamicComponent && !this.builder.getComponentFactory(col.cellTemplate)) {
        cols.push(col.dynamicComponent);
        colNames.push(col.cellTemplate);
        if (col.directives) {
          colDirectives = colDirectives.concat(col.directives);
        }
      }

      if (col.dynamicHeaderComponent && !this.builder.getComponentFactory(col.headerCellTemplate)) {
        cols.push(col.dynamicHeaderComponent);
        colNames.push(col.headerCellTemplate);
        if (col.headerDirectives) {
          colDirectives = colDirectives.concat(col.headerDirectives);
        }
      }
    }
    const length = colDirectives.length;
    colDirectives = colDirectives.concat(cols);

    if (Array.isArray(colDirectives) && !colDirectives.length) {
      return Promise.resolve(true);
    }

    if (Array.isArray(this.options.components)) {
      Array.prototype.push.apply(colDirectives, this.options.components);
    }
    if (Array.isArray(this.options.pipes)) {
      Array.prototype.push.apply(colDirectives, this.options.pipes);
    }

    return this.builder.createComponentFactory(colDirectives, colNames, length);
  }

  /**
   * Generate filter data for columns when autoGenerateData = true
   */
  private generateFilterDataOnColumns() {
    if (this.filterColumns.length) {
      for (const row of this._initialData) {
        for (const col of this.filterColumns) {
          if (CTableHelper.getFieldValue(row, col.field) !== '' &&
            col.filterOptions.data.indexOf(CTableHelper.getFieldValue(row, col.field)) === -1) {
            col.filterOptions.data.push(CTableHelper.getFieldValue(row, col.field));
          }
        }
      }
      // sort data filter
      const sortFn = (a, b) => {
        return a > b ? 1 : (b > a ? -1 : 0);
      };
      for (const col of this.filterColumns) {
        col.filterOptions.data.sort(sortFn);
      }
    }
  }

  private filterOnColumn(column: CTableColumnConfig, value: string | number, checked: boolean) {
    const values = column.filterOptions.values;
    if (checked) {
      if (values.indexOf(value) === -1) {
        values.push(value);
      }
    } else {
      for (const i in values) {
        if (values[i] === value) {
          values.splice(Number(i), 1);
          break;
        }
      }
    }

    if (values.length === column.filterOptions.data.length) {
      column.filterOptions.checkAll = true;
    }

    if (values.length === 0) {
      column.filterOptions.checkAll = false;
    }

    this.generateData(this._pageSize, this._page);
  }

  private searchDataOnColumn(column: CTableColumnConfig, searchText: string) {
    searchText = searchText + '';
    column.searchText = searchText.toLowerCase();
    this.generateData(this._pageSize, this._page);
  }

  private toggleExpandRow(row) {
    row._expand = !row._expand;
  }

  private toggleExpandRows() {
    this._expandRows = !this._expandRows;
    for (const row of this.tableData) {
      row._expand = this._expandRows;
    }
  }

  private toggleCheckAll(column: CTableColumnConfig, event, checked) {
    CTableHelper.toggleAllCheckbox(event, checked);
    column.filterOptions.checkAll = checked;
    column.filterOptions.values = checked ? column.filterOptions.data.slice(0) : [];
    this.generateData(this._pageSize, this._page);
  }

  private filterData(data: Array<Object>) {
    if (this.options.useSearch && this._filterText) {
      this._filterText = this._filterText + '';
      const result = [];
      for (const row of data) {
        if (CTableHelper.isRowContainFilterText(row, this.options.searchOptions, this._filterText.toLowerCase())) {
          result.push(row);
        }
      }

      this._finalData = result;
    }
  }

  private filterDataOnColumns(data: Array<Object>, actionType) {
    let validFn;
    switch (actionType) {
      case 'useFilter':
        if (!this.useColumnFilter) {
          return;
        }
        validFn = CTableHelper.isRowValidFilterData;
        break;
      case 'useSearch':
        if (!this.useColumnSearch) {
          return;
        }
        validFn = CTableHelper.isRowContainSearchText;
        break;
      case 'useFilterDate':
        if (!this.useColumnFilterDate) {
          return;
        }
        validFn = CTableHelper.isRowValidOnDateRange;
        break;
      case 'useSort':
        if (!this.useColumnSort) {
          return;
        }
        for (const column of this.options.columnDefs) {
          if (column.sort) {
            const sortFn = (a, b) => {
              a = a['entity'][column.field];
              b = b['entity'][column.field];
              if (column.sort === 'asc') {
                return (a > b) ? 1 :
                  ((b > a) ? -1 : 0);
              }
              return (a < b) ? 1 :
                ((b < a) ? -1 : 0);
            };

            data.sort(sortFn);
            // use break because just support sort in one column
            break;
          }
        }
        return;
    }

    const result = [],
      columns = CTableHelper.getColumnsByFilter(this.options.columnDefs, actionType);
    for (const row of data) {
      if (validFn(row, columns)) {
        result.push(row);
      }
    }
    this._finalData = result;
  }

  private getSearchModel() {
    const model = {};
    if (this._filterText) {
      model['_filterText'] = this._filterText;
    }
    if (this.options.usePaging) {
      model['_page'] = this._page;
      model['_pageSize'] = this._pageSize;
    }

    for (const column of this.options.columnDefs) {
      if (column.useSort && column.sort) {
        if (!model['_order']) {
          model['_order'] = {};
        }
        model['_order'][column.field] = column.sort;
      }

      if (column.useFilter && column.filterOptions.values.length) {
        model[column.field] = column.filterOptions.values;
      }

      if (column.useSearch && column.searchText) {
        model[column.field] = column.searchText;
      }

      if (column.useFilterDate && (column.filterDateOptions.from || column.filterDateOptions.to)) {
        model[column.field] = {
          from: column.filterDateOptions.fromDate,
          to: column.filterDateOptions.toDate
        };
      }
    }
    return model;
  }

  private checkExpandRow(row, expandStatus = true) {
    const expandField = this.options.expandRowOptions.field;
    return this.options.useExpandRow && expandStatus && (this.options.expandRowOptions.tableOptions.useBackboneModel ?
      (row['entity'][expandField]['models'] && row['entity'][expandField]['models']['length']) :
      (row['entity'][expandField] && row['entity'][expandField]['length']));
  }

  public getSortClass(sortValue) {
    if (sortValue) {
      switch (sortValue.toLowerCase()) {
        case 'asc':
          return this.options.sortClass.asc;
        case 'desc':
          return this.options.sortClass.desc;
      }
    }
    return this.options.sortClass.default;
  }

  getSelectedData(propertyName) {
    const selectedData = [];
    _.each(this.tableData, (row) => {
      if (row.check) {
        if (propertyName) {
          selectedData.push(row['entity'][propertyName]);
        } else {
          selectedData.push(row);
        }
      }
    });
    return selectedData;
  }

  selectAll() {
    this.checkAll = !this.checkAll;

    _.each(this.tableData, (row) => {
      row.check = this.checkAll;
    });
  }
}
