import { NgModule } from '@angular/core';
import { CTableComponent } from './c-table.component';
import { CInputDebounceDirective } from './c-input-debounce.directive';
import { CCellComponent } from './c-cell.component';
import { FormsModule } from '@angular/forms';
import { CTableInputSearchComponent } from './c-table-input-search.component';
import { CDynamicRowBuilder } from './c-dynamic-row-builder';
import { CHeaderCellComponent } from './c-header-cell.component';
import { CPipeModule } from '../../pipes/c-pipe.module';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [CommonModule, FormsModule, CPipeModule],
  declarations: [CInputDebounceDirective, CTableComponent, CCellComponent, CTableInputSearchComponent, CHeaderCellComponent],
  providers: [
    CDynamicRowBuilder
  ],
  exports: [CInputDebounceDirective, CTableComponent, CTableInputSearchComponent]
})
export class CTableModule {
}
