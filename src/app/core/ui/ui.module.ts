import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CTableModule } from './c-table/c-table.module';
import { CClassModule } from './c-class/c-class.module';

@NgModule({
  imports: [
    CommonModule,
    CTableModule,
    CClassModule
  ],
  exports: [
    CTableModule,
    CClassModule
  ],
  declarations: []
})
export class UiModule { }
