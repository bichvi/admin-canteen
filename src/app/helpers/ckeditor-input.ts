import { Component } from '@angular/core';
import { FieldType } from '@ngx-formly/core';


@Component({
  selector: 'formly-ckeditor',
  template: `
    <div class="form-group">
      <div class="input-group">
        <ck-editor [formControl]="formControl" [formlyAttributes]="field"
                   skin="moono-lisa" language="en" [config]="config" [fullPage]="false"></ck-editor>
      </div>
    </div>
  `,
})
export class CkeditorInputComponent extends FieldType {
  config = {};

  constructor() {
    super();

    this.config['toolbarGroups'] = [
      {name: 'clipboard', groups: ['clipboard', 'undo']},
      // { name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
      {name: 'links'},
      // { name: 'insert' },
      // { name: 'forms' },
      // { name: 'tools' },
      // { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
      // { name: 'others' },
      // '/',
      {name: 'basicstyles', groups: ['basicstyles', 'cleanup']},
      {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align']},
      {name: 'styles'},
      {name: 'colors'},
      // { name: 'about' }
    ];

    // Remove some buttons provided by the standard plugins, which are
    // not needed in the Standard(s) toolbar.
    this.config['removeButtons'] = 'Underline,Subscript,Superscript,Table,Image';

    this.config['removePlugins'] = 'sourcearea,specialchar,horizontalrule,elementspath,resize';

    // Set the most common block elements.
    this.config['format_tags'] = 'p;h1;h2;h3;pre';

    // Simplify the dialog windows.
    this.config['removeDialogTabs'] = 'image:advanced;link:advanced';
  }
}
