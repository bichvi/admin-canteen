import { Injectable } from '@angular/core';
import {HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';

import { AuthenticationService } from '../services';
import { ToastrService } from 'ngx-toastr';
import {Router} from '@angular/router';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthenticationService,  private router: Router, private toasterService: ToastrService) { }

    // intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    //     return next.handle(request).pipe(catchError(err => {
    //         if (err.status === 401) {
    //             // auto logout if 401 response returned from api
    //             this.authenticationService.logout();
    //             location.reload(true);
    //             this.router.navigate(['/login']);
    //         }
    //
    //         const error = err.error.message || err.statusText;
    //         return throwError(error);
    //     }))
    // }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log(request);
    return next.handle(request).pipe(tap(evt => {
      if (evt instanceof HttpResponse) {
        console.log(evt);
        if (evt.body && evt.body.status === 1) {
          this.toasterService.success(evt.body.message, '', { positionClass: 'toast-bottom-center' });
        }
      }
    }), catchError(err => {
      if (err.status === 401) {
        // auto logout if 401 response returned from api
        this.authenticationService.logout();
        // location.reload(true);
        this.router.navigate(['/login']).then(r => console.log(r));
      } else if (err.status === 403) {
        this.toasterService.error('An error occurred', '', { positionClass: 'toast-bottom-center' });
      }

      const error = err.error.message || err.statusText;
      return throwError(error);
      // if (err instanceof HttpErrorResponse) {
      //   // do error handling here
      //   switch (err.status) {
      //     case 401:
      //       this.authenticationService.logout();
      //       this.router.navigate(['/login']).then(r => console.log(r));
      //       break;
      //     case 403:
      //       try {
      //         this.toasterService.error(err.error.message, err.error.title, { positionClass: 'toast-bottom-center' });
      //       } catch (e) {
      //         this.toasterService.error('An error occurred', '', { positionClass: 'toast-bottom-center' });
      //       }
      //       break;
      //     case 404:
      //     case 500:
      //     default:
      //       if (request.body && request.body._preventToast) {
      //         return;
      //       }
      //
      //       if (err.error && err.error.Message) {
      //         alert(err.error.Message);
      //       } else {
      //         alert(err.message);
      //       }
      //   }
      // }
    })) as any;
  }
}
