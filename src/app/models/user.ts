export class User {
    user_admin_id: number;
    user_group_id: string;
    username: string;
    email: string;
    password: string;
    fullname: string;
    created_date: string;
    created_by: string;
    modified_date: string;
    modified_by: string;
    is_active: boolean;
    is_deleted: boolean;
    // token?: string;
    // status: number;
    // message: string;
    data?: string;
}