import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { BankService } from 'src/app/services/bank.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import AppHelper from 'src/app/helpers/app.helper';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-bank-form',
  templateUrl: './bank-form.component.html'
})
export class BankFormComponent implements OnInit {
  form = new FormGroup({});
  model: any = {};
  public bank;
  public imageSrc: string;
  options: FormlyFormOptions = {};
  fields: FormlyFieldConfig[] = [
    {
      fieldGroupClassName: 'row',
      fieldGroup: [
        {
          className: 'col-xl-4 col-lg-6 col-12',
          type: 'input', key: 'bank_name',
          templateOptions: {label: 'Tên ngân hàng', placeholder: 'Nhâp tên ngân hàng', required: true},
        },
      ],
    }
  ];
  constructor(
    private bankService: BankService,
    public activeModal: NgbActiveModal,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    if (this.model.logo) {
      this.imageSrc = 'https://uit-canteen.s3-ap-southeast-1.amazonaws.com' + this.model.logo;
    }
    this.bank = AppHelper.deepClone(this.model);
  }
  handleInputChange(e) {
    const file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    const pattern = /image-*/;
    const reader = new FileReader();
    if (!file.type.match(pattern)) {
      this.toastr.error('Định dạng không đúng', 'Lỗi');
      return;
    }
    reader.readAsDataURL(file);
    reader.onload = this._handleReaderLoaded.bind(this);
  }
  _handleReaderLoaded(e) {
    const reader = e.target;
    this.imageSrc = reader.result;
    this.model.logo_data = reader.result;
  }

  createBank() {
    const result = this.bank.bank_id !== 0 ?
        this.bankService.updateBank(this.model, this.bank.bank_id) :
        this.bankService.updateBank(this.model);
    result.toPromise().then((response) => {
        if (response.status === 1) {
          this.activeModal.close();
          this.bankService.publish(BankService.EVENT_REFRESH_BANK_LIST, response.data);
          this.toastr.success(response.message, 'Thành công');
          this.options.resetModel();
          this.imageSrc = null;
        } else {
          this.toastr.error(response.message, 'Lỗi', {
            enableHtml: true
          });
        }
      });
    }
}

