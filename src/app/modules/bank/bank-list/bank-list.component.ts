import { Component, OnInit, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';
import { BankService } from 'src/app/services/bank.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BankFormComponent } from '../bank-form/bank-form.component';
import { CTableComponent, CTableConfig } from 'src/app/core/ui/c-table/c-table';
// tslint:disable-next-line:max-line-length
import { BankActionColComponent, BankCheckColComponent, BankActionHeaderColComponent } from '../columns/bank-action-column.component';
import { BankNameColComponent } from '../columns/bank-name-column.component';
import {BankImageColComponent} from '../columns/bank-image-column.component';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-bank-list',
  templateUrl: './bank-list.component.html'
})
export class BankListComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('bankTable') bankTable: CTableComponent;

  public tableDefaultConfig = {
    useLocalTable: true,
    tableTitle: 'Danh sách ngân hàng liên kết',
    tableClass: 'table table-striped table-borderless mb-0',
    useSearch: true,
    searchOptions: {
      inputVisible: true,
      placeHolderText: 'Tìm kiếm theo tên',
    },
    pagingOptions: {
      showTotalRecords: true,
      pageSizes: [5, 10, 15, 20],
      pageSize: 10,
      currentPage: 1
    },
  };


  public categories = [];
  // tslint:disable-next-line:ban-types
  allChecked: Boolean = false;

  tableOptions: CTableConfig = Object.assign({
    columnDefs: [
      {
        displayName: '',
        cellComponent: BankCheckColComponent,
        headerCellComponent: BankActionHeaderColComponent,
      },
      {
        displayName: 'STT',
        field: 'no',
      },
      {
        displayName: 'Biểu tượng',
        field: 'logo',
        cellComponent: BankImageColComponent,
        cellClass: 'w-1',
      },
      {
        displayName: 'Tên ngân hàng',
        field: 'bank_name',
        cellClass: 'font-weight',
        cellComponent: BankNameColComponent,
      },
    ],
    extraData: {
      remove: () => {
          const listId = [];
          this.categories.forEach(obj => {
            if (obj.checkedAction) {
              listId.push(obj.bank_id);
            }
          });

          if (!listId.length) {
            this.toastr.error('Please select bank(s) to remove', 'Error');
            return;
          }

          this.bankService.deleteMultiBank({
            bank_ids: listId,
          }).subscribe((e: any) => {
            this.bankTable.refresh();
            this.allChecked = false;
            this.toastr.success(e.message, 'Success');
          });
      },
      openDetails: (data) => {
        this.openBankModal(data);
      },
      setActionAll: () => {
        this.allChecked = !!!this.allChecked;
        this.categories.forEach(obj => {
          obj.checkedAction = this.allChecked;
        });
      },
      getAllCheckStatus: () => {
        return this.allChecked;
      }
    },
    refresh: (params, table) => {
      this.bankService.getBankList().subscribe((res: any) => {
        if (res.status === 1 && res.data) {
          this.categories = res.data;
          let startNo = (params._page * 10) - 9;
          res.data.forEach((item: any) => {
            item.no = startNo;
            ++startNo ;
          });
          table.setData(res.data);
        }
      });
    }
  }, this.tableDefaultConfig);

  constructor(private bankService: BankService,  private modalService: NgbModal,  private toastr: ToastrService) { }

  ngOnInit() {
    this.bankService.subscribe(BankService.EVENT_REFRESH_BANK_LIST, () => {
      this.bankTable.refresh();
    });
  }

  ngAfterViewInit() {
    this.bankTable.refresh();
  }
  openBankModal(bank = null) {
    const modalRef = this.modalService.open(BankFormComponent, {size: 'lg', centered: true, backdrop: 'static'});
    modalRef.componentInstance.model = bank ? bank : {};
  }

  ngOnDestroy() {
    this.bankService.unsubscribe(BankService.EVENT_REFRESH_BANK_LIST);
  }
}
