import { NgModule } from '@angular/core';

import { BankRoutingModule } from './bank-routing.module';
import { BankListComponent } from './bank-list/bank-list.component';
import { BankFormComponent } from './bank-form/bank-form.component';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { FormlyModule } from '@ngx-formly/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '../common/common.module';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { CoreModule } from 'src/app/core/core.module';
import { BankActionColComponent, BankCheckColComponent, BankActionHeaderColComponent } from './columns/bank-action-column.component';
import { BankNameColComponent } from './columns/bank-name-column.component';
import { FormlyFieldFile } from './file-type.component';
import { FileValueAccessor } from './file-value-accessor';
import {BankImageColComponent} from './columns/bank-image-column.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations: [BankListComponent, BankFormComponent, BankActionColComponent, BankNameColComponent, BankCheckColComponent, BankActionHeaderColComponent, BankImageColComponent, FormlyFieldFile, FileValueAccessor],
  entryComponents: [BankListComponent, BankFormComponent, BankActionColComponent, BankNameColComponent, BankCheckColComponent, BankActionHeaderColComponent, BankImageColComponent],
  imports: [
    BankRoutingModule,
    CommonModule,
    ReactiveFormsModule,
    BrowserModule,
    CoreModule,
    FormsModule,
    HttpModule,
    NgbModule,
    FormlyBootstrapModule,
    FormlyModule.forRoot({
      validationMessages: [
        { name: 'required', message: 'Trường này là bắt buộc' },
      ],
      types: [
        { name: 'file', component: FormlyFieldFile, wrappers: ['form-field'] },
      ],
    }),
    ConfirmationPopoverModule.forRoot(),
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot() // ToastrModule added
  ]
})
export class BankModule { }
