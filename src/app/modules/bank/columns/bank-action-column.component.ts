import { Component } from '@angular/core';
import { CTableCell } from '../../../core/ui/c-table/c-table-config';

@Component({
  selector: 'app-bank-action-col',
  template: `
    <button title="Remove"
            class="btn btn-link px-0 text-decoration-none"
            mwlConfirmationPopover placement="left" [appendToBody]="true"
            [popoverTitle]="'Confirmation'"
            [popoverMessage]="'Are you sure you want to remove this bank?'"
            (confirm)="extraData.remove('single', row.entity)"
    ><i class="fa fa-trash"></i></button>`
})

export class BankActionColComponent implements CTableCell {
  extraData;
  row;
  column;
}

@Component({
  selector: 'app-bank-check-action',
  template: `
    <div class="custom-control custom-checkbox">
      <input class="custom-control-input" type="checkbox" name="checkboxA"
          id="bank-{{row.entity.bank_id}}"
          [checked]="row.entity.checkedAction"
          (change)="row.entity.checkedAction = !row.entity.checkedAction">
          <label class="custom-control-label" for="bank-{{row.entity.bank_id}}"></label>
    </div>
    `
})
export class BankCheckColComponent implements CTableCell {
  extraData;
  row;
  column;
}


@Component({
  selector: 'app-bank-action-header',
  template: `
      <div class="custom-control custom-checkbox">
        <input type="checkbox" name="checkboxHeader" class="custom-control-input"
                id="check-1-{{extraData.getAllCheckStatus()}}"
               [checked]="extraData.getAllCheckStatus()"
               (change)="extraData.setActionAll()">
               <label class="custom-control-label custom-lead cursor-pointer" for="check-1-{{extraData.getAllCheckStatus()}}"></label>
      </div>
  `
})
export class BankActionHeaderColComponent implements CTableCell {
  extraData;
  row;
  column;
}
