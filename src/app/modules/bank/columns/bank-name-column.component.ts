import { Component } from '@angular/core';
import { CTableCell } from '../../../core/ui/c-table/c-table-config';

@Component({
  selector: 'app-bank-name-col',
  template: `
  <a style=" color: #005192 ;" (click)="extraData.openDetails(row.entity)" title="{{row.entity.bank_name}}">{{row.entity.bank_name}}</a>
    `
})

export class BankNameColComponent implements CTableCell {
  extraData;
  row;
  column;
}


