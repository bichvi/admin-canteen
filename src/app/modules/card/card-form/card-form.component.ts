import {Component, OnInit, OnDestroy, AfterContentChecked} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { CardService } from 'src/app/services/card.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import AppHelper from 'src/app/helpers/app.helper';
import {startWith, takeUntil, tap} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {ToastrService} from 'ngx-toastr';
import {BankService} from '../../../services/bank.service';
import {TemplateOptionsConstants} from '../../../core/constans/template-options-constants';
@Component({
  selector: 'app-card-form',
  templateUrl: './card-form.component.html',
})
export class CardFormComponent implements OnInit, OnDestroy {

  form = new FormGroup({});
  model: any = {};
  public card;
  options: FormlyFormOptions = {};
  public imageSrc: string;
  onDestroy$ = new Subject<void>();
  fields: FormlyFieldConfig[] = [
    {
      fieldGroupClassName: 'row',
      fieldGroup: [
        {
          className: 'col-xl-6 col-lg-6 col-12',
          key: 'cardholder_name',
          type: 'input',
          templateOptions: {
            label: 'Card name',
            placeholder: 'Enter card name',
            required: true,
          },
        },
        {
          className: 'col-xl-4 col-lg-6 col-12',
          key: 'card_type_id',
          type: 'radio',
          defaultValue: 1,
          templateOptions: {
            type: 'radio',
            label: 'Select type card',
            options: TemplateOptionsConstants.bankType
          }
        },
        {
          className: 'col-xl-4 col-lg-6 col-12',
          type: 'select',
          key: 'bank_id',
          templateOptions: {
            label: 'Bank',
            placeholder: 'Select bank',
            required: true,
            options: [],
          },
          hooks: {
            onInit: (field) => {
              this.bankService.getBankList().toPromise()
                .then((res) => {
                  field['templateOptions'].options = Array.from(res.data, item => {
                    // this.category.push(item);
                    return {label: item['bank_name'], value: item['bank_id']};
                  });
                  field.formControl.setValue(field.model[field.key]);
                });
            },
            onChanges: (field) => {
              field.formControl.valueChanges.pipe(
                takeUntil(this.onDestroy$),
                startWith(field.formControl.value),
                tap(value => {
                }),
              ).subscribe();
            },
          },
        },
        {
          className: 'col-xl-4 col-lg-6 col-12',
          key: 'valid_from',
          type: 'input',
          templateOptions: {
            label: 'Valid from',
            pattern: /^(0[1-9]|1[0-2])\/([0-9]{2})$/,
            required: true,
            placeholder: 'Enter valid from',
          },
          validation: {
            messages: {
              pattern: (error, field: FormlyFieldConfig) => `"${field.formControl.value}" is not a valid field with format MM/YY`,
            },
          },
          hideExpression: () => {
            return this.model.card_type_id === TemplateOptionsConstants.bankType[1].value;
          }
        },
        {
          className: 'col-xl-4 col-lg-6 col-12',
          key: 'expiration_date',
          type: 'input',
          templateOptions: {
            label: 'Expiration date',
            pattern: /^(0[1-9]|1[0-2])\/([0-9]{2})$/,
            required: true,
            placeholder: 'Enter expiration date',
          },
          validation: {
            messages: {
              pattern: (error, field: FormlyFieldConfig) => `"${field.formControl.value}" is not a valid field with format MM/YY`,
            },
          },
          hideExpression: () => {
            return this.model.card_type_id === TemplateOptionsConstants.bankType[0].value;
          }
        },
        {
          className: 'col-xl-4 col-lg-6 col-12',
          key: 'cvv',
          type: 'input',
          templateOptions: {
            label: 'CVV',
            placeholder: 'Enter cvv',
          },
          hideExpression: () => {
            return this.model.card_type_id === TemplateOptionsConstants.bankType[0].value;
          }
        },
        {
          className: 'col-xl-4 col-lg-6 col-12',
          key: 'balance',
          type: 'input',
          templateOptions: {
            label: 'Balance',
            placeholder: 'Enter balance',
          },
        },
      ]
    }
  ];

  constructor(
    private cardService: CardService,
    private bankService: BankService,
    public activeModal: NgbActiveModal,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.card = AppHelper.deepClone(this.model);
  }
  createCard() {
    if (this.form.valid) {
      // tslint:disable-next-line:max-line-length
      const result = this.card.card_id !== 0 ?
        this.cardService.updateCard(this.model, this.card.card_id) :
        this.cardService.updateCard(this.model);
      result.toPromise().then((response) => {
        if (response.status === 1) {
          this.activeModal.close();
          this.toastr.success(response.message, 'Success');
          this.cardService.getCardList();
          this.cardService.publish(CardService.EVENT_REFRESH_CARD_LIST, response.data);
          this.options.resetModel();
        } else {
          this.toastr.error(response.message, 'Error',{
            enableHtml: true
          });
        }
      });
    }
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

}
