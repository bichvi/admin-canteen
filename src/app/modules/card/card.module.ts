import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CardRoutingModule } from './card-routing.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { CoreModule } from 'src/app/core/core.module';
import { HttpModule } from '@angular/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { FormlyModule } from '@ngx-formly/core';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { CardFormComponent } from './card-form/card-form.component';
// tslint:disable-next-line:max-line-length
import { CardCheckColComponent, CardActionHeaderColComponent, CardActionColComponent} from './column/card-action-column.component';
import { CardNameColComponent } from './column/card-name-column.component';
import { CardListComponent } from './card-list/card-list.component';

@NgModule({
  // tslint:disable-next-line:max-line-length
  declarations: [
    CardFormComponent,
    CardListComponent,
    CardActionColComponent,
    CardCheckColComponent,
    CardActionHeaderColComponent,
    CardNameColComponent
  ],
  // tslint:disable-next-line:max-line-length
  entryComponents: [
    CardFormComponent,
    CardListComponent,
    CardActionColComponent,
    CardCheckColComponent,
    CardActionHeaderColComponent,
    CardNameColComponent
  ],
  imports: [
    CommonModule,
    CardRoutingModule,
    ReactiveFormsModule,
    BrowserModule,
    CoreModule,
    FormsModule,
    HttpModule,
    NgbModule,
    FormlyBootstrapModule,
    FormlyModule.forRoot({
      validationMessages: [
        { name: 'required', message: 'Trường này là bắt buộc' },
      ],
    }),
    ConfirmationPopoverModule.forRoot(),
  ]
})
export class CardModule { }
