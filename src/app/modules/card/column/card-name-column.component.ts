import { Component } from '@angular/core';
import { CTableCell } from '../../../core/ui/c-table/c-table-config';

@Component({
  selector: 'app-card-name-col',
  template: `
  <a style=" color: #005192 ;" (click)="extraData.openDetails(row.entity)" title="{{row.entity.cardholder_name}}">{{row.entity.cardholder_name}}</a>
    `
})

export class CardNameColComponent implements CTableCell {
  extraData;
  row;
  column;
}


