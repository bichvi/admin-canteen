import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { CategoryService } from 'src/app/services/category.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import AppHelper from 'src/app/helpers/app.helper';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-category-form',
  templateUrl: './category-form.component.html',
  styleUrls: ['./category-form.component.css']
})
export class CategoryFormComponent implements OnInit {
  form = new FormGroup({});
  model: any = {};
  public category;
  public imageSrc: string;
  options: FormlyFormOptions = {};
  fields: FormlyFieldConfig[] = [
    {
      fieldGroupClassName: 'row',
      fieldGroup: [
        {
          className: 'col-xl-4 col-lg-6 col-12',
          type: 'input', key: 'food_category_name',
          templateOptions: {label: 'Tên danh mục', placeholder: 'Nhập tên danh mục', required: true},
        },
      ],
    }
  ];
  constructor(
    private categoryService: CategoryService,
    public activeModal: NgbActiveModal,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    if (this.model.image) {
      this.imageSrc = 'https://uit-canteen.s3-ap-southeast-1.amazonaws.com' + this.model.image;
    }
    this.category = AppHelper.deepClone(this.model);
  }
  handleInputChange(e) {
    const file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    const pattern = /image-*/;
    const reader = new FileReader();
    if (!file.type.match(pattern)) {
      this.toastr.error('Sai định dạng', 'Lỗi');
      return;
    }
    reader.readAsDataURL(file);
    reader.onload = this._handleReaderLoaded.bind(this);
  }
  _handleReaderLoaded(e) {
    const reader = e.target;
    this.imageSrc = reader.result;
    this.model.image_data = reader.result;
  }

  createCategory() {
    const result = this.category.food_category_id !== 0 ?
        this.categoryService.updateCategory(this.model, this.category.food_category_id) :
        this.categoryService.updateCategory(this.model);
    result.toPromise().then((response) => {
        if (response.status === 1) {
          this.activeModal.close();
          this.categoryService.publish(CategoryService.EVENT_REFRESH_CATEGORY_LIST, response.data);
          this.toastr.success(response.message, 'Thành công');
          this.options.resetModel();
        } else {
          this.toastr.error(response.message);
        }
      });
    }
}

