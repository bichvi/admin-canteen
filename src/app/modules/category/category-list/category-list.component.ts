import { Component, OnInit, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';
import { CategoryService } from 'src/app/services/category.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CategoryFormComponent } from '../category-form/category-form.component';
import { CTableComponent, CTableConfig } from 'src/app/core/ui/c-table/c-table';
// tslint:disable-next-line:max-line-length
import { CategoryActionColComponent, CategoryCheckColComponent, CategoryActionHeaderColComponent } from '../columns/category-action-column.component';
import { CategoryNameColComponent } from '../columns/category-name-column.component';
import {CategoryImageColComponent} from '../columns/category-image-column.component';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.css']
})
export class CategoryListComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('categoryTable') categoryTable: CTableComponent;

  public tableDefaultConfig = {
    useLocalTable: true,
    tableTitle: 'Danh sách danh mục',
    tableClass: 'table table-striped table-borderless mb-0',
    useSearch: true,
    searchOptions: {
      inputVisible: true,
      placeHolderText: 'Tìm kiếm theo tên',
    },
    pagingOptions: {
      showTotalRecords: true,
      pageSizes: [5, 10, 15, 20],
      pageSize: 10,
      currentPage: 1
    },
  };


  public categories = [];
  // tslint:disable-next-line:ban-types
  allChecked: Boolean = false;
  tableOptions: CTableConfig = Object.assign({
    columnDefs: [
      {
        displayName: '',
        cellComponent: CategoryCheckColComponent,
        headerCellComponent: CategoryActionHeaderColComponent,
      },
      {
        displayName: 'STT',
        field: 'no',
      },
      {
        displayName: 'Hình danh mục',
        field: 'image',
        cellComponent: CategoryImageColComponent,
        cellClass: 'w-1',
      },
      {
        displayName: 'Tên danh mục',
        field: 'food_category_name',
        cellClass: 'font-weight',
        cellComponent: CategoryNameColComponent,
      },
    ],
    extraData: {
      remove: () => {
          const listId = [];
          this.categories.forEach(obj => {
            if (obj.checkedAction) {
              listId.push(obj.food_category_id);
            }
          });

          if (!listId.length) {
            this.toastr.error('Vui lòng chọn category để xóa', 'Lỗi');
            return;
          }

          this.categoryService.deleteMultiCategory({
            food_category_ids: listId,
          }).subscribe((e: any) => {
            this.categoryTable.refresh();
            this.allChecked = false;
            this.toastr.success(e.message, 'Thành công');
          });
      },
      openDetails: (data) => {
        this.openCategoryModal(data);
      },
      setActionAll: () => {
        this.allChecked = !!!this.allChecked;
        this.categories.forEach(obj => {
          obj.checkedAction = this.allChecked;
        });
      },
      getAllCheckStatus: () => {
        return this.allChecked;
      }
    },
    refresh: (params, table) => {
      this.categoryService.getCategoryList().subscribe((res: any) => {
        if (res.status === 1 && res.data) {
          this.categories = res.data;
          let startNo = (params._page * 10) - 9;
          res.data.forEach((item: any) => {
            item.no = startNo;
            ++startNo ;
          });
          table.setData(res.data);
        }
      });
    }
  }, this.tableDefaultConfig);

  constructor(private categoryService: CategoryService,  private modalService: NgbModal,  private toastr: ToastrService) { }

  ngOnInit() {
    this.listCategory();
    this.categoryService.subscribe(CategoryService.EVENT_REFRESH_CATEGORY_LIST, () => {
      this.categoryTable.refresh();
    });
  }

  ngAfterViewInit() {
    this.categoryTable.refresh();
  }

  listCategory() {
    this.categoryService.getCategoryList()
    .subscribe(categories => {
        this.categories = categories.data;
    });
  }

  deleteCategory(id: number) {
    this.categoryService.deleteCategory(id)
    .subscribe(categories => {
        this.categories = categories.data;
    });
  }

  openCategoryModal(category = null) {
    const modalRef = this.modalService.open(CategoryFormComponent, {size: 'lg', centered: true, backdrop: 'static'});
    modalRef.componentInstance.model = category ? category : {};
  }

  ngOnDestroy() {
    this.categoryService.unsubscribe(CategoryService.EVENT_REFRESH_CATEGORY_LIST);
  }
}
