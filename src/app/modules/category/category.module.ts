import { NgModule } from '@angular/core';

import { CategoryRoutingModule } from './category-routing.module';
import { CategoryListComponent } from './category-list/category-list.component';
import { CategoryFormComponent } from './category-form/category-form.component';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { FormlyModule } from '@ngx-formly/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '../common/common.module';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { CoreModule } from 'src/app/core/core.module';
import { CategoryActionColComponent, CategoryCheckColComponent, CategoryActionHeaderColComponent } from './columns/category-action-column.component';
import { CategoryNameColComponent } from './columns/category-name-column.component';
import { FormlyFieldFile } from './file-type.component';
import { FileValueAccessor } from './file-value-accessor';
import {CategoryImageColComponent} from './columns/category-image-column.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations: [CategoryListComponent, CategoryFormComponent, CategoryActionColComponent, CategoryNameColComponent, CategoryCheckColComponent, CategoryActionHeaderColComponent, CategoryImageColComponent, FormlyFieldFile, FileValueAccessor],
  entryComponents: [CategoryListComponent, CategoryFormComponent, CategoryActionColComponent, CategoryNameColComponent, CategoryCheckColComponent, CategoryActionHeaderColComponent, CategoryImageColComponent],
  imports: [
    CategoryRoutingModule,
    CommonModule,
    ReactiveFormsModule,
    BrowserModule,
    CoreModule,
    FormsModule,
    HttpModule,
    NgbModule,
    FormlyBootstrapModule,
    FormlyModule.forRoot({
      validationMessages: [
        { name: 'required', message: 'Trường này là bắt buộc' },
      ],
      types: [
        { name: 'file', component: FormlyFieldFile, wrappers: ['form-field'] },
      ],
    }),
    ConfirmationPopoverModule.forRoot(),
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot() // ToastrModule added
  ]
})
export class CategoryModule { }
