import { Component } from '@angular/core';
import { CTableCell } from '../../../core/ui/c-table/c-table-config';

@Component({
  selector: 'app-catgory-image-col',
  template: `
      <img class="rounded-circle shadow" height="38px" width="38px"
           src="{{row.entity.image ? link +  row.entity.image : \'./assets/assets/images/category-image.png\'}}"
           onError="this.src='./assets/assets/images/category-image.png';">
  `
})

export class CategoryImageColComponent implements CTableCell {
  extraData;
  row;
  column;
  link = 'https://uit-canteen.s3-ap-southeast-1.amazonaws.com';
}


