import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AuthenticationService } from 'src/app/services/authentication.service';
import {EventService} from '../../../services/event.service';
import {BankService} from '../../../services/bank.service';
declare var $: any;
@Component({ templateUrl: 'authentication.component.html' })
export class AuthenticationComponent implements OnInit {
  authenticationForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    public authenticationService: AuthenticationService
  ) {
    if (this.authenticationService.currentUserValue) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
    this.authenticationForm = this.formBuilder.group({
      username: ['', Validators.required],
    });
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/';
  }

  // convenience getter for easy access to form fields
  get f() { return this.authenticationForm.controls; }

  authenticateLogin() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.authenticationForm.invalid) {
      return;
    }
    this.loading = true;
    this.authenticationService.authenticationLogin(this.f.username.value)
      .pipe(first())
      .subscribe(
        res => {
          if (res.data) {
            console.log(res);
            if (res.status === 1) {
              this.authenticationService.publish(AuthenticationService.EVENT_RESET_PASSWORD, res.data.access_token);
              // this.router.navigate(['/reset-password']);
              // window.location.href = 'http://18.141.75.87/reset-password?access_token=' + res.data.access_token;
              // $('.topbar').show();
              // $('.left-sidebar').show();
              // $('.footer').show();
              // this.router.navigate([this.returnUrl]).then(r => console.log(r));
            } else {
              this.error = 'Tên đăng nhập không tồn tại';
              this.loading = false;
            }
          }
        },
        error => {
          this.error = error;
          this.loading = false;
        });
  }
}

