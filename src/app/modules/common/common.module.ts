import { NgModule } from '@angular/core';
import { CommonRoutingModule } from './common-routing.module';
import { HomeComponent } from './home/home.component';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormlyModule } from '@ngx-formly/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { CoreModule } from 'src/app/core/core.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import {DatepickerTypeComponent} from './datepicker.type';
import { OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE, OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { ChartsModule } from 'ng2-charts';
import { NgCircleProgressModule } from 'ng-circle-progress';
import {MonthyearpickerTypeComponent} from './monthyearpicker.type';
import {YearpickerTypeComponent} from './yearpicker.type';

import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
const socketConfig: SocketIoConfig = { url: 'http://18.141.75.87:6001', options: {} };

@NgModule({
  // tslint:disable-next-line:max-line-length
  declarations: [HomeComponent, DatepickerTypeComponent, MonthyearpickerTypeComponent, YearpickerTypeComponent],
  // tslint:disable-next-line:max-line-length
  entryComponents: [HomeComponent],
  imports: [
    CommonRoutingModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    BrowserModule,
    CoreModule,
    FormsModule,
    HttpModule,
    NgbModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    ChartsModule,
    FormlyBootstrapModule,
    FormlyModule.forRoot({
      validationMessages: [
        { name: 'required', message: 'Trường này là bắt buộc' },
      ],
      types: [
        {name: 'datepicker', component: DatepickerTypeComponent, extends: 'input'},
        {name: 'monthyearpicker', component: MonthyearpickerTypeComponent, extends: 'input'},
        {name: 'yearpicker', component: YearpickerTypeComponent, extends: 'input'},
      ],
    }),
    NgCircleProgressModule.forRoot({
      radius: 100,
      outerStrokeWidth: 16,
      innerStrokeWidth: 8,
      outerStrokeColor: '#78C000',
      innerStrokeColor: '#C7E596',
      animationDuration: 300,
    }),
    ConfirmationPopoverModule.forRoot(),
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(), // ToastrModule added
    SocketIoModule.forRoot(socketConfig) //Socket module
  ]
})
export class CommonModule { }
