import {Component, OnInit, ViewChild} from '@angular/core';
import { FieldType } from '@ngx-formly/core';
import {DateTimeAdapter, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE, OwlNativeDateTimeModule} from 'ng-pick-datetime';
@Component({
  selector: 'formly-datepicker',
  template: `
    <div class="form-group">
      <div class="input-group" #datetimeIconPosition>
        <div  (click)="handlePositionClick()" class="input-group-prepend" style="cursor: pointer" *ngIf="dtOptions.displayIcon && dtOptions.iconPosition == 'left'">
          <span class="input-group-text bg-transparent border-right-0">
            <i class="fa fa-calendar"></i>
          </span>
        </div>
              <owl-date-time [pickerType]="dtOptions.pickerType" [stepMinute]="dtOptions.stepMinute" #d></owl-date-time>
          <ng-container *ngIf="!dtOptions.filter else Filter">
            <input class="form-control" [formControl]="formControl" autocomplete="off" [disabled]="dtOptions.disable"
                   [min]="dtOptions.min" [max]="dtOptions.max"
                   [formlyAttributes]="field" [owlDateTimeTrigger]="d" [owlDateTime]="d"  #datetimeInput>
          </ng-container>
          <ng-template #Filter>
              <input class="form-control" [formControl]="formControl" autocomplete="off" [disabled]="dtOptions.disable"
                     [min]="dtOptions.min" [max]="dtOptions.max"
                     [formlyAttributes]="field" [owlDateTimeTrigger]="d" [owlDateTime]="d"
                     [owlDateTimeFilter]="dtOptions.filterFn" (afterPickerClosed)="dtOptions.filterFn" #datetimeInput>
          </ng-template>
        <div class="input-group-append cursor-pointer" *ngIf="dtOptions.displayIcon && dtOptions.iconPosition === 'right'" (click)="handlePositionClick()" #datetimeIconPosition>
          <span class="input-group-text">
            <i [class]="dtOptions.pickerType != 'timer' ? 'fa fa-calendar' : 'fa fa-clock'"></i>
          </span>
        </div>
      </div>
    </div>
  `,
})
export class DatepickerTypeComponent extends FieldType implements OnInit {
  field;
  // About options document here https://github.com/DanielYKPan/date-time-picker#readme
  dtOptions = {
    min: null,
    max: null,
    pickerType: 'both',
    displayIcon: true,
    iconPosition: 'right',
    stepMinute: 1,
    disable: false,
    filter: false,
    filterFn: (d: Date) => {
      return d;
    }
  };

  @ViewChild('datetimeIconPosition') datetimeIconPosition;
  @ViewChild('datetimeInput') datetimeInput;

  constructor(dateTimeAdapter: DateTimeAdapter<any>) {
    super();
    dateTimeAdapter.setLocale('vi-VN');
  }
  ngOnInit() {
    if (this.field.templateOptions.dtOptions) {
      this.field.templateOptions.dtOptions = Object.assign(this.dtOptions, this.field.templateOptions.dtOptions);
    }
  }
  handlePositionClick() {
    this.datetimeInput.nativeElement.click();
  }
}
