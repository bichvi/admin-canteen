import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
import {UserFormComponent} from '../../user/user-form/user-form.component';
import {UserListComponent} from '../../user/user-list/user-list.component';
import {UserService} from '../../../services';
import {OrderListComponent} from '../../order/order-list/order-list.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, AfterViewInit {
  @ViewChild(UserListComponent) userListComponent;
  constructor(private router: Router,
              private authenticationService: AuthenticationService,
              private userService: UserService,
              private modalService: NgbModal) { }
  public user;
  ngOnInit() {
    this.userService.getUserInfo().subscribe(user => {
      this.user = user.data;
    });
  }
  openUserModal() {
    const modalRef = this.modalService.open(UserFormComponent, {size: 'lg', centered: true, backdrop: 'static'});
    modalRef.componentInstance.model = this.user;
  }
  ngAfterViewInit(): void {
    // console.log(this.userListComponent);
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }
}
