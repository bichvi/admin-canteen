import { Component, OnInit, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';
import { DashboardService } from 'src/app/services/dashboard.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CTableComponent, CTableConfig } from 'src/app/core/ui/c-table/c-table';
import {ToastrService} from 'ngx-toastr';
import {FormGroup} from '@angular/forms';
import {FormlyFieldConfig, FormlyFormOptions} from '@ngx-formly/core';
import { ChartType, ChartOptions, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import {startWith, takeUntil, tap} from 'rxjs/operators';
import * as moment from 'moment';
import {Subject} from 'rxjs';
import { Color} from 'ng2-charts';
import * as pluginAnnotations from 'chartjs-plugin-annotation';
import { UserService } from 'src/app/services';
import { User } from 'src/app/models/user';
import { Socket } from 'ngx-socket-io';
import {Headers} from '@angular/http';
import {TemplateOptionsConstants} from '../../../core/constans/template-options-constants';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy, AfterViewInit {
  user: User;
  loading = false;
  form = new FormGroup({});
  onDestroy$ = new Subject<void>();
  year = [2019, 2020, 2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028, 2029, 2030];
  model: any = {
    type: 'date',
    from: new Date(),
    to: new Date(),
  };
  public dashboard;
  public staticList: any = {};
  public tableDefaultConfig = {
    useLocalTable: true,
    usePaging: false,
    tableClass: 'table table-striped table-borderless mb-0',
  };
  options: FormlyFormOptions = {};
  fields: FormlyFieldConfig[] = [
    {
      fieldGroupClassName: 'row',
      fieldGroup: [
        {
          className: 'col-xl-4 col-lg-4 col-12',
          type: 'select', key: 'type',
          templateOptions: {
            label: 'Mốc thời gian',
            placeholder: 'Chọn mốc',
            required: true,
            options: [
              { value: 'date', label: 'Ngày'  },
              { value: 'month', label: 'Tháng'},
              { value: 'year', label: 'Năm'  },
            ],
          },
        },
        // {
        //   className: 'col-xl-4 col-lg-4 col-12',
        //   type: 'select',
        //   key: 'from',
        //   templateOptions: {label: 'Từ', placeholder: 'Chọn năm', required: true,  options: []},
        //   hooks: {
        //     onInit: (field) => {
        //       field['templateOptions'].options = Array.from(this.year, item => {
        //         return {label: item, value: item};
        //       });
        //       field.formControl.setValue(field.model[field.key]);
        //     },
        //     onChanges: (field) => {
        //       field.formControl.valueChanges.pipe(
        //         takeUntil(this.onDestroy$),
        //         startWith(field.formControl.value),
        //         tap(value => {
        //         }),
        //       ).subscribe();
        //     },
        //   },
        //   hideExpression: () => {
        //     return this.model.type === 'date';
        //   }
        // },
        // {
        //   className: 'col-xl-4 col-lg-4 col-12',
        //   type: 'select',
        //   key: 'to',
        //   templateOptions: {label: 'Đến', placeholder: 'Chọn năm', required: true,  options: []},
        //   hooks: {
        //     onInit: (field) => {
        //       field['templateOptions'].options = Array.from(this.year, item => {
        //         return {label: item, value: item};
        //       });
        //       field.formControl.setValue(field.model[field.key]);
        //     },
        //     onChanges: (field) => {
        //       field.formControl.valueChanges.pipe(
        //         takeUntil(this.onDestroy$),
        //         startWith(field.formControl.value),
        //         tap(value => {
        //         }),
        //       ).subscribe();
        //     },
        //   },
        //   hideExpression: () => {
        //     return this.model.type === 'date';
        //   }
        // },
        {
          className: 'col-xl-4 col-md-4 col-12', key: 'from', type: 'datepicker',
          templateOptions: {
            label: 'Từ',
            required: false,
            placeholder: 'mm/dd/yyyy',
            dtOptions: {
              pickerType: 'calendar',
            },
          },
          hooks: {
            onChanges: (field) => {
              field.formControl.valueChanges.pipe(
                takeUntil(this.onDestroy$),
                startWith(field.formControl.value),
                tap(value => {
                  this.onSubmit();
                }),
              ).subscribe();
            },
          },
          // hideExpression: () => {
          //   return this.model.type !== 'date';
          // }
        },
        {
          className: 'col-xl-4 col-md-4 col-12', key: 'to', type: 'datepicker',
          templateOptions: {
            label: 'Đến',
            required: false,
            placeholder: 'mm/dd/yyyy',
            dtOptions: {
              pickerType: 'calendar',
            },
          },
          hooks: {
            onChanges: (field) => {
              field.formControl.valueChanges.pipe(
                takeUntil(this.onDestroy$),
                startWith(field.formControl.value),
                tap(value => {
                  this.onSubmit();
                }),
              ).subscribe();
            },
          },
          // hideExpression: () => {
          //   return this.model.type !== 'date';
          // }
        },
      ],
    }
  ];
  public barChartOptions: ChartOptions = {
    responsive: true,
    // tooltips: {
    //   callbacks: {
    //     value: tooltipItem => `${tooltipItem.yLabel}`,
    //   }
    // },
    scales: { xAxes: [], yAxes: [] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
        display: false
      }
    }
  };
  public barChartLabels: Label[] = [];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [pluginDataLabels];

  public barChartData: ChartDataSets[] = [
    { data: [], label: '' },
    { data: [], label: '' }
  ];
  @ViewChild('topFoodTable') topFoodTable: CTableComponent;

  public topFoods = [];
  public refreshFoodsByParams = false;

  tableOptions: CTableConfig = Object.assign({
    columnDefs: [
      {
        displayName: 'Tên',
        field: 'food_name',
        cellClass: 'font-weight',
      },
      {
        displayName: 'Số lượng đặt',
        field: 'total_order',
        cellClass: 'font-weight',
      },
    ],
    extraData: {
    },
    refresh: (params, table) => {
      if (this.refreshFoodsByParams) {
        table.setData(this.topFoods);
        this.refreshFoodsByParams = false;
      } else {
        if (this.model.from && this.model.to) {
          this.model.from = this.convertDate(this.model.from);
          this.model.to = this.convertDate(this.model.to);
        }
        this.dashboardService.getTopFoods(this.model).subscribe((res: any) => {
          if (res.status === 1 && res.data) {
            this.topFoods = res.data;
            table.setData(this.topFoods);
          }
        });
      }
    }
  }, this.tableDefaultConfig);

  @ViewChild('topCustomersTable') topCustomersTable: CTableComponent;

  public topCustomers = [];
  public refreshCustomersByParams = false;

  topCustomersOptions: CTableConfig = Object.assign({
    columnDefs: [
      {
        displayName: 'Tên',
        field: 'fullname',
        cellClass: 'font-weight',
      },
      {
        displayName: 'Tổng tiền',
        field: 'total_purchased',
        cellClass: 'font-weight',
      },
    ],
    extraData: {
    },
    refresh: (params, table) => {
      if (this.refreshCustomersByParams) {
        table.setData(this.topCustomers);
        this.refreshCustomersByParams = false;
      } else {
        this.dashboardService.getTopCustomers(this.model).subscribe((res: any) => {
          if (res.status === 1 && res.data) {
            this.topCustomers = res.data;
            table.setData(this.topCustomers);
          }
        });
      }
    }
  }, this.tableDefaultConfig);


  // Pie
  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'top',
    },
    plugins: {
      datalabels: {
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          return label;
        },
        display: false
      },
    }
  };
  public pieChartLabels: Label[] = [];
  public pieChartData: number[] = [0, 0, 0];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [pluginDataLabels];
  public pieChartDataIndex = [];
  public pieChartColors = [
    {
      backgroundColor: ['rgba(255,0,0,0.3)', 'rgba(0,255,0,0.3)', 'rgba(0,0,255,0.3)'],
    },
  ];

  public pieChartLabels1: Label[] = [];
  public pieChartData1: number[] = [0, 0, 0];
  // tslint:disable-next-line:max-line-length
  constructor(private dashboardService: DashboardService,  private modalService: NgbModal,  private toastr: ToastrService, private userService: UserService, private socket: Socket) { }
  ngOnInit() {
    this.loading = true;
    // get users from secure api end point
    this.userService.getUserInfo().subscribe(user => {
      this.loading = false;
      this.user = user;
      console.log(this.user);
      this.loadSocket();
    });
    this.dashboardService.subscribe(DashboardService.EVENT_REFRESH_DASHBOARD_LIST, () => {
      this.topFoodTable.refresh();
      this.topCustomersTable.refresh();
    });
  }

  ngAfterViewInit() {
    this.topFoodTable.refresh();
    this.topCustomersTable.refresh();
    this.onSubmit();
  }

  onSubmit() {
    if (this.model.from && this.model.to) {
      this.model.from = this.convertDate(this.model.from);
      this.model.to = this.convertDate(this.model.to);
    }

    this.topFoodTable.refresh();
    this.topCustomersTable.refresh();
    this.dashboardService.loadPieChart(this.model).toPromise().then(res => {
      if (res.data) {
        this.pieChartData = res.data.total_order.data;
        this.pieChartLabels = res.data.total_order.labels;
        this.pieChartData1 = res.data.revenue.data;
        this.pieChartLabels1 = res.data.revenue.labels;
        this.pieChartDataIndex = res.data.data_index;
      }
    });

    this.dashboardService.loadBarChart(this.model).toPromise().then(res => {
      if (res.data) {
        this.barChartData = Array.from(Object.values(res.data.datasets), (item, index) => {
          return { data: item['data'], label: item['label'] };
        });
        this.barChartLabels = res.data.labels;
      }
    });
    this.dashboardService.loadStatistics(this.model).toPromise().then(res => {
      if (res.data) {
        this.staticList = res.data;
      }
    });
  }

  convertDate(inputFormat) {
    function pad(s) { return (s < 10) ? '0' + s : s; }
    const d = new Date(inputFormat);
    return [d.getFullYear(), pad(d.getMonth() + 1), pad(d.getDate())].join('-');
  }

  loadSocket() {
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    const jwtToken = currentUser.data;
    this.socket.emit('init', jwtToken);

    this.socket.on('new_user', data => {
      if (data) {
        this.staticList.total_user_register++;
      }
    });

    this.socket.on('new_order_dashboard', socketData => {
      // console.log(socketData);
      if (typeof socketData !== 'undefined') {
        const data = socketData.data;
        this.staticList.total_order++;

        if (data && typeof data.total_price_discount !== 'undefined' && typeof data.total_price !== 'undefined') {
          this.staticList.revenue = +this.staticList.revenue + +data.total_price_discount;
          this.staticList.total_discount = +this.staticList.total_discount + (+data.total_price - +data.total_price_discount);

          // update barchart data
          // let indexValue = null;
          // switch (this.model.type) {
          //   case 'date':
          //     indexValue = (new Date(socketData.created_date)).getDate();
          //     break;
          //   case 'month':
          //     indexValue = (new Date(socketData.created_date)).getDate();
          //     break;
          //   case 'year':
          //     indexValue = (new Date(socketData.created_date)).getDate();
          //     break;
          //   default: break;
          // }
          // let index = this.barChartDataIndex.indexOf(indexValue);

          let barChartIndex = null;
          switch (data.payment_method_id) {
            case 1:
              barChartIndex = this.barChartData[0].data.length - 1;
              this.barChartData[0].data[barChartIndex] = +this.barChartData[0].data[barChartIndex] + +data.total_price_discount;
              this.barChartData[0].data = this.barChartData[0].data.slice();
              break;
            case 2:
              barChartIndex = this.barChartData[1].data.length - 1;
              this.barChartData[1].data[barChartIndex] = +this.barChartData[1].data[barChartIndex] + +data.total_price_discount;
              this.barChartData[1].data = this.barChartData[1].data.slice();
              break;
            default: break;
          }

          // update piechart data
          let index = null;
          let indexValue = null;
          for (const orderDetail of data.order_details) {
            indexValue = orderDetail.food_category_id;
            index = this.pieChartDataIndex.indexOf(indexValue);
            this.pieChartData[index]++;
            this.pieChartData1[index] = +this.pieChartData1[index] + orderDetail.price_discount;

            // update food data
            const indexFood = this.topFoods.findIndex(element => element.food_id === orderDetail.food_id);
            if (indexFood !== -1) {
              this.topFoods[indexFood].total_order = +this.topFoods[indexFood].total_order + orderDetail.quantity;
            } else if (this.topFoods.length < 10) {
              this.topFoods.push({
                food_id: orderDetail.food_id,
                food_name: orderDetail.food_name,
                total_order: orderDetail.quantity
              });
            }
          }
          this.pieChartData = this.pieChartData.slice();
          this.pieChartData1 = this.pieChartData1.slice();

          // update customer data
          // tslint:disable-next-line:triple-equals
          const indexCustomer = this.topCustomers.findIndex(element => element.user_id == data.user_id);
          if (indexCustomer !== -1) {
            // tslint:disable-next-line:max-line-length
            this.topCustomers[indexCustomer].total_purchased = +this.topCustomers[indexCustomer].total_purchased + data.total_price_discount;
          } else if (this.topCustomers.length < 10) {
            this.topCustomers.push({
              user_id: data.user_id,
              fullname: data.fullname,
              total_purchased: data.total_price_discount
            });
          }

          this.refreshCustomersByParams = true;
          this.topCustomersTable.refresh();

          this.refreshFoodsByParams = true;
          this.topFoodTable.refresh();
        }
      }
    });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
    this.dashboardService.unsubscribe(DashboardService.EVENT_REFRESH_DASHBOARD_LIST);
  }
}
