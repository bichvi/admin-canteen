import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AuthenticationService } from 'src/app/services/authentication.service';
declare var $: any;
@Component({ templateUrl: 'login.component.html' })
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
  ) {
    if (this.authenticationService.currentUserValue) {
      this.router.navigate(['/']);
    }
    // redirect to home if already logged in
    // let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    // console.log(currentUser);
    //     console.log(this.authenticationService.currentUserValue);
    //     if (this.authenticationService.currentUserValue) {
    //         console.log('navigator toi home');
    //         this.router.navigate(['/']);
    //     } else {
    $('.topbar').hide();
    $('.left-sidebar').hide();
    $('.footer').hide();
    //     }
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/';
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }
    this.loading = true;
    this.authenticationService.login(this.f.username.value, this.f.password.value)
      .pipe(first())
      .subscribe(
        data => {
          if (data) {
            if (data.status === 1) {
              $('.topbar').show();
              $('.left-sidebar').show();
              $('.footer').show();
              this.router.navigate([this.returnUrl]).then(r => console.log(r));
            } else {
              this.error = 'Sai tên tài khoản hoặc mật khẩu';
              this.loading = false;
            }
          }
        },
        error => {
          this.error = error;
          this.loading = false;
        });
  }
}


// import { Component, OnInit } from '@angular/core';
// import { Router } from '@angular/router';
// import { AuthenticationService } from 'src/app/services';
// declare var $: any;
// @Component({ templateUrl: 'reset.component.html' })

// export class ResetComponent implements OnInit {
//     model: any = {};
//     loading = false;
//     error = '';

//     constructor(
//         private router: Router,
//         private authenticationService: AuthenticationService) {
//             $('.topbar').hide();
//             $('.left-sidebar').hide();
//             $('.footer').hide();
//         }

//     ngOnInit() {
//         // reset login status
//         this.authenticationService.logout();
//     }

//     login() {
//         this.loading = true;
//         this.authenticationService.login(this.model.username, this.model.password)
//             .subscribe(result => {
//                 console.log(result);
//                 if (result.status === 1) {
//                     // login successful
//                     this.router.navigate(['/']);
//                 } else {
//                     // login failed
//                     this.error = 'Username or password is incorrect';
//                     this.loading = false;
//                 }
//             });
//     }
// }
