import {ChangeDetectionStrategy, Component, OnInit, ViewChild} from '@angular/core';
import { FieldType } from '@ngx-formly/core';
// tslint:disable-next-line:max-line-length
import {DateTimeAdapter, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE, OwlDateTimeComponent, OwlDateTimeFormats} from 'ng-pick-datetime';
import { MomentDateTimeAdapter, OwlMomentDateTimeModule } from 'ng-pick-datetime-moment';
import * as _moment from 'moment';
import { Moment } from 'moment';

const moment = (_moment as any).default ? (_moment as any).default : _moment;
import {FormControl} from '@angular/forms';
export const MY_MOMENT_DATE_TIME_FORMATS: OwlDateTimeFormats = {
  parseInput: 'MM/YYYY',
  fullPickerInput: 'l LT',
  datePickerInput: 'MM/YYYY',
  timePickerInput: 'LT',
  monthYearLabel: 'MMM YYYY',
  dateA11yLabel: 'LL',
  monthYearA11yLabel: 'MMMM YYYY',
};
@Component({
  selector: 'formly-monthyearpicker',
  template: `
      <div class="form-group">
          <div class="input-group">
              <input class="form-control" aria-label="Amount (to the nearest dollar)" autocomplete="off"
                     [disabled]="dtOptions.disable"
                     [formlyAttributes]="field"
                     [owlDateTimeTrigger]="dt"
                     [owlDateTime]="dt"
                     [formControl]="formControl">
              <div class="input-group-append">
                  <span class="input-group-text"><i class="fa fa-calendar"></i></span>
              </div>
              <owl-date-time [pickerType]="'calendar'"
                             [startView]="'multi-years'"
                             (yearSelected)="chosenYearHandler($event)"
                             (monthSelected)="chosenMonthHandler($event, dt)"
                             #dt="owlDateTime"></owl-date-time>
          </div>
      </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {provide: OWL_DATE_TIME_LOCALE, useValue: 'vi'},
    // `MomentDateTimeAdapter` and `OWL_MOMENT_DATE_TIME_FORMATS` can be automatically provided by importing
    // `OwlMomentDateTimeModule` in your applications root module. We provide it at the component level
    // here, due to limitations of our example generation script.
    {provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE]},

    {provide: OWL_DATE_TIME_FORMATS, useValue: MY_MOMENT_DATE_TIME_FORMATS},
  ],
})
export class MonthyearpickerTypeComponent extends FieldType implements OnInit {
  field;
  // About options document here https://github.com/DanielYKPan/date-time-picker#readme
  dtOptions = {
    min: null,
    max: null,
    pickerType: 'both',
    displayIcon: true,
    iconPosition: 'right',
    stepMinute: 1,
    disable: false,
    filter: false,
    filterFn: (d: Date) => {
      return d;
    }
  };
  constructor() {
    super();
  }
  ngOnInit() {
    // this.field.formControl.setValue(moment());
    if (this.field.templateOptions.dtOptions) {
      this.field.templateOptions.dtOptions = Object.assign(this.dtOptions, this.field.templateOptions.dtOptions);
    }
  }
  chosenYearHandler( normalizedYear: Moment ) {
    if (this.field.formControl.value) {
      const ctrlValue =  this.field.formControl.value;
      ctrlValue.year(normalizedYear.year());
      this.field.formControl.setValue(ctrlValue);
    }
  }

  chosenMonthHandler( normalizedMonth: Moment, datepicker: OwlDateTimeComponent<any> ) {
    if (this.field.formControl.value) {
      const ctrlValue = this.field.formControl.value;
      ctrlValue.month(normalizedMonth.month());
      this.field.formControl.setValue(ctrlValue);
      datepicker.close();
    }
  }
}
