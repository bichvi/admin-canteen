import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AuthenticationService } from 'src/app/services/authentication.service';
import {BankService} from '../../../services/bank.service';
declare var $: any;
@Component({ templateUrl: 'reset.component.html' })
export class ResetComponent implements OnInit {
  resetForm: FormGroup;
  loading = false;
  submitted = false;
  error = '';

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
  ) {
    // if (this.authenticationService.currentUserValue) {
    //   this.router.navigate(['/']);
    // }
  }

  ngOnInit() {
    this.resetForm = this.formBuilder.group({
      password: ['', Validators.required],
    });
    // // get return url from route parameters or default to '/'
    // this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/';
  }

  // convenience getter for easy access to form fields
  get f() { return this.resetForm.controls; }

  resetPassword() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.resetForm.invalid) {
      return;
    }
    this.loading = true;
    // @ts-ignore
    // this.route.queryParams.subscribe(params => {
    //   if (params.access_token) {
    this.authenticationService.subscribe(AuthenticationService.EVENT_RESET_PASSWORD, (data) => {
      console.log(data);
      this.authenReset(data);
    });
    //   }
    // });
  }
  authenReset(data) {
    this.authenticationService.resetPass(this.f.password.value, data)
      .pipe(first())
      .subscribe (
        res => {
          console.log(res);
          if (res.status === 1) {
            this.router.navigate(['/login']);
          } else {
            this.error = 'Tên đăng nhập không tồn tại';
            this.loading = false;
          }
        },
        error => {
          this.error = error;
          this.loading = false;
        });
  }
}

