import { Component } from '@angular/core';
import { CTableCell } from '../../../core/ui/c-table/c-table-config';

@Component({
  selector: 'app-dashboard-image-col',
  template: `
      <img class="rounded-circle shadow" height="38px" width="38px"
           src="{{row.entity.logo ? link +  row.entity.logo : \'./assets/assets/images/dashboard-image.jpg\'}}"
           onError="this.src='./assets/assets/images/dashboard-image.jpg';">
  `
})

export class DashboardImageColComponent implements CTableCell {
  extraData;
  row;
  column;
  link = 'https://uit-canteen.s3-ap-southeast-1.amazonaws.com';
}


