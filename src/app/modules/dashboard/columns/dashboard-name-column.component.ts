import { Component } from '@angular/core';
import { CTableCell } from '../../../core/ui/c-table/c-table-config';

@Component({
  selector: 'app-dashboard-name-col',
  template: `
  <a style=" color: #005192 ;" (click)="extraData.openDetails(row.entity)" title="{{row.entity.dashboard_name}}">{{row.entity.dashboard_name}}</a>
    `
})

export class DashboardNameColComponent implements CTableCell {
  extraData;
  row;
  column;
}


