import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { DashboardService } from 'src/app/services/dashboard.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import AppHelper from 'src/app/helpers/app.helper';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-dashboard-form',
  templateUrl: './dashboard-form.component.html'
})
export class DashboardFormComponent implements OnInit {
  form = new FormGroup({});
  model: any = {};
  public dashboard;
  public imageSrc: string;
  options: FormlyFormOptions = {};
  fields: FormlyFieldConfig[] = [
    {
      fieldGroupClassName: 'row',
      fieldGroup: [
        {
          className: 'col-xl-4 col-lg-6 col-12',
          type: 'input', key: 'dashboard_name',
          templateOptions: {label: 'Dashboard name', placeholder: 'Enter dashboard name', required: true},
        },
      ],
    }
  ];
  constructor(
    private dashboardService: DashboardService,
    public activeModal: NgbActiveModal,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    if (this.model.logo) {
      this.imageSrc = 'https://uit-canteen.s3-ap-southeast-1.amazonaws.com' + this.model.logo;
    }
    this.dashboard = AppHelper.deepClone(this.model);
  }
  handleInputChange(e) {
    const file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    const pattern = /image-*/;
    const reader = new FileReader();
    if (!file.type.match(pattern)) {
      this.toastr.error('Invalid format', 'Error');
      return;
    }
    reader.readAsDataURL(file);
    reader.onload = this._handleReaderLoaded.bind(this);
  }
  _handleReaderLoaded(e) {
    const reader = e.target;
    this.imageSrc = reader.result;
    this.model.logo_data = reader.result;
  }

  createDashboard() {
    const result = this.dashboard.dashboard_id !== 0 ?
        this.dashboardService.updateDashboard(this.model, this.dashboard.dashboard_id) :
        this.dashboardService.updateDashboard(this.model);
    result.toPromise().then((response) => {
        if (response.status === 1) {
          this.activeModal.close();
          this.dashboardService.publish(DashboardService.EVENT_REFRESH_DASHBOARD_LIST, response.data);
          this.toastr.success(response.message, 'Success');
          this.options.resetModel();
        } else {
          this.toastr.error(response.message);
        }
      });
    }
}

