import { Component, OnInit, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';
import { DashboardService } from 'src/app/services/dashboard.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {ToastrService} from 'ngx-toastr';
import {FormGroup} from '@angular/forms';
import {FormlyFieldConfig, FormlyFormOptions} from '@ngx-formly/core';
import { ChartType, ChartOptions, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import {startWith, takeUntil, tap} from 'rxjs/operators';
import * as moment from 'moment';
import {Subject} from 'rxjs';


@Component({
  selector: 'app-dashboard-list',
  templateUrl: './dashboard-list.component.html'
})
export class DashboardListComponent implements OnInit, OnDestroy {
  form = new FormGroup({});
  estimateForm = new FormGroup({});
  estimateModel: any = {
  };
  public isNew: boolean;
  public isUpdate = false;
  public today = this.convertDate(new Date());
  onDestroy$ = new Subject<void>();
  model: any = {
    month: moment(),
  };
  result: any = {
    percent1: null,
    percent2: null,
    data1: null,
    data2: null,
    requiredTotal: null,
    currentTotal: null
  };
  public dashboard;
  public data;
  public estimateList: any = {};
  public tableDefaultConfig = {
    useLocalTable: true,
    usePaging: false,
    tableClass: 'table table-striped table-borderless mb-0',
  };
  options: FormlyFormOptions = {};
  fields: FormlyFieldConfig[] = [
    {
      fieldGroupClassName: 'row',
      fieldGroup: [
        {
          className: 'col-xl-4 col-md-4 col-12', key: 'month', type: 'monthyearpicker',
          templateOptions: {
            label: 'Thời gian',
            required: false,
            placeholder: 'mm/yyyy',
            dtOptions: {
              pickerType: 'calendar'
            },
          },
          hooks: {
            onChanges: (field) => {
              field.formControl.valueChanges.pipe(
                takeUntil(this.onDestroy$),
                startWith(field.formControl.value),
                tap(value => {
                  this.onSubmit();
                }),
              ).subscribe();
            },
          },
        }
      ],
    }
  ];

  estimateOptions: FormlyFormOptions = {};
  estimateFields: FormlyFieldConfig[] = [
    {
      fieldGroupClassName: 'row',
      fieldGroup: [
        {
          className: 'col-xl-4 col-lg-4 col-12',
          type: 'input', key: 'fixed_cost',
          templateOptions: {placeholder: 'Nhập tổng chi phí', required: true},
        },
        {
          className: 'col-xl-4 col-lg-4 col-12',
          type: 'input', key: 'variable_cost',
          templateOptions: {placeholder: 'Nhập chi phí khác', required: true},
        },
        {
          className: 'col-xl-4 col-lg-4 col-12',
          type: 'input', key: 'desired_profit',
          templateOptions: {placeholder: 'Nhập số tiền lời mong muốn', required: true},
        },
      ],
    }
  ];
  public barChartOptions: ChartOptions = {
    responsive: true,
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
        display: false
      }
    }
  };
  public barChartLabels: Label[] = [];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [pluginDataLabels];

  public barChartData: ChartDataSets[] = [
    { data: [], label: '' },
    { data: [], label: '' }
  ];


  constructor(private dashboardService: DashboardService,  private modalService: NgbModal,  private toastr: ToastrService) { }
  ngOnInit() {
  }
  onEstimate() {
    // console.log(this.model);
    this.estimateModel = Object.assign(this.model, this.estimateModel);
    // if(this.estimateModel.monitoring_id > 0) {
    //   this.dashboardService.createMonitoring(this.estimateModel, this.estimateModel.monitoring_id)
    // }
    console.log(this.estimateModel);
    const temp = this.estimateModel.monitoring_id !== 0 ?
      this.dashboardService.createMonitoring(this.estimateModel, this.estimateModel.monitoring_id) :
      this.dashboardService.createMonitoring(this.estimateModel);
    temp.toPromise().then(res => {
      this.isUpdate = false;
      this.loadResult();
      // if (res.data.fixed_cost) {
      //   this.isNew = true;
      //   this.estimateModel.fixed_cost = Number(res.data.fixed_cost);
      //   this.estimateModel.variable_cost = Number(res.data.variable_cost);
      //   this.estimateModel.desired_profit = res.data.desired_profit;
      //   this.result.requiredTotal = res.data.required_total_revenue;
      //   this.result.currentTotal = res.data.current_total_revenue;
      //   if (res.data.total_cost - res.data.current_profit < 0) {
      //     this.result.percent1 = 100;
      //     this.result.data1 = 0;
      //     this.result.percent2 = ((res.data.current_profit - res.data.total_cost) / res.data.desired_profit) * 100;
      //     this.result.data2 = res.data.desired_profit - (res.data.current_profit - res.data.total_cost);
      //   } else {
      //     this.result.percent1 = (res.data.current_profit / res.data.total_cost) * 100;
      //     this.result.data1 = res.data.total_cost - res.data.current_profit;
      //   }
      // }
    });
  }
  onSubmit() {
    this.model.month = this.convertYearMonth(this.model.month._d);
    this.loadResult();
  }
  loadResult() {
    this.dashboardService.getDetailMonitoring(this.model).toPromise().then(res => {
      if (res.data.monitoring_id > 0) {
        this.isNew = false;
        this.estimateModel.fixed_cost = Number(res.data.fixed_cost);
        this.estimateModel.variable_cost = Number(res.data.variable_cost);
        this.estimateModel.desired_profit = res.data.desired_profit;
        this.estimateModel.monitoring_id = res.data.monitoring_id;
        this.result.requiredTotal = res.data.required_total_revenue;
        this.result.currentTotal = res.data.current_total_revenue;
        if (res.data.total_cost - res.data.current_profit < 0) {
          this.result.percent1 = 100;
          this.result.data1 = 0;
          this.result.percent2 = ((res.data.current_profit - res.data.total_cost) / res.data.desired_profit) * 100;
          this.result.data2 = res.data.desired_profit - (res.data.current_profit - res.data.total_cost);
        } else {
          this.result.percent1 = (res.data.current_profit / res.data.total_cost) * 100;
          this.result.data1 = res.data.total_cost - res.data.current_profit;
        }
      } else {
        this.isNew = true;
        this.estimateModel.fixed_cost = null;
        this.estimateModel.variable_cost = null;
        this.estimateModel.desired_profit = null;
        this.result.requiredTotal = null;
        this.result.currentTotal = null;
      }
    });
    this.dashboardService.loadMonitoringBarChart(this.model).toPromise().then(res => {
      if (res.data && res.data.datasets) {
        this.barChartData = Array.from(Object.values(res.data.datasets), (item, index) => {
          return { data: item['data'], label: item['label'] };
        });
        this.barChartLabels = res.data.labels;
      }
    });
  }

  convertDate(inputFormat) {
    function pad(s) { return (s < 10) ? '0' + s : s; }
    const d = new Date(inputFormat);
    return [d.getFullYear(), pad(d.getMonth() + 1), pad(d.getDate())].join('-');
  }

  showUpdate() {
    this.isUpdate = true;
  }

  convertYearMonth(inputFormat) {
    function pad(s) { return (s < 10) ? '0' + s : s; }
    const d = new Date(inputFormat);
    return [d.getFullYear(), pad(d.getMonth() + 1)].join('-');
  }
  ngOnDestroy() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
    this.dashboardService.unsubscribe(DashboardService.EVENT_REFRESH_DASHBOARD_LIST);
  }
}
