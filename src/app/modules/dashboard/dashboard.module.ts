import { NgModule } from '@angular/core';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardListComponent } from './dashboard-list/dashboard-list.component';
import { DashboardFormComponent } from './dashboard-form/dashboard-form.component';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { FormlyModule } from '@ngx-formly/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '../common/common.module';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { CoreModule } from 'src/app/core/core.module';
// tslint:disable-next-line:max-line-length
import { DashboardActionColComponent, DashboardCheckColComponent, DashboardActionHeaderColComponent } from './columns/dashboard-action-column.component';
import { DashboardNameColComponent } from './columns/dashboard-name-column.component';
import {DashboardImageColComponent} from './columns/dashboard-image-column.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import {DatepickerTypeComponent} from './datepicker.type';
import { OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE, OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { ChartsModule } from 'ng2-charts';
import { NgCircleProgressModule } from 'ng-circle-progress';
import {MonthyearpickerTypeComponent} from './monthyearpicker.type';
import {YearpickerTypeComponent} from './yearpicker.type';


@NgModule({
  // tslint:disable-next-line:max-line-length
  declarations: [DashboardListComponent, DashboardFormComponent, DashboardActionColComponent, DashboardNameColComponent, DashboardCheckColComponent, DashboardActionHeaderColComponent, DashboardImageColComponent, DatepickerTypeComponent, MonthyearpickerTypeComponent, YearpickerTypeComponent],
  // tslint:disable-next-line:max-line-length
  entryComponents: [DashboardListComponent, DashboardFormComponent, DashboardActionColComponent, DashboardNameColComponent, DashboardCheckColComponent, DashboardActionHeaderColComponent, DashboardImageColComponent],
  imports: [
    DashboardRoutingModule,
    CommonModule,
    ReactiveFormsModule,
    BrowserModule,
    CoreModule,
    FormsModule,
    HttpModule,
    NgbModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    ChartsModule,
    FormlyBootstrapModule,
    FormlyModule.forRoot({
      validationMessages: [
        { name: 'required', message: 'Trường này là bắt buộc' },
      ],
      types: [
        {name: 'datepicker', component: DatepickerTypeComponent, extends: 'input'},
        {name: 'monthyearpicker', component: MonthyearpickerTypeComponent, extends: 'input'},
        {name: 'yearpicker', component: YearpickerTypeComponent, extends: 'input'},
      ],
    }),
    NgCircleProgressModule.forRoot({
      radius: 100,
      outerStrokeWidth: 16,
      innerStrokeWidth: 8,
      outerStrokeColor: '#78C000',
      innerStrokeColor: '#C7E596',
      animationDuration: 300,
    }),
    ConfirmationPopoverModule.forRoot(),
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot() // ToastrModule added
  ]
})
export class DashboardModule { }
