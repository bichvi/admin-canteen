import {ChangeDetectionStrategy, Component, OnInit, ViewChild} from '@angular/core';
import { FieldType } from '@ngx-formly/core';
import {DateTimeAdapter, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE, OwlDateTimeFormats} from 'ng-pick-datetime';
import {MomentDateTimeAdapter} from 'ng-pick-datetime-moment';

// export const MY_MOMENT_DATE_TIME_FORMATS: OwlDateTimeFormats = {
//   parseInput: 'MM/YYYY',
//   fullPickerInput: 'l LT',
//   datePickerInput: 'MM/YYYY',
//   timePickerInput: 'LT',
//   monthYearLabel: 'MMM YYYY',
//   dateA11yLabel: 'LL',
//   monthYearA11yLabel: 'MMMM YYYY',
// };
@Component({
  selector: 'formly-datepicker',
  template: `
      <div class="form-group">
          <div class="input-group" #datetimeIconPosition>
              <div  (click)="handlePositionClick()" class="input-group-prepend" style="cursor: pointer" *ngIf="dtOptions.displayIcon && dtOptions.iconPosition == 'left'">
          <span class="input-group-text bg-transparent border-right-0">
            <i class="fa fa-calendar"></i>
          </span>
              </div>
              <owl-date-time [pickerType]="dtOptions.pickerType" [stepMinute]="dtOptions.stepMinute" #d></owl-date-time>
              <ng-container *ngIf="!dtOptions.filter else Filter">
                  <input class="form-control" [formControl]="formControl" autocomplete="off" [disabled]="dtOptions.disable"
                         [min]="dtOptions.min" [max]="dtOptions.max"
                         [formlyAttributes]="field" [owlDateTimeTrigger]="d" [owlDateTime]="d"  #datetimeInput>
              </ng-container>
              <ng-template #Filter>
                  <input class="form-control" [formControl]="formControl" autocomplete="off" [disabled]="dtOptions.disable"
                         [min]="dtOptions.min" [max]="dtOptions.max"
                         [formlyAttributes]="field" [owlDateTimeTrigger]="d" [owlDateTime]="d"
                         [owlDateTimeFilter]="dtOptions.filterFn" (afterPickerClosed)="dtOptions.filterFn" #datetimeInput>
              </ng-template>
              <div class="input-group-append cursor-pointer" *ngIf="dtOptions.displayIcon && dtOptions.iconPosition === 'right'" (click)="handlePositionClick()" #datetimeIconPosition>
          <span class="input-group-text">
            <i [class]="dtOptions.pickerType != 'timer' ? 'fa fa-calendar' : 'fa fa-clock'"></i>
          </span>
              </div>
          </div>
      </div>
  `,
  // changeDetection: ChangeDetectionStrategy.OnPush,
  // providers: [
  //   {provide: OWL_DATE_TIME_LOCALE, useValue: 'vi'},
  //   // `MomentDateTimeAdapter` and `OWL_MOMENT_DATE_TIME_FORMATS` can be automatically provided by importing
  //   // `OwlMomentDateTimeModule` in your applications root module. We provide it at the component level
  //   // here, due to limitations of our example generation script.
  //   {provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE]},
  //
  //   {provide: OWL_DATE_TIME_FORMATS, useValue: MY_MOMENT_DATE_TIME_FORMATS},
  // ],
})
export class DatepickerTypeComponent extends FieldType implements OnInit {
  field;
  // About options document here https://github.com/DanielYKPan/date-time-picker#readme
  dtOptions = {
    min: null,
    max: null,
    showDateMonthOnly: false,
    pickerType: 'both',
    displayIcon: true,
    iconPosition: 'right',
    stepMinute: 1,
    disable: false,
    filter: false,
    filterFn: (d: Date) => {
      return d;
    }
  };

  @ViewChild('datetimeIconPosition') datetimeIconPosition;
  @ViewChild('datetimeInput') datetimeInput;

  constructor() {
    super();
  }
  ngOnInit() {
    if (this.field.templateOptions.dtOptions) {
      this.field.templateOptions.dtOptions = Object.assign(this.dtOptions, this.field.templateOptions.dtOptions);
    }
  }
  handlePositionClick() {
    this.datetimeInput.nativeElement.click();
  }
}
