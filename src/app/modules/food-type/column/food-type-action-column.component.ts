import { Component } from '@angular/core';
import { CTableCell } from '../../../core/ui/c-table/c-table-config';

@Component({
  selector: 'app-type-food-action-col',
  template: `
    <button title="Remove"
            class="btn btn-link px-0 text-decoration-none"
            mwlConfirmationPopover placement="left" [appendToBody]="true"
            [popoverTitle]="'Confirmation'"
            [popoverMessage]="'Are you sure you want to remove this food type?'"
            (confirm)="extraData.remove('single', row.entity)">
            <i class="fa fa-trash"></i></button>`
})

export class FoodTypeActionColComponent implements CTableCell {
  extraData;
  row;
  column;
}

@Component({
  selector: 'app-food-type-check-action',
  template: `
    <div class="custom-control custom-checkbox">
      <input class="custom-control-input" type="checkbox" name="checkboxA"
          id="food-type-{{row.entity.food_type_id}}"
          [checked]="row.entity.checkedAction"
          (change)="row.entity.checkedAction = !row.entity.checkedAction">
          <label class="custom-control-label" for="food-type-{{row.entity.food_type_id}}"></label>
    </div>
    `
})
export class FoodTypeCheckColComponent implements CTableCell {
  extraData;
  row;
  column;
}


@Component({
  selector: 'app-food-type-action-header',
  template: `
      <div class="custom-control custom-checkbox">
        <input type="checkbox" name="checkboxHeader" class="custom-control-input"
                id="check-1-{{extraData.getAllCheckStatus()}}"
               [checked]="extraData.getAllCheckStatus()"
               (change)="extraData.setActionAll()">
               <label class="custom-control-label custom-lead cursor-pointer" for="check-1-{{extraData.getAllCheckStatus()}}"></label>
      </div>
  `
})
export class FoodTypeActionHeaderColComponent implements CTableCell {
  extraData;
  row;
  column;
}
