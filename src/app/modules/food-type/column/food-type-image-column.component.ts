import {AfterContentChecked, Component} from '@angular/core';
import { CTableCell } from '../../../core/ui/c-table/c-table-config';

@Component({
  selector: 'app-food-type-image-col',
  template: `
      <img class="rounded-circle shadow" height="38px" width="38px"
           src="{{row.entity.image ? 'https://uit-canteen.s3-ap-southeast-1.amazonaws.com' +
             row.entity.image : \'./assets/assets/images/food-image.png\'}}"
           onError="this.src='./assets/assets/images/food-image.png';">
  `
})

export class FoodTypeImageColComponent implements CTableCell {
  extraData;
  row;
  column;
  link = 'https://uit-canteen.s3-ap-southeast-1.amazonaws.com';
}


