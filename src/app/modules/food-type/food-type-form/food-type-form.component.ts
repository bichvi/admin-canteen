import {Component, OnInit, OnDestroy, AfterContentChecked} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { FoodService } from 'src/app/services/food.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import AppHelper from 'src/app/helpers/app.helper';
import {startWith, takeUntil, tap} from 'rxjs/operators';
import {CategoryService} from '../../../services/category.service';
import {Subject} from 'rxjs';
import {ToastrService} from 'ngx-toastr';
@Component({
  selector: 'app-food-type-form',
  templateUrl: './food-type-form.component.html',
})
export class FoodTypeFormComponent implements OnInit, OnDestroy {

  form = new FormGroup({});
  model: any = {};
  public foodType;
  options: FormlyFormOptions = {};
  public imageSrc: string;
  onDestroy$ = new Subject<void>();
  fields: FormlyFieldConfig[] = [
    {
      fieldGroupClassName: 'row',
      fieldGroup: [
        {
          className: 'col-xl-6 col-lg-6 col-12',
          key: 'food_type_name',
          type: 'input',
          templateOptions: {
            label: 'Tên loại thực phẩm',
            placeholder: 'Nhập tên loại thực phẩm',
            required: true,
          },
        },
        {
          className: 'col-xl-4 col-lg-6 col-12',
          type: 'select',
          key: 'food_category_id',
          templateOptions: {
            label: 'Danh mục',
            placeholder: 'Chọn danh mục',
            required: true,
            options: [],
          },
          hooks: {
            onInit: (field) => {
              this.categoryService.getCategoryList().toPromise()
                .then((res) => {
                  field['templateOptions'].options = Array.from(res.data, item => {
                    // this.category.push(item);
                    return {label: item['food_category_name'], value: item['food_category_id']};
                  });
                  field.formControl.setValue(field.model[field.key]);
                });
            },
            onChanges: (field) => {
              field.formControl.valueChanges.pipe(
                takeUntil(this.onDestroy$),
                startWith(field.formControl.value),
                tap(value => {
                }),
              ).subscribe();
            },
          },
        },
      ]
    }
  ];

  constructor(
    private foodService: FoodService,
    private categoryService: CategoryService,
    public activeModal: NgbActiveModal,
    private toastr: ToastrService) { }

  ngOnInit() {
    if (this.model.image) {
      console.log(this.imageSrc);
      this.imageSrc = 'https://uit-canteen.s3-ap-southeast-1.amazonaws.com' + this.model.image;
      // this.foodService.getFoodTypeList();
    }
    this.foodType = AppHelper.deepClone(this.model);
  }
  handleInputChange(e) {
    const file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    const pattern = /image-*/;
    const reader = new FileReader();
    if (!file.type.match(pattern)) {
      this.toastr.error('Sai định dạng', 'Lỗi');
      return;
    }
    reader.onload = this._handleReaderLoaded.bind(this);
    reader.readAsDataURL(file);
  }
  _handleReaderLoaded(e) {
    const reader = e.target;
    this.imageSrc = reader.result;
    this.model.image_data = reader.result;
  }

  createFoodType() {
    if (this.form.valid) {
      // tslint:disable-next-line:max-line-length
      const result = this.foodType.food_type_id !== 0 ?
        this.foodService.updateFoodType(this.model, this.foodType.food_type_id) :
        this.foodService.updateFoodType(this.model);
      result.toPromise().then((response) => {
        if (response.status === 1) {
          this.activeModal.close();
          this.toastr.success(response.message, 'Success');
          this.foodService.getFoodTypeList();
          this.foodService.publish(FoodService.EVENT_REFRESH_FOOD_TYPE_LIST, response['data']);
          this.options.resetModel();
        } else {
          this.toastr.error(response.message, 'Lỗi', {
            enableHtml: true
          });
        }
      });
    }
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

}
