import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';;
import { FoodTypeListComponent } from './food-type-list/food-type-list.component';

const routes: Routes = [
  { path : 'food-type', component : FoodTypeListComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FoodTypeRoutingModule { }
