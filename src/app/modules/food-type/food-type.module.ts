import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FoodTypeRoutingModule } from './food-type-routing.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { CoreModule } from 'src/app/core/core.module';
import { HttpModule } from '@angular/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { FormlyModule } from '@ngx-formly/core';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { FoodTypeFormComponent } from './food-type-form/food-type-form.component';
// tslint:disable-next-line:max-line-length
import { FoodTypeCheckColComponent, FoodTypeActionHeaderColComponent, FoodTypeActionColComponent} from './column/food-type-action-column.component';
import { FoodTypeNameColComponent } from './column/food-type-name-column.component';
import { FoodTypeListComponent } from './food-type-list/food-type-list.component';
import {FoodTypeImageColComponent} from './column/food-type-image-column.component';

@NgModule({
  // tslint:disable-next-line:max-line-length
  declarations: [
    FoodTypeFormComponent,
    FoodTypeListComponent,
    FoodTypeActionColComponent,
    FoodTypeCheckColComponent,
    FoodTypeActionHeaderColComponent,
    FoodTypeNameColComponent,
    FoodTypeImageColComponent
  ],
  // tslint:disable-next-line:max-line-length
  entryComponents: [
    FoodTypeFormComponent,
    FoodTypeListComponent,
    FoodTypeActionColComponent,
    FoodTypeCheckColComponent,
    FoodTypeActionHeaderColComponent,
    FoodTypeNameColComponent,
    FoodTypeImageColComponent
  ],
  imports: [
    CommonModule,
    FoodTypeRoutingModule,
    ReactiveFormsModule,
    BrowserModule,
    CoreModule,
    FormsModule,
    HttpModule,
    NgbModule,
    FormlyBootstrapModule,
    FormlyModule.forRoot({
      validationMessages: [
        { name: 'required', message: 'Trường này là bắt buộc' },
      ],
    }),
    ConfirmationPopoverModule.forRoot(),
  ]
})
export class FoodTypeModule { }
