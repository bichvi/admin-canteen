import { Component } from '@angular/core';
import { CTableCell } from '../../../core/ui/c-table/c-table-config';

@Component({
  selector: 'app-food-name-col',
  template: `
  <a style=" color: #005192 ;" (click)="extraData.openDetails(row.entity)" title="{{row.entity.food_name}}">{{row.entity.food_name}}</a>
    `
})

export class FoodNameColComponent implements CTableCell {
  extraData;
  row;
  column;
}


