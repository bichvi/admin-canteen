import {Component, OnInit, OnDestroy, AfterViewInit} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { FoodService } from 'src/app/services/food.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import AppHelper from 'src/app/helpers/app.helper';
import {CategoryService} from '../../../services/category.service';
import {startWith, takeUntil, tap} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {ToastrService} from 'ngx-toastr';
import {MaterialService} from '../../../services/material.service';

@Component({
  selector: 'app-food-form',
  templateUrl: './food-form.component.html',
  styleUrls: ['./food-form.component.css']
})
export class FoodFormComponent implements OnInit, OnDestroy {
  constructor(
    private foodService: FoodService,
    private categoryService: CategoryService,
    private materialService: MaterialService,
    public activeModal: NgbActiveModal,
    private toastr: ToastrService) { }
  form = new FormGroup({});
  model: any = {};
  category = [];
  public food;
  public imageSrc: string;
  onDestroy$ = new Subject<void>();
  options: FormlyFormOptions = {};

  fields: FormlyFieldConfig[] = [
    {
      fieldGroupClassName: 'row',
      fieldGroup: [
        {
          className: 'col-xl-6 col-lg-6 col-12',
          type: 'select',
          key: 'food_category_id',
          templateOptions: {
            label: 'Danh mục',
            placeholder: 'Chọn danh mục',
            options: [],
          },
          hooks: {
            onInit: (field) => {
              this.categoryService.getCategoryList().toPromise()
                .then((res) => {
                  field['templateOptions'].options = Array.from(res.data, item => {
                    // this.category.push(item);
                    return {label: item['food_category_name'], value: item['food_category_id']};
                  });
                  field.formControl.setValue(field.model[field.key]);
                });
            },
            onChanges: (field) => {
              field.formControl.valueChanges.pipe(
                takeUntil(this.onDestroy$),
                startWith(field.formControl.value),
                tap(value => {
                }),
              ).subscribe();
            },
          },
        },
        {
          className: 'col-xl-6 col-lg-6 col-12',
          type: 'select',
          key: 'food_type_id',
          templateOptions: {label: 'Loại thực phẩm', placeholder: 'Chọn loại thực phẩm', required: true,  options: []},
          hooks: {
            onInit: (field) => {
              this.foodService.getFoodTypeList().toPromise()
                .then((res) => {
                  field['templateOptions'].options = Array.from(res.data, item => {
                    // this.category.push(item);
                    return {label: item['food_type_name'], value: item['food_type_id']};
                  });
                  field.formControl.setValue(field.model[field.key]);
                });
            },
            onChanges: (field) => {
              field.formControl.valueChanges.pipe(
                takeUntil(this.onDestroy$),
                startWith(field.formControl.value),
                tap(value => {
                }),
              ).subscribe();
            },
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'row',
      fieldGroup: [
        {
          className: 'col-xl-6 col-lg-6 col-12',
          type: 'input', key: 'food_name',
          templateOptions: {label: 'Tên thực phẩm', placeholder: 'Nhập tên thực phẩm', required: true},
        },
        {
          className: 'col-xl-6 col-lg-6 col-12',
          type: 'input', key: 'quantity',
          templateOptions: {label: 'Số lượng', placeholder: 'Nhập Số lượng', required: true},
        },
      ],
    },
    {
      fieldGroupClassName: 'row',
      fieldGroup: [
        {
          className: 'col-xl-6 col-lg-6 col-12',
          type: 'input', key: 'price',
          templateOptions: {label: 'Giá', placeholder: 'Nhập giá', required: true},
        },
        {
          className: 'col-xl-6 col-lg-6 col-12',
          type: 'input', key: 'price_discount',
          templateOptions: {label: 'Giá giảm', placeholder: 'Nhập giá đã giảm'},
        },
      ],
    },
    {
      fieldGroupClassName: 'row',
      fieldGroup: [
        {
          className: 'col-xl-12 col-lg-12 col-12',
          key: 'material_info',
          type: 'formly-repeat-section',
          fieldArray: {
            fieldGroupClassName: 'row',
            templateOptions: {
              btnText: 'Thêm nguyên liệu',
              showRemove: true
            },
            fieldGroup: [
              {
                className: 'col-sm-6',
                type: 'select',
                key: 'material_id',
                templateOptions: {
                  label: 'Nguyên liệu',
                  placeholder: 'Chọn nguyên liệu',
                  options: [],
                },
                hooks: {
                  onInit: (field) => {
                    this.materialService.getMaterialList().toPromise()
                      .then((res) => {
                        field['templateOptions'].options = Array.from(res.data, item => {
                          console.log(item);
                          return {label: item['material_name'], value: item['material_id']};
                        });
                        field.formControl.setValue(field.model[field.key]);
                      });
                  },
                  onChanges: (field) => {
                    field.formControl.valueChanges.pipe(
                      takeUntil(this.onDestroy$),
                      startWith(field.formControl.value),
                      tap(value => {
                      }),
                    ).subscribe();
                  },
                },
              },
              {
                type: 'input', key: 'mass', className: 'col-sm-6',
                templateOptions: {label: 'Khối lượng', placeholder: 'Nhập khối lượng'},
              }
            ],
          },
        },
      ],
    }
  ];


  ngOnInit() {
    if (this.model.image) {
      this.imageSrc = 'https://uit-canteen.s3-ap-southeast-1.amazonaws.com' + this.model.image;
    }
    this.food = AppHelper.deepClone(this.model);
  }
  handleInputChange(e) {
    const file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    const pattern = /image-*/;
    const reader = new FileReader();
    if (!file.type.match(pattern)) {
      this.toastr.error('Sai định dạng', 'Lỗi');
      return;
    }
    reader.onload = this._handleReaderLoaded.bind(this);
    reader.readAsDataURL(file);
  }
  _handleReaderLoaded(e) {
    const reader = e.target;
    this.imageSrc = reader.result;
    this.model.image_data = reader.result;
  }

  createFood() {
    if (this.form.valid) {
      const result = this.food.food_id !== 0 ?
        this.foodService.updateFood(this.model, this.food.food_id) :
        this.foodService.updateFood(this.model);
      result.toPromise().then((response) => {
        if (response.status === 1) {
          this.activeModal.close();
          this.toastr.success(response.message, 'Thành công');
          this.foodService.publish(FoodService.EVENT_REFRESH_FOOD_LIST, response.data);
          this.options.resetModel();
        } else {
          this.toastr.error(response.message, 'Lỗi', {
            enableHtml: true
          });
        }
      });
    }
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

}
