import {Component, ComponentFactory, ComponentFactoryResolver, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {Location} from '@angular/common';
import {FoodListComponent} from '../food-list/food-list.component';
import {FoodFormComponent} from '../food-form/food-form.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {FoodService} from '../../../services/food.service';


@Component({
  selector: 'app-food-management',
  templateUrl: 'food-management.component.html',
})
export class FoodManagementComponent implements OnInit {
  // @ts-ignore
  @ViewChild('tab0', {read: ViewContainerRef, static: true})
  public allTab: ViewContainerRef;

  // @ts-ignore
  @ViewChild('tab1', {read: ViewContainerRef, static: true})
  public departmentTab;

  // @ts-ignore
  @ViewChild('tab2', {read: ViewContainerRef, static: true})
  public stockTab;

  public tab: number;
  public keyWord = '';

  private foodListCompFactory: ComponentFactory<FoodListComponent>;
  private mappings = {};

  constructor(private activatedRoute: ActivatedRoute,
              private componentFactoryResolver: ComponentFactoryResolver,
              private location: Location,
              private router: Router,
              private foodService: FoodService,
              private modalService: NgbModal) {

    this.activatedRoute = activatedRoute;
    this.foodListCompFactory = this.componentFactoryResolver.resolveComponentFactory(FoodListComponent);
    this.mappings['all'] = ['allTab', 'all', 0];
    this.mappings['department'] = ['departmentTab', 'department', 1];
    this.mappings['stock'] = ['stockTab', 'stock', 2];
  }

  viewDetail(model) {
    // apps.navigateRelated('./' + GuaranteeModel.getModelId(model));
  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      if (params['tab'] !== 'all') {
        this.resolveTab('all');
      }

      this.resolveTab(params['tab']);
    });
  }

  public resolveTab(tab: string) {
    tab = this.mappings[tab] ? tab : 'all';
    this.tab = this.mappings[tab][2];
    this.generateComponent(this[this.mappings[tab][0]], this.mappings[tab][1], tab);

    this.router.navigate(
      [],
      {
        preserveFragment: true,
        queryParamsHandling: 'merge',
        queryParams: {
          'tab': tab
        }
      }).then(r => console.log(r));
  }

  public selectTab(tab: string) {
    this.resolveTab(tab);
  }

  public generateComponent(viewContainerRef: ViewContainerRef, optionsStatus?: string, params?: any) {
    if (viewContainerRef.length) {
      return;
    }

    const componentRef = viewContainerRef.createComponent(this.foodListCompFactory);
    componentRef.instance['is_published'] = params;

  }

  openGroupForm() {
    this.modalService.open(FoodFormComponent, {size: 'lg', centered: true, backdrop: 'static'});
    // modalRef.componentInstance.emitClosePopup.subscribe((next) => {
    //   // this.listGroup();
    // });
  }

  // refreshTable() {
  //   this.foodService.publish(FoodService.EVENT_REFRESH, this.keyWord);
  // }
}
