import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FoodRoutingModule } from './food-routing.module';
import { FoodListComponent } from './food-list/food-list.component';
import { FoodFormComponent } from './food-form/food-form.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { CoreModule } from 'src/app/core/core.module';
import { HttpModule } from '@angular/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { FormlyModule } from '@ngx-formly/core';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { FoodActionHeaderColComponent, FoodCheckColComponent } from './column/food-action-column.component';
import { FoodNameColComponent } from './column/food-name-column.component';
import {FoodImageColComponent} from './column/food-image-column.component';
import {FoodManagementComponent} from './food-management/food-management.component';
import {FormlyMultiCheckboxComponent} from '../role/multicheckbox.type';
import {MultiSelectCheckboxComponent} from '../role/multiselect.type';
import {RepeatTypeComponent} from './repeat-section.type';

export function minlengthValidationMessages(err, field) {
  return `Should have at least ${field.templateOptions.minLength} characters`;
}

@NgModule({
  // tslint:disable-next-line:max-line-length
  declarations: [
    FoodListComponent,
    FoodManagementComponent,
    FoodFormComponent,
    FoodNameColComponent,
    FoodActionHeaderColComponent,
    RepeatTypeComponent,
    FoodCheckColComponent,
    FoodImageColComponent
  ],
  // tslint:disable-next-line:max-line-length
  entryComponents: [
    FoodManagementComponent,
    FoodListComponent,
    FoodFormComponent,
    FoodNameColComponent,
    FoodActionHeaderColComponent,
    FoodCheckColComponent,
    FoodImageColComponent,
    RepeatTypeComponent
  ],
  imports: [
    CommonModule,
    FoodRoutingModule,
    ReactiveFormsModule,
    BrowserModule,
    CoreModule,
    FormsModule,
    HttpModule,
    NgbModule,
    FormlyBootstrapModule,
    FormlyModule.forRoot({
      validationMessages: [
        { name: 'required', message: 'Trường này là bắt buộc' },
        { name: 'minlength', message: minlengthValidationMessages },
      ],
      types: [
        {name: 'formly-repeat-section', component: RepeatTypeComponent}
      ],
    }),
    ConfirmationPopoverModule.forRoot(),
  ]
})
export class FoodModule { }
