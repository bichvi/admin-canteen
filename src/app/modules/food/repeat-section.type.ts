import { Component } from '@angular/core';
import { FieldArrayType } from '@ngx-formly/core';

@Component({
  selector: 'formly-repeat-section',
  template: `
      <div *ngIf="field?.fieldArray?.templateOptions?.btnText">
          <button title="{{field.fieldArray.templateOptions.btnText}}" class="btn btn-primary mb-3" [attr.disabled]="field?.fieldArray?.templateOptions?.disable"
                  type="button" (click)="add()"><i class="fas fa-plus pr-2"></i>{{field.fieldArray.templateOptions.btnText}}</button>
      </div>
      <div *ngFor="let fieldItem of field.fieldGroup; let i = index;" class="card mb-3 border-0 shadow" style="background: #fafafa;">
          <div class="card-body position-relative p-3">
              <formly-group [field]="fieldItem">
                  <div class="card-option" style="margin-top: 5px; margin-right: 8px" *ngIf="field?.fieldArray?.templateOptions?.showRemove">
                      <button class="btn btn-sm btn-danger" (click)="remove(i)">Xóa</button>
<!--                      <a class="d-inline-block cursor-pointer" (click)="remove(i)">-->
<!--                          <i class="fa fa-trash mr-2"> Xóa</i>-->
<!--                      </a>-->
                  </div>
              </formly-group>
          </div>
      </div>
  `,
})
export class RepeatTypeComponent extends FieldArrayType {
  constructor() {
    super();
  }
}
