import {AfterContentChecked, Component} from '@angular/core';
import { CTableCell } from '../../../core/ui/c-table/c-table-config';

@Component({
  selector: 'app-material-type-image-col',
  template: `
      <img class="rounded-circle shadow" height="38px" width="38px"
           src="{{row.entity.image ? 'https://uit-canteen.s3-ap-southeast-1.amazonaws.com' +
             row.entity.image : \'./assets/assets/images/material-image.png\'}}"
           onError="this.src='./assets/assets/images/material-image.png';">
  `
})

export class MaterialTypeImageColComponent implements CTableCell {
  extraData;
  row;
  column;
  link = 'https://uit-canteen.s3-ap-southeast-1.amazonaws.com';
}


