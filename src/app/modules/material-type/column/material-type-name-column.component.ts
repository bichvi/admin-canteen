import { Component } from '@angular/core';
import { CTableCell } from '../../../core/ui/c-table/c-table-config';

@Component({
  selector: 'app-material-type-name-col',
  template: `
  <a style=" color: #005192 ;" (click)="extraData.openDetails(row.entity)" title="{{row.entity.material_type_name}}">{{row.entity.material_type_name}}</a>
    `
})

export class MaterialTypeNameColComponent implements CTableCell {
  extraData;
  row;
  column;
}


