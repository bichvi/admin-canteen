import {Component, OnInit, OnDestroy, AfterContentChecked} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { MaterialService } from 'src/app/services/material.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import AppHelper from 'src/app/helpers/app.helper';
import {startWith, takeUntil, tap} from 'rxjs/operators';
import {CategoryService} from '../../../services/category.service';
import {Subject} from 'rxjs';
import {ToastrService} from 'ngx-toastr';
@Component({
  selector: 'app-material-type-form',
  templateUrl: './material-type-form.component.html',
})
export class MaterialTypeFormComponent implements OnInit, OnDestroy {

  form = new FormGroup({});
  model: any = {};
  public materialType;
  options: FormlyFormOptions = {};
  public imageSrc: string;
  onDestroy$ = new Subject<void>();
  fields: FormlyFieldConfig[] = [
    {
      fieldGroupClassName: 'row',
      fieldGroup: [
        {
          className: 'col-xl-6 col-lg-6 col-12',
          key: 'material_type_name',
          type: 'input',
          templateOptions: {
            label: 'Tên loại nguyên vật liệu',
            placeholder: 'Nhập tên loại nguyên vật liệu',
            required: true,
          },
        },
      ]
    }
  ];

  constructor(
    private materialService: MaterialService,
    private categoryService: CategoryService,
    public activeModal: NgbActiveModal,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.materialType = AppHelper.deepClone(this.model);
  }
  createMaterialType() {
      const result = this.materialType.material_type_id !== 0 ?
        this.materialService.updateMaterialType(this.model, this.materialType.material_type_id) :
        this.materialService.updateMaterialType(this.model);
      result.toPromise().then((response) => {
        if (response.status === 1) {
          this.activeModal.close();
          this.toastr.success(response.message, 'Success');
          this.materialService.getMaterialTypeList();
          this.materialService.publish(MaterialService.EVENT_REFRESH_MATERIAL_TYPE_LIST, response['data']);
          this.options.resetModel();
        } else {
          this.toastr.error(response.message, 'Lỗi', {
            enableHtml: true
          });
        }
      });
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

}
