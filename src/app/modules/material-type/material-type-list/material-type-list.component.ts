import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild } from '@angular/core';
import { CTableComponent, CTableConfig } from 'src/app/core/ui/c-table/c-table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MaterialService } from 'src/app/services/material.service';
import { MaterialTypeCheckColComponent, MaterialTypeActionHeaderColComponent } from '../column/material-type-action-column.component';
import { MaterialTypeNameColComponent } from '../column/material-type-name-column.component';
import { MaterialTypeFormComponent } from '../material-type-form/material-type-form.component';
import {MaterialTypeImageColComponent} from '../column/material-type-image-column.component';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-material-type-list',
  templateUrl: './material-type-list.component.html',
})
export class MaterialTypeListComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('materialTypeTable') materialTypeTable: CTableComponent;

  public tableDefaultConfig = {
    useLocalTable: true,
    tableTitle: 'Danh sách loại nguyên vật liệu',
    tableClass: 'table table-striped table-borderless mb-0',
    useSearch: true,
    searchOptions: {
      inputVisible: true,
      placeHolderText: 'Tìm kiếm theo tên',
    },
    pagingOptions: {
      showTotalRecords: true,
      pageSizes: [5, 10, 15, 20],
      pageSize: 10,
      currentPage: 1
    },
  };


  public materialTypes = [];
  // tslint:disable-next-line:ban-types
  allChecked: Boolean = false;

  eventSubscription = null;

  tableOptions: CTableConfig = Object.assign({
    columnDefs: [
      {
        displayName: '',
        cellComponent: MaterialTypeCheckColComponent,
        headerCellComponent: MaterialTypeActionHeaderColComponent,
        cellHeaderClass: 'text-center'
      },
      {
        displayName: 'STT',
        field: 'no',
      },
      {
        displayName: 'Tên loại nguyên vật liệu',
        field: 'material_type_name',
        cellClass: 'font-weight',
        cellComponent: MaterialTypeNameColComponent,
      }
    ],
    extraData: {
      remove: () => {
          const listId = [];
          this.materialTypes.forEach(obj => {
            if (obj.checkedAction) {
              listId.push(obj.material_type_id);
            }
          });

          if (!listId.length) {
            this.toastr.error('Vui lòng chọn loại nguyên vật liệu để xóa', 'Lỗi');
            return;
          }

          this.materialService.deleteMultiMaterialType({
            material_type_ids: listId,
          }).subscribe((e: any) => {
            this.materialTypeTable.refresh();
            this.allChecked = false;
            if (e.status === 0) {
              this.toastr.warning(e.message, 'Lỗi');
            } else {
              this.toastr.success(e.message, 'Thành công');
            }
          });
      },
      openDetails: (data) => {
        this.openMaterialTypeModal(data);
      },
      setActionAll: () => {
        this.allChecked = !!!this.allChecked;
        this.materialTypes.forEach(obj => {
          obj.checkedAction = this.allChecked;
        });
      },
      getAllCheckStatus: () => {
        return this.allChecked;
      }
    },
    refresh: (params, table) => {
      this.materialService.getMaterialTypeList().subscribe((res: any) => {
        if (res.status === 1 && res.data) {
          this.materialTypes = res.data;
          let startNo = (params._page * 10) - 9;
          res.data.forEach((item: any) => {
            item.no = startNo;
            ++startNo ;
          });

          table.setData(res.data);
        }
      });
    }
  }, this.tableDefaultConfig);

  constructor(private materialService: MaterialService,  private modalService: NgbModal,  private toastr: ToastrService) { }

  ngOnInit() {
    this.materialService.subscribe(MaterialService.EVENT_REFRESH_MATERIAL_TYPE_LIST, () => {
      this.listMaterialType();
    });
  }

  ngAfterViewInit() {
    this.materialTypeTable.refresh();
  }

  listMaterialType() {
    this.materialService.getMaterialTypeList()
    .subscribe(materialTypes => {
        this.materialTypes = materialTypes.data;
    });
  }
  openMaterialTypeModal(materialType = null) {
    const modalRef = this.modalService.open(MaterialTypeFormComponent, {size: 'lg', centered: true, backdrop: 'static'});
    modalRef.componentInstance.model = materialType ? materialType : {};
  }

  ngOnDestroy() {
    this.materialService.unsubscribe(MaterialService.EVENT_REFRESH_MATERIAL_TYPE_LIST);
  }
}
