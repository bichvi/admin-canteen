import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';;
import { MaterialTypeListComponent } from './material-type-list/material-type-list.component';

const routes: Routes = [
  { path : 'material-type', component : MaterialTypeListComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MaterialTypeRoutingModule { }
