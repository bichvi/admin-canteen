import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaterialTypeRoutingModule } from './material-type-routing.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { CoreModule } from 'src/app/core/core.module';
import { HttpModule } from '@angular/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { FormlyModule } from '@ngx-formly/core';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { MaterialTypeFormComponent } from './material-type-form/material-type-form.component';
// tslint:disable-next-line:max-line-length
import { MaterialTypeCheckColComponent, MaterialTypeActionHeaderColComponent, MaterialTypeActionColComponent} from './column/material-type-action-column.component';
import { MaterialTypeNameColComponent } from './column/material-type-name-column.component';
import { MaterialTypeListComponent } from './material-type-list/material-type-list.component';
import {MaterialTypeImageColComponent} from './column/material-type-image-column.component';

@NgModule({
  // tslint:disable-next-line:max-line-length
  declarations: [
    MaterialTypeFormComponent,
    MaterialTypeListComponent,
    MaterialTypeActionColComponent,
    MaterialTypeCheckColComponent,
    MaterialTypeActionHeaderColComponent,
    MaterialTypeNameColComponent,
    MaterialTypeImageColComponent
  ],
  // tslint:disable-next-line:max-line-length
  entryComponents: [
    MaterialTypeFormComponent,
    MaterialTypeListComponent,
    MaterialTypeActionColComponent,
    MaterialTypeCheckColComponent,
    MaterialTypeActionHeaderColComponent,
    MaterialTypeNameColComponent,
    MaterialTypeImageColComponent
  ],
  imports: [
    CommonModule,
    MaterialTypeRoutingModule,
    ReactiveFormsModule,
    BrowserModule,
    CoreModule,
    FormsModule,
    HttpModule,
    NgbModule,
    FormlyBootstrapModule,
    FormlyModule.forRoot({
      validationMessages: [
        { name: 'required', message: 'Trường này là bắt buộc' },
      ],
    }),
    ConfirmationPopoverModule.forRoot(),
  ]
})
export class MaterialTypeModule { }
