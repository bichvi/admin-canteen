import { Component } from '@angular/core';
import { CTableCell } from '../../../core/ui/c-table/c-table-config';

@Component({
  selector: 'app-catgory-name-col',
  template: `
  <a style=" color: #005192 ;" (click)="extraData.openDetails(row.entity)" title="{{row.entity.material_name}}">{{row.entity.material_name}}</a>
    `
})

export class MaterialNameColComponent implements CTableCell {
  extraData;
  row;
  column;
}


