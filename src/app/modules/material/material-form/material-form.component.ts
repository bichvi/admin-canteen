import {Component, OnInit, ViewChild, ElementRef, OnDestroy} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { MaterialService } from 'src/app/services/material.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import AppHelper from 'src/app/helpers/app.helper';
import { ToastrService } from 'ngx-toastr';
import {startWith, takeUntil, tap} from 'rxjs/operators';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-material-form',
  templateUrl: './material-form.component.html',
  styleUrls: ['./material-form.component.css']
})
export class MaterialFormComponent implements OnInit, OnDestroy {
  form = new FormGroup({});
  model: any = {};
  public material;
  public imageSrc: string;
  onDestroy$ = new Subject<void>();
  options: FormlyFormOptions = {};
  fields: FormlyFieldConfig[] = [
    {
      fieldGroupClassName: 'row',
      fieldGroup: [
        {
          className: 'col-xl-4 col-lg-6 col-12',
          type: 'input', key: 'material_name',
          templateOptions: {label: 'Tên nguyên vật liệu', placeholder: 'Nhập tên nguyên vật liệu', required: true},
        },
        {
          className: 'col-xl-4 col-lg-6 col-12',
          type: 'input', key: 'material_price',
          templateOptions: {label: 'Giá nguyên vật liệu', placeholder: 'Nhập giá nguyên vật liệu', required: true},
        }
      ],
    },
    {
      fieldGroupClassName: 'row',
      fieldGroup: [
        {
          className: 'col-xl-4 col-lg-6 col-12',
          type: 'select',
          key: 'material_type_id',
          templateOptions: {label: 'Loại nguyên vật liệu', placeholder: 'Chọn nguyên vật liệu', required: true,  options: []},
          hooks: {
            onInit: (field) => {
              this.materialService.getMaterialTypeList().toPromise()
                .then((res) => {
                  field['templateOptions'].options = Array.from(res.data, item => {
                    // this.category.push(item);
                    return {label: item['material_type_name'], value: item['material_type_id']};
                  });
                  field.formControl.setValue(field.model[field.key]);
                });
            },
            onChanges: (field) => {
              field.formControl.valueChanges.pipe(
                takeUntil(this.onDestroy$),
                startWith(field.formControl.value),
                tap(value => {
                }),
              ).subscribe();
            },
          },
        },
      ],
    }
  ];
  constructor(
    private materialService: MaterialService,
    public activeModal: NgbActiveModal,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.material = AppHelper.deepClone(this.model);
  }
  createMaterial() {
    const result = this.material.material_id !== 0 ?
        this.materialService.updateMaterial(this.model, this.material.material_id) :
        this.materialService.updateMaterial(this.model);
    result.toPromise().then((response) => {
        if (response.status === 1) {
          this.activeModal.close();
          this.materialService.publish(MaterialService.EVENT_REFRESH_MATERIAL_LIST, response.data);
          this.toastr.success(response.message, 'Thành công');
          this.options.resetModel();
        } else {
          this.toastr.error(response.message);
        }
      });
    }
  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}

