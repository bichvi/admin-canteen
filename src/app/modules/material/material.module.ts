import { NgModule } from '@angular/core';

import { MaterialRoutingModule } from './material-routing.module';
import { MaterialListComponent } from './material-list/material-list.component';
import { MaterialFormComponent } from './material-form/material-form.component';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { FormlyModule } from '@ngx-formly/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '../common/common.module';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { CoreModule } from 'src/app/core/core.module';
import { MaterialActionColComponent, MaterialCheckColComponent, MaterialActionHeaderColComponent } from './columns/material-action-column.component';
import { MaterialNameColComponent } from './columns/material-name-column.component';
import { FormlyFieldFile } from './file-type.component';
import { FileValueAccessor } from './file-value-accessor';
import {MaterialImageColComponent} from './columns/material-image-column.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations: [MaterialListComponent, MaterialFormComponent, MaterialActionColComponent, MaterialNameColComponent, MaterialCheckColComponent, MaterialActionHeaderColComponent, MaterialImageColComponent, FormlyFieldFile, FileValueAccessor],
  entryComponents: [MaterialListComponent, MaterialFormComponent, MaterialActionColComponent, MaterialNameColComponent, MaterialCheckColComponent, MaterialActionHeaderColComponent, MaterialImageColComponent],
  imports: [
    MaterialRoutingModule,
    CommonModule,
    ReactiveFormsModule,
    BrowserModule,
    CoreModule,
    FormsModule,
    HttpModule,
    NgbModule,
    FormlyBootstrapModule,
    FormlyModule.forRoot({
      validationMessages: [
        { name: 'required', message: 'Trường này là bắt buộc' },
      ],
      types: [
        { name: 'file', component: FormlyFieldFile, wrappers: ['form-field'] },
      ],
    }),
    ConfirmationPopoverModule.forRoot(),
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot() // ToastrModule added
  ]
})
export class MaterialModule { }
