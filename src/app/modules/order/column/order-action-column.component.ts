import { Component } from '@angular/core';
import { CTableCell } from '../../../core/ui/c-table/c-table-config';

@Component({
  selector: 'app-order-check-action',
  template: `
    <div class="custom-control custom-checkbox">
      <input class="custom-control-input" type="checkbox" name="checkboxA"
          id="order-{{row.entity.order_id}}"
          [checked]="row.entity.checkedAction"
          (change)="row.entity.checkedAction = !row.entity.checkedAction">
          <label class="custom-control-label" for="order-{{row.entity.order_id}}"></label>
    </div>
    `
})
export class OrderCheckColComponent implements CTableCell {
  extraData;
  row;
  column;
}


@Component({
  selector: 'app-order-action-header',
  template: `
      <div class="custom-control custom-checkbox">
        <input type="checkbox" name="checkboxHeader" class="custom-control-input"
                id="check-1-{{extraData.getAllCheckStatus()}}"
               [checked]="extraData.getAllCheckStatus()"
               (change)="extraData.setActionAll()">
               <label class="custom-control-label custom-lead cursor-pointer" for="check-1-{{extraData.getAllCheckStatus()}}"></label>
      </div>
  `
})
export class OrderActionHeaderColComponent implements CTableCell {
  extraData;
  row;
  column;
}

@Component({
  selector: 'app-order-action-header',
  template: `
      <countdown-timer [end]="row.entity.time_remaining"></countdown-timer>
  `
})
export class TimeRemainingColComponent implements CTableCell {
  extraData;
  row;
  column;
}

