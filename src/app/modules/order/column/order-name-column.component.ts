import { Component } from '@angular/core';
import { CTableCell } from '../../../core/ui/c-table/c-table-config';

@Component({
  selector: 'app-order-name-col',
  template: `
  <a style=" color: #005192 ;" (click)="extraData.openDetails(row.entity)" title="{{row.entity.order_id}}">{{row.entity.order_id}}</a>
    `
})

export class OrderNameColComponent implements CTableCell {
  extraData;
  row;
  column;
}


