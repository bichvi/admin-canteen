import {Component, OnInit, OnDestroy, AfterViewInit, ViewChild, Input, SimpleChanges, OnChanges} from '@angular/core';
import { CTableComponent, CTableConfig } from 'src/app/core/ui/c-table/c-table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { OrderService } from 'src/app/services/order.service';
import { OrderFormComponent } from '../order-form/order-form.component';
import {OrderNameColComponent} from '../column/order-name-column.component';
import {TimeRemainingColComponent} from '../column/order-action-column.component';

@Component({
  selector: 'app-order-done-list',
  templateUrl: './order-done-list.component.html'
})
export class OrderDoneListComponent implements OnInit, OnDestroy, AfterViewInit{
  @ViewChild('orderDoneTable') orderDoneTable: CTableComponent;
  public tableDefaultConfig = {
    useLocalTable: true,
    tableTitle: 'Danh sách đơn hàng',
    tableClass: 'table table-striped table-borderless mb-0',
    useSearch: true,
    searchOptions: {
      inputVisible: true,
      placeHolderText: 'Tìm kiếm theo tên',
    },
    pagingOptions: {
      showTotalRecords: true,
      pageSizes: [5, 10, 15, 20],
      pageSize: 10,
      currentPage: 1
    },
  };

  public doneOrdersList = [];
  public refreshByParams = false;

  constructor(private orderService: OrderService,  private modalService: NgbModal) { }
  doneTableOptions: CTableConfig = Object.assign({
    columnDefs: [
      {
        displayName: 'ID',
        field: 'order_id',
        cellClass: 'font-weight',
        cellComponent: OrderNameColComponent
      },
      {
        displayName: 'Đơn giá',
        field: 'total_price',
        cellClass: 'font-weight',
      },
      {
        displayName: 'Phương thức thanh toán',
        field: 'payment_method_name',
        cellClass: 'font-weight',
      },
      {
        displayName: 'Trạng thái thanh toán',
        field: 'payment_status_name',
        cellClass: 'font-weight',
      },
      {
        displayName: 'Trạng thái',
        field: 'status_name',
        cellClass: 'font-weight',
      }
    ],
    extraData: {
      openDetails: (data) => {
        this.openOrderModal(data);
      },
    },
    refresh: (params, table) => {
      if (this.refreshByParams){
        table.setData(this.doneOrdersList);
        this.refreshByParams = false;
      } else {
        this.orderService.getDoneList().subscribe((res: any) => {
          if (res.status === 1 && res.data) {
            this.doneOrdersList = res.data;
            table.setData(res.data);
          }
        });
      }
    }
  }, this.tableDefaultConfig);


  ngOnInit() {
    this.orderService.subscribe(OrderService.EVENT_REFRESH_ORDER_LIST, () => {
      this.orderDoneTable.refresh();
    });
    this.orderService.subscribe(OrderService.EVENT_DONE_ORDER_PUSH, ($orderData) => {
      this.refreshByParams = true;
      this.doneOrdersList.unshift($orderData);
      this.orderDoneTable.refresh();
    });
  }

  ngAfterViewInit() {
    this.orderDoneTable.refresh();
  }
  openOrderModal(order = null) {
    const modalRef = this.modalService.open(OrderFormComponent, {size: 'lg', centered: true, backdrop: 'static'});
    modalRef.componentInstance.model = order ? order : {};
  }

  ngOnDestroy() {
    // this.orderService.unsubscribe(OrderService.EVENT_REFRESH_ORDER_LIST);
    // this.orderService.unsubscribe(OrderService.EVENT_CHANGE_ORDER_LIST);
  }
}
