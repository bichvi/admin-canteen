import {Injectable, Component, OnInit, OnDestroy, AfterViewInit, ViewChild, Input} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { OrderService } from 'src/app/services/order.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import AppHelper from 'src/app/helpers/app.helper';
import {startWith, takeUntil, tap} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {ToastrService} from 'ngx-toastr';
import {CTableComponent} from '../../../core/ui/c-table/c-table.component';
import {CTableConfig} from '../../../core/ui/c-table/c-table-config';


@Component({
  selector: 'app-order-form',
  templateUrl: './order-form.component.html',
})
export class OrderFormComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('orderDetailTable')
  orderDetailTable: CTableComponent;
  form = new FormGroup({});
  @Input() model;
  public admins = [] as any;
  public order;
  onDestroy$ = new Subject<void>();
  constructor(
    private orderService: OrderService,
    public activeModal: NgbActiveModal,
    private toastr: ToastrService,
  ) { }
  public tableDefaultConfig = {
    useLocalTable: true,
    tableClass: 'table',
    // useSearch: true,
    // searchOptions: {
    //   inputVisible: true,
    //   placeHolderText: 'Search by name',
    // },
    pagingOptions: {
      showTotalRecords: true,
      pageSizes: [5, 10, 15, 20],
      pageSize: 10,
      currentPage: 1
    },
  };

  public users = [];
  tableOptions: CTableConfig = Object.assign({
    columnDefs: [
      {
        displayName: 'Tên món',
        field: 'food_name',
      },
      {
        displayName: 'Giá ban đầu',
        field: 'price',
      },
      {
        displayName: 'Giá khuyến mãi',
        field: 'price_discount',
      },
      {
        displayName: 'Số lượng',
        field: 'quantity',
      },
      {
        displayName: 'Ghi chú',
        field: 'note',
      }
    ],
    refresh: (params, table) => {
          table.setData(this.order.order_details);
    },
    usePaging: false
  }, this.tableDefaultConfig);


  ngOnInit() {
    this.order = AppHelper.deepClone(this.model);

    // this.orderDetailTable.refresh();
  }
  // createOrder() {
  //   const params = this.role;
  //   const result = this.role.user_group_id !== 0 ?
  //     this.roleService.updateRole(params, this.role.user_group_id) :
  //     this.roleService.updateRole(params);
  //   result.toPromise().then((response) => {
  //     if (response.status === 1) {
  //       if (this.role.user_group_id !== 0) {
  //         Object.assign(this.model, this.role);
  //       }
  //       this.roleService.publish(RoleService.EVENT_REFRESH_ROLE_LIST, response.data);
  //       this.toastr.success(response.message, 'Thành công');
  //     } else {
  //       this.toastr.error(response.message);
  //     }
  //     this.activeModal.close();
  //   });
  // }

  forceFinishOrder() {
    const result = this.orderService.forceFinishOrder({order_id: this.order.order_id});
    result.toPromise().then((response) => {
      if (response.status === 1) {
        this.activeModal.close();
        const oldStatus = this.order.status;
        this.order.status = 101;
        this.order.status_name = 'Hoàn tất';

        switch (oldStatus) {
          case 201:
            this.orderService.publish(OrderService.EVENT_PENDING_ORDER_MOVE, this.order);
            break;
          case 202:
            this.orderService.publish(OrderService.EVENT_ORDER_MOVE, this.order);
            break;
          default:
            break;
        }

        this.toastr.success(response.message, 'Thành công');
      } else {
        this.toastr.error(response.message);
      }
    });
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  ngAfterViewInit() {
    if (this.orderDetailTable) {
      this.orderDetailTable.refresh();
    }
  }
}
