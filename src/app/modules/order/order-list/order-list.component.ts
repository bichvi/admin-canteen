import {Component, OnInit, OnDestroy, AfterViewInit, ViewChild, Input, SimpleChanges, OnChanges} from '@angular/core';
import { CTableComponent, CTableConfig } from 'src/app/core/ui/c-table/c-table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { OrderService } from 'src/app/services/order.service';
import { OrderFormComponent } from '../order-form/order-form.component';
import {OrderNameColComponent} from '../column/order-name-column.component';
import {Socket} from 'ngx-socket-io';
import {TimeRemainingColComponent} from '../column/order-action-column.component';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html'
})
export class OrderListComponent implements OnInit, OnDestroy, AfterViewInit, OnChanges{
  @ViewChild('orderTable') orderTable: CTableComponent;
  @Input() orderModel: any;
  manageOrder;
  public tableDefaultConfig = {
    useLocalTable: true,
    tableTitle: 'Danh sách đơn hàng',
    tableClass: 'table table-striped table-borderless mb-0',
    useSearch: true,
    searchOptions: {
      inputVisible: true,
      placeHolderText: 'Tìm kiếm theo tên',
    },
    pagingOptions: {
      showTotalRecords: true,
      pageSizes: [5, 10, 15, 20],
      pageSize: 10,
      currentPage: 1
    },
  };

  constructor(private orderService: OrderService,  private modalService: NgbModal, private socket: Socket) { }

  public waitingOrdersList = [];
  public refreshByParams = false;

  tableOptions: CTableConfig = Object.assign({
    columnDefs: [
      {
        displayName: 'ID',
        field: 'order_id',
        cellClass: 'font-weight',
        cellComponent: OrderNameColComponent
      },
      {
        displayName: 'Tổng giá',
        field: 'total_price',
        cellClass: 'font-weight',
      },
      {
        displayName: 'Phương thức thanh toán',
        field: 'payment_method_name',
        cellClass: 'font-weight',
      },
      {
        displayName: 'Trạng thái thanh toán',
        field: 'payment_status_name',
        cellClass: 'font-weight',
      },
      {
        displayName: 'Thời gian còn lại',
        field: 'time_remaining',
        cellClass: 'font-weight text-danger',
        cellComponent: TimeRemainingColComponent
      },
      {
        displayName: 'Trạng thái đơn hàng',
        field: 'status_name',
        cellClass: 'font-weight',
      }
    ],
    extraData: {
      openDetails: (data) => {
        this.openOrderModal(data);
      },
    },
    refresh: (params, table) => {
      if (this.refreshByParams){
        table.setData(this.waitingOrdersList);
        this.refreshByParams = false;
      } else {
        this.orderService.getWaitingList().subscribe((res: any) => {
          if (res.status === 1 && res.data) {
            this.waitingOrdersList = res.data;
            table.setData(this.waitingOrdersList);
          }
        });
      }
    }
  }, this.tableDefaultConfig);


  ngOnInit() {
    this.orderService.subscribe(OrderService.EVENT_REFRESH_ORDER_LIST, () => {
      this.orderTable.refresh();
    });
    this.orderService.subscribe(OrderService.EVENT_ORDER_MOVE, (data) => {
      this.refreshByParams = true;
      const index = this.waitingOrdersList.findIndex(element => element.order_id === data.order_id);
      const orderData = this.waitingOrdersList[index];
      this.waitingOrdersList.splice(index, 1);
      this.orderTable.refresh();

      //chuyển đến status
      switch (data.status) {
        case 101:
          orderData.status_name = "Hoàn tất";
          this.orderService.publish(OrderService.EVENT_DONE_ORDER_PUSH, orderData);
          break;
        case 102:
          orderData.status_name = "Đã hủy";
          this.orderService.publish(OrderService.EVENT_DONE_ORDER_PUSH, orderData);
          break;
        case 201:
          orderData.status_name = "Đang xử lý";
          orderData.delivery_place_name = data.delivery_place_name;
          this.orderService.publish(OrderService.EVENT_PENDING_ORDER_PUSH, orderData);
          break;
        default:
          break;
      }
    });

    this.orderService.subscribe(OrderService.EVENT_ORDER_PUSH, ($orderData) => {
      this.refreshByParams = true;
      this.waitingOrdersList.push($orderData);
      this.orderTable.refresh();
    });
  }

  ngAfterViewInit() {
    this.orderTable.refresh();
  }
  openOrderModal(order = null) {
    const modalRef = this.modalService.open(OrderFormComponent, {size: 'lg', centered: true, backdrop: 'static'});
    modalRef.componentInstance.model = order ? order : {};
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.orderModel && changes.orderModel.currentValue && changes.orderModel.currentValue.manageOrder) {
      this.manageOrder = true;
    }
  }

  // loadSocket() {
  //   const currentUser = JSON.parse(localStorage.getItem('currentUser'));
  //   const jwtToken = currentUser.data;
  //   this.socket.emit('init', jwtToken);
  //
  //   this.socket.on('new_order', socketData => {
  //     if (typeof socketData !== 'undefined') {
  //       const data = socketData.data;
  //       this.waitingOrdersList.push(data);
  //       this.refreshByParams = true;
  //       this.orderTable.refresh();
  //     }
  //   });
  // }

  ngOnDestroy() {
    this.orderService.unsubscribe(OrderService.EVENT_REFRESH_ORDER_LIST);
    this.orderService.unsubscribe(OrderService.EVENT_CHANGE_ORDER_LIST);
  }
}
