import {AfterViewInit, Component, ComponentFactory, ComponentFactoryResolver, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {Location} from '@angular/common';
import {OrderListComponent} from '../order-list/order-list.component';
import {OrderFormComponent} from '../order-form/order-form.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {OrderService} from '../../../services/order.service';
import {Socket} from 'ngx-socket-io';
declare var $: any;
@Component({
  selector: 'app-order-management',
  templateUrl: 'order-management.component.html',
})
export class OrderManagementComponent implements OnInit, AfterViewInit {
  @ViewChild(OrderListComponent) orderListComponent;
  clicked = true;
  constructor(private activatedRoute: ActivatedRoute,
              private componentFactoryResolver: ComponentFactoryResolver,
              private location: Location,
              private router: Router,
              private orderService: OrderService,
              private modalService: NgbModal,
              private socket: Socket) {
  }

  viewDetail(model) {
    // apps.navigateRelated('./' + GuaranteeModel.getModelId(model));
  }

  ngOnInit() {
    this.loadSocket();
    $('.abc').click();
    this.orderService.publish(OrderService.EVENT_REFRESH_ORDER_LIST);
  }

  ngAfterViewInit() {
    console.log(this.orderListComponent.orderTable);
  }
  openGroupForm() {
    this.modalService.open(OrderFormComponent, {size: 'lg', centered: true, backdrop: 'static'});
    // modalRef.componentInstance.emitClosePopup.subscribe((next) => {
    //   // this.listGroup();
    // });
  }

  activityTabClickedButton(clicked?) {
    this.clicked = !clicked;
  }

  loadSocket() {
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    const jwtToken = currentUser.data;
    this.socket.emit('init', jwtToken);

    this.socket.on('new_order_handle', socketData => {
      if (typeof socketData !== 'undefined') {
        const data = socketData;
        switch (data.status) {
          case 201:
            this.orderService.publish(OrderService.EVENT_PENDING_ORDER_PUSH, data);
            break;
          case 202:
            this.orderService.publish(OrderService.EVENT_ORDER_PUSH, data);
            break;
          default:
            break;
        }
      }
    });

    this.socket.on('changed_status_order', socketData => {
      if (typeof socketData !== 'undefined') {
        const data = socketData;
        switch (data.old_status) {
          case 201:
            this.orderService.publish(OrderService.EVENT_PENDING_ORDER_MOVE, data);
            break;
          case 202:
            this.orderService.publish(OrderService.EVENT_ORDER_MOVE, data);
            break;
          default:
            break;
        }
      }
    });
  }

  // refreshTable() {
  //   this.orderService.publish(OrderService.EVENT_REFRESH, this.keyWord);
  // }
}
