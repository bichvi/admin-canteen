import {Component, OnInit, OnDestroy, AfterViewInit, ViewChild, Input, SimpleChanges, OnChanges} from '@angular/core';
import { CTableComponent, CTableConfig } from 'src/app/core/ui/c-table/c-table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { OrderService } from 'src/app/services/order.service';
import { OrderFormComponent } from '../order-form/order-form.component';
import {Socket} from 'ngx-socket-io';
import {OrderNameColComponent} from '../column/order-name-column.component';
import {TimeRemainingColComponent} from '../column/order-action-column.component';

@Component({
  selector: 'app-order-pending-list',
  templateUrl: './order-pending-list.component.html'
})
export class OrderPendingListComponent implements OnInit, OnDestroy, AfterViewInit{
  @ViewChild('orderPendingTable') orderPendingTable: CTableComponent;
  public tableDefaultConfig = {
    useLocalTable: true,
    tableTitle: 'Danh sách đơn hàng',
    tableClass: 'table table-striped table-borderless mb-0',
    useSearch: true,
    searchOptions: {
      inputVisible: true,
      placeHolderText: 'Tìm kiếm theo tên',
    },
    pagingOptions: {
      showTotalRecords: true,
      pageSizes: [5, 10, 15, 20],
      pageSize: 10,
      currentPage: 1
    },
  };

  constructor(private orderService: OrderService,  private modalService: NgbModal, private socket: Socket) { }

  public pendingOrdersList = [];
  public refreshByParams = false;

  pendingTableOptions: CTableConfig = Object.assign({
    columnDefs: [
      {
        displayName: 'ID',
        field: 'order_id',
        cellClass: 'font-weight',
        cellComponent: OrderNameColComponent
      },
      {
        displayName: 'Đơn giá',
        field: 'total_price',
        cellClass: 'font-weight',
      },
      {
        displayName: 'Phương thức thanh toán',
        field: 'payment_method_name',
        cellClass: 'font-weight',
      },
      {
        displayName: 'Trạng thái thanh toán',
        field: 'payment_status_name',
        cellClass: 'font-weight',
      },
      {
        displayName: 'Trạng thái',
        field: 'status_name',
        cellClass: 'font-weight',
      },
        {
            displayName: 'Nơi giao hàng',
            field: 'delivery_place_name',
            cellClass: 'font-weight',
        }
    ],
    extraData: {
      openDetails: (data) => {
        this.openOrderModal(data);
      },
    },
    refresh: (params, table) => {
      if (this.refreshByParams){
        table.setData(this.pendingOrdersList);
        this.refreshByParams = false;
      }
      else {
        this.orderService.getPendingList().subscribe((res: any) => {
          if (res.status === 1 && res.data) {
            this.pendingOrdersList = res.data;
            table.setData(res.data);
          }
        });
      }
    }
  }, this.tableDefaultConfig);


  ngOnInit() {
    this.orderService.subscribe(OrderService.EVENT_REFRESH_ORDER_LIST, () => {
      this.orderPendingTable.refresh();
    });
    this.orderService.subscribe(OrderService.EVENT_PENDING_ORDER_MOVE, (data) => {
      this.refreshByParams = true;
      const index = this.pendingOrdersList.findIndex(element => element.order_id == data.order_id);
      const orderData = this.pendingOrdersList[index]; console.log(orderData);
      this.pendingOrdersList.splice(index, 1);
      this.orderPendingTable.refresh();

      switch (data.status) {
        case 101:
          orderData.status_name = "Hoàn tất";
          this.orderService.publish(OrderService.EVENT_DONE_ORDER_PUSH, orderData);
          break;
        case 102:
          orderData.status_name = "Đã hủy";
          this.orderService.publish(OrderService.EVENT_DONE_ORDER_PUSH, orderData);
          break;
        default:
          break;
      }
    });

    this.orderService.subscribe(OrderService.EVENT_PENDING_ORDER_PUSH, (orderData) => {
      this.refreshByParams = true;
      this.pendingOrdersList.unshift(orderData);
      this.orderPendingTable.refresh();
    });

  }

  ngAfterViewInit() {
    this.orderPendingTable.refresh();
  }
  openOrderModal(order = null) {
    const modalRef = this.modalService.open(OrderFormComponent, {size: 'lg', centered: true, backdrop: 'static'});
    modalRef.componentInstance.model = order ? order : {};
  }

  ngOnDestroy() {
    // this.orderService.unsubscribe(OrderService.EVENT_REFRESH_ORDER_LIST);
    // this.orderService.unsubscribe(OrderService.EVENT_CHANGE_ORDER_LIST);
  }
}
