import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderRoutingModule } from './order-routing.module';
import { OrderListComponent } from './order-list/order-list.component';
import { OrderFormComponent } from './order-form/order-form.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { CoreModule } from 'src/app/core/core.module';
import { HttpModule } from '@angular/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { FormlyModule } from '@ngx-formly/core';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import {OrderActionHeaderColComponent, OrderCheckColComponent, TimeRemainingColComponent} from './column/order-action-column.component';
import { OrderNameColComponent } from './column/order-name-column.component';
import {OrderManagementComponent} from './order-management/order-management.component';
import {OrderPendingListComponent} from './order-pending-list/order-pending-list.component';
import {OrderDoneListComponent} from './order-done-list/order-done-list.component';
import { CountdownTimerModule } from 'ngx-countdown-timer';
export function minlengthValidationMessages(err, field) {
  return `Should have at least ${field.templateOptions.minLength} characters`;
}

@NgModule({
  // tslint:disable-next-line:max-line-length
  declarations: [
    OrderListComponent,
    OrderPendingListComponent,
    OrderDoneListComponent,
    OrderManagementComponent,
    OrderFormComponent,
    OrderNameColComponent,
    OrderActionHeaderColComponent,
    OrderCheckColComponent,
    TimeRemainingColComponent
  ],
  // tslint:disable-next-line:max-line-length
  entryComponents: [
    OrderManagementComponent,
    OrderListComponent,
    OrderPendingListComponent,
    OrderDoneListComponent,
    OrderFormComponent,
    OrderNameColComponent,
    OrderActionHeaderColComponent,
    OrderCheckColComponent,
    TimeRemainingColComponent
  ],
  imports: [
    CommonModule,
    OrderRoutingModule,
    ReactiveFormsModule,
    BrowserModule,
    CoreModule,
    FormsModule,
    HttpModule,
    NgbModule,
    CountdownTimerModule.forRoot(),
    FormlyBootstrapModule,
    FormlyModule.forRoot({
      validationMessages: [
        { name: 'required', message: 'Trường này là bắt buộc' },
        { name: 'minlength', message: minlengthValidationMessages },
      ],
    }),
    ConfirmationPopoverModule.forRoot(),
  ],
  exports: [
    OrderListComponent,
    OrderPendingListComponent,
    OrderDoneListComponent,
    OrderManagementComponent,
    OrderFormComponent,
    OrderNameColComponent,
    OrderActionHeaderColComponent,
    OrderCheckColComponent,
    TimeRemainingColComponent
  ],
})
export class OrderModule { }
