import { Component } from '@angular/core';
import { CTableCell } from '../../../core/ui/c-table/c-table-config';

@Component({
  selector: 'app-post-name-col',
  template: `
  <a style=" color: #005192 ;" (click)="extraData.openDetails(row.entity)" title="{{row.entity.post_title}}">{{row.entity.post_title}}</a>
    `
})

export class PostNameColComponent implements CTableCell {
  extraData;
  row;
  column;
}


