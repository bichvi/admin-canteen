import {Component, OnInit, OnDestroy, AfterContentChecked} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { PostService } from 'src/app/services/post.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import AppHelper from 'src/app/helpers/app.helper';
import {Subject} from 'rxjs';
import {ToastrService} from 'ngx-toastr';
@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
})
export class PostFormComponent implements OnInit, OnDestroy {

  form = new FormGroup({});
  model: any = {};
  public post;
  options: FormlyFormOptions = {};
  public imageSrc: string;
  onDestroy$ = new Subject<void>();
  fields: FormlyFieldConfig[] = [
    {
      fieldGroupClassName: 'row',
      fieldGroup: [
        {
          key: 'post_title',
          type: 'input',
          templateOptions: {
            type: 'text',
            label: 'Tiêu đề',
            placeholder: 'Nhập tiêu đề',
            required: true
          }
        },
        {
          key: 'post_content',
          type: 'ckeditor',
          templateOptions: {
            rows: 4,
            label: 'Nội dung',
            placeholder: 'Nhập nôi dung',
            required: true
          }
        }
      ]
    }
  ];

  constructor(
    private postService: PostService,
    public activeModal: NgbActiveModal,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.post = AppHelper.deepClone(this.model);
  }
  createPost() {
    if (this.form.valid) {
      // tslint:disable-next-line:max-line-length
      const result = this.post.post_id !== 0 ?
        this.postService.updatePost(this.model, this.post.post_id) :
        this.postService.updatePost(this.model);
      result.toPromise().then((response) => {
        if (response.status === 1) {
          this.activeModal.close();
          this.toastr.success(response.message, 'Thành công');
          this.postService.getPostList();
          this.postService.publish(PostService.EVENT_REFRESH_POST_LIST, response['data']);
          this.options.resetModel();
        } else {
          this.toastr.error(response.message, 'Lỗi', {
            enableHtml: true
          });
        }
      });
    }
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

}
