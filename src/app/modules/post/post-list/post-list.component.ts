import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild } from '@angular/core';
import { CTableComponent, CTableConfig } from 'src/app/core/ui/c-table/c-table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PostCheckColComponent, PostActionHeaderColComponent } from '../column/post-action-column.component';
import { PostNameColComponent } from '../column/post-name-column.component';
import { PostFormComponent } from '../post-form/post-form.component';
import {ToastrService} from 'ngx-toastr';
import {PostService} from '../../../services/post.service';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
})
export class PostListComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('postTable') postTable: CTableComponent;

  public tableDefaultConfig = {
    useLocalTable: true,
    tableTitle: 'Danh sách bài viết',
    tableClass: 'table table-striped table-borderless mb-0',
    useSearch: true,
    searchOptions: {
      inputVisible: true,
      placeHolderText: 'Tìm kiếm theo tên',
    },
    pagingOptions: {
      showTotalRecords: true,
      pageSizes: [5, 10, 15, 20],
      pageSize: 10,
      currentPage: 1
    },
  };


  public posts = [];
  // tslint:disable-next-line:ban-types
  allChecked: Boolean = false;

  eventSubscription = null;

  tableOptions: CTableConfig = Object.assign({
    columnDefs: [
      {
        displayName: '',
        cellComponent: PostCheckColComponent,
        headerCellComponent: PostActionHeaderColComponent,
        cellHeaderClass: 'text-center'
      },
      {
        displayName: 'STT',
        field: 'no',
      },
      {
        displayName: 'Tiêu đề',
        field: 'post_title',
        cellClass: 'font-weight',
        cellComponent: PostNameColComponent,
      },
      {
        displayName: 'Nội dung',
        field: 'post_content',
        cellClass: 'font-weight',
      },
    ],
    extraData: {
      remove: () => {
        const listId = [];
        this.posts.forEach(obj => {
          if (obj.checkedAction) {
            listId.push(obj.post_id);
          }
        });

        if (!listId.length) {
          this.toastr.error('Vui lòng chọn bài viết để xóa', 'Error');
          return;
        }

        this.postService.deleteMultiPost({
          post_ids: listId,
        }).subscribe((e: any) => {
          this.postTable.refresh();
          this.allChecked = false;
          if (e.status === 0) {
            this.toastr.warning(e.message, 'Error');
          } else {
            this.toastr.success(e.message, 'Success');
          }
        });
      },
      publish: () => {
        const listId = [];
        this.posts.forEach(obj => {
          if (obj.checkedAction) {
            listId.push(obj.post_id);
          }
        });

        if (!listId.length) {
          this.toastr.error('Vui lòng chọn bài viết để xuất bản', 'Lỗi');
          return;
        }

        this.postService.publishPost({
          post_ids: listId,
          is_published: 1
        }).subscribe((e: any) => {
          this.postTable.refresh();
          this.allChecked = false;
          if (e.status === 0) {
            this.toastr.warning(e.message, 'Lỗi');
          } else {
            this.toastr.success(e.message, 'Thành công');
          }
        });
      },
      unpublish: () => {
          const listId = [];
          this.posts.forEach(obj => {
            if (obj.checkedAction) {
              listId.push(obj.post_id);
            }
          });

          if (!listId.length) {
            this.toastr.error('Vui lòng chọn bài viết để không xuất bản', 'Lỗi');
            return;
          }

          this.postService.unpublishPost({
            post_ids: listId,
            is_published: 0
          }).subscribe((e: any) => {
            this.postTable.refresh();
            this.allChecked = false;
            if (e.status === 0) {
              this.toastr.warning(e.message, 'Lỗi');
            } else {
              this.toastr.success(e.message, 'Thành công');
            }
          });
      },
      openDetails: (data) => {
        this.openPostModal(data);
      },
      setActionAll: () => {
        this.allChecked = !!!this.allChecked;
        this.posts.forEach(obj => {
          obj.checkedAction = this.allChecked;
        });
      },
      getAllCheckStatus: () => {
        return this.allChecked;
      }
    },
    refresh: (params, table) => {
      this.postService.getPostList().subscribe((res: any) => {
        if (res.status === 1 && res.data) {
          this.posts = res.data;
          let startNo = (params._page * 10) - 9;
          res.data.forEach((item: any) => {
            item.no = startNo;
            ++startNo ;
          });

          table.setData(res.data);
        }
      });
    }
  }, this.tableDefaultConfig);

  constructor(private postService: PostService,  private modalService: NgbModal,  private toastr: ToastrService) { }

  ngOnInit() {
    this.listPost();
    this.postService.subscribe(PostService.EVENT_REFRESH_POST_LIST, () => {
      this.listPost();
    });
  }

  ngAfterViewInit() {
    this.postTable.refresh();
  }

  listPost() {
    this.postService.getPostList()
    .subscribe(posts => {
        this.posts = posts.data;
    });
  }
  openPostModal(post = null) {
    const modalRef = this.modalService.open(PostFormComponent, {size: 'lg', centered: true, backdrop: 'static'});
    modalRef.componentInstance.model = post ? post : {};
  }

  ngOnDestroy() {
    this.postService.unsubscribe(PostService.EVENT_REFRESH_POST_LIST);
  }
}
