import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PostRoutingModule } from './post-routing.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { CoreModule } from 'src/app/core/core.module';
import { HttpModule } from '@angular/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { FormlyModule } from '@ngx-formly/core';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { PostFormComponent } from './post-form/post-form.component';
// tslint:disable-next-line:max-line-length
import { PostCheckColComponent, PostActionHeaderColComponent, PostActionColComponent} from './column/post-action-column.component';
import { PostNameColComponent } from './column/post-name-column.component';
import { PostListComponent } from './post-list/post-list.component';
import {CkeditorInputComponent} from '../../helpers/ckeditor-input';
import { CKEditorModule } from 'ngx-ckeditor';


@NgModule({
  // tslint:disable-next-line:max-line-length
  declarations: [
    PostFormComponent,
    PostListComponent,
    PostActionColComponent,
    PostCheckColComponent,
    PostActionHeaderColComponent,
    PostNameColComponent,
    CkeditorInputComponent,
  ],
  // tslint:disable-next-line:max-line-length
  entryComponents: [
    PostFormComponent,
    PostListComponent,
    PostActionColComponent,
    PostCheckColComponent,
    PostActionHeaderColComponent,
    PostNameColComponent
  ],
  imports: [
    CommonModule,
    PostRoutingModule,
    ReactiveFormsModule,
    BrowserModule,
    CoreModule,
    FormsModule,
    HttpModule,
    NgbModule,
    CKEditorModule,
    FormlyBootstrapModule,
    FormlyModule.forRoot({
      validationMessages: [
        { name: 'required', message: 'Trường này là bắt buộc' },
      ],
      types: [
        { name: 'ckeditor', component: CkeditorInputComponent, extends: 'textarea'},
      ]
    }),
    ConfirmationPopoverModule.forRoot(),
  ]
})
export class PostModule { }
