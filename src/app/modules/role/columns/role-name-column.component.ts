import { Component } from '@angular/core';
import { CTableCell } from '../../../core/ui/c-table/c-table-config';

@Component({
  selector: 'app-role-name-col',
  template: `
  <a style=" color: #005192 ;" (click)="extraData.openDetails(row)" title="{{row.entity.user_group_name}}">{{row.entity.user_group_name}}</a>
    `
})

export class RoleNameColComponent implements CTableCell {
  extraData;
  row;
  column;
}


