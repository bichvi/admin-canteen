import {Component, OnDestroy, OnInit} from '@angular/core';
import { FieldType } from '@ngx-formly/core';
import {filter, startWith, takeUntil} from 'rxjs/operators';
import {Subject } from 'rxjs';
import {debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';

@Component({
  selector: 'formly-multiselect',
  template: `
      <div class="form-group mb-0">
          <label>{{multiSelectOptions.label}}</label>
          <ng-container *ngIf="multiSelectOptions.specialSelect else basicSelect">
              <ng-select
                  [formControl]="formControl"
                  [formlyAttributes]="field"
                  [placeholder]="to.placeholder"
                  [items]="items$ | async"
                  [trackByFn]="trackByFn"
                  bindLabel="{{multiSelectOptions.bindLabel}}"
                  [addTag]="multiSelectOptions.addTag"
                  [multiple]="multiSelectOptions.multiple"
                  [maxSelectedItems]="multiSelectOptions.maxItem"
                  [hideSelected]="multiSelectOptions.hideSelected"
                  [typeahead]="search$"
                  [(ngModel)]="selectedItems">

                   <ng-template ng-multi-label-tmp let-items="items" let-clear="clear">
                       <div class="p-1 ng-value" *ngFor="let item of items">
                           <div class="d-flex justify-content-between">
                               <div class="pr-2 pl-1">
                                   <img class="mr-1 rounded-circle shadow bg-primary" width="25px" height="25px"
                                        [src]="(item.profileThumb) ? item.profileThumb : './assets/images/avatar-logo-small-o.png'"> {{item.englishName}}
                               </div>
                               <div class="align-self-center" (click)="clear(item)"><i class="icon-close icon-sm cursor-pointer" style="vertical-align: -3px;"></i></div>
                           </div>
                       </div>
                   </ng-template>
                   <ng-template ng-option-tmp let-item="item">
                       <div class="dropdown-item px-2">
                           <div class="d-flex justify-content-between">
                               <div>
                                   <img class="mr-1 rounded-circle shadow bg-primary" width="25px" height="25px"
                                        [src]="(item.profileThumb) ? item.profileThumb : './assets/images/avatar-logo-small-o.png'">
                                   <span title="{{item.englishName | cText}}">{{item.englishName | cText}}</span>
                               </div>
                               <div>
                                   <i title="{{item.title}}" class="{{item.disabled ? 'align-self-center icon-warning' : 'align-self-center fas fa-user-check'}}"></i>
                               </div>
                           </div>
                       </div>
                   </ng-template>
              </ng-select>

          </ng-container>

          <ng-template #basicSelect>
              <ng-select
                      [formControl]="formControl"
                      [formlyAttributes]="field"
                      [placeholder]="to.placeholder"
                      [items]="items$ | async"
                      [trackByFn]="trackByFn"
                      bindLabel="{{multiSelectOptions.bindLabel}}"
                      [addTag]="multiSelectOptions.addTag"
                      [multiple]="multiSelectOptions.multiple"
                      [maxSelectedItems]="multiSelectOptions.maxItem"
                      [hideSelected]="multiSelectOptions.hideSelected"
                      [typeahead]="search$"
                      [(ngModel)]="selectedItems">
                  <ng-template ng-option-tmp let-item="item">
                      <div class="dropdown-item px-2">
                          <div class="d-flex justify-content-between">
                              <div>
                                  <span title="{{item.username}}">{{item.username}}</span>
                              </div>
                          </div>
                      </div>
                  </ng-template>
              </ng-select>
          </ng-template>
      </div>
  `,
})
export class MultiSelectCheckboxComponent extends FieldType implements OnInit, OnDestroy {
  items$;
  search$ = new Subject<string>();
  selectedItems = [];
  onDestroy$ = new Subject<void>();
  field;
  model;
  max: number;

  // @ts-ignore
  multiSelectOptions = {
    addTag: false,
    label: 'label',
    maxItem: 100,
    multiple: true,
    specialSelect: false,
    bindLabel: 'englishName',
    hideSelected: true,
    bindValue: null,
  };

  constructor() {
    super();
  }

  trackByFn(item) {
    return item.id;
  }

  ngOnInit() {
    if (this.field.templateOptions.multiSelectOptions) {
      Object.assign(this.multiSelectOptions, this.field.templateOptions.multiSelectOptions);
    }

    this.items$ = this.search$.pipe(
      takeUntil(this.onDestroy$),
      startWith(''),
      filter(v => v !== null),
      debounceTime(200),
      distinctUntilChanged(),
      switchMap(this.to.search$),
    );
    this.items$.subscribe();
  }

  ngOnDestroy() {
    this.onDestroy$.complete();
  }
}
