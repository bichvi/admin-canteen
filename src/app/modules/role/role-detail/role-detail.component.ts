import {Component, OnInit, ViewChild, ElementRef, OnDestroy, AfterViewInit, Input} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { RoleService } from 'src/app/services/role.service';
import AppHelper from 'src/app/helpers/app.helper';
import { ToastrService } from 'ngx-toastr';
import {map, startWith, takeUntil, tap} from 'rxjs/operators';
import {Observable, Subject} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {CTableComponent} from '../../../core/ui/c-table/c-table.component';
import {CTableConfig} from '../../../core/ui/c-table/c-table-config';
import {UserActionHeaderColComponent, UserCheckColComponent} from '../../user/column/user-action-column.component';
import {UserImageColComponent} from '../../user/column/user-image-column.component';
import {UserNameColComponent} from '../../user/column/user-name-column.component';
import {UserStatusColComponent} from '../../user/column/user-status-column.component';
import {UserService} from '../../../services';

@Component({
  selector: 'app-role-form',
  templateUrl: './role-detail.component.html'
})
export class RoleDetailComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('roleTableAssign')
  roleTableAssign: CTableComponent;
  form = new FormGroup({});
  public id: number;
  @Input() model;
  public admins = [] as any;
  public role;
  onDestroy$ = new Subject<void>();
  options: FormlyFormOptions = {};
  fields: FormlyFieldConfig[] = [
    {
      fieldGroupClassName: 'row',
      fieldGroup: [
        {
          className: 'col-lg-12 col-12',
          type: 'input', key: 'user_group_name',
          templateOptions: {label: 'Tên nhóm user', placeholder: 'Nhập tên nhóm user', required: true},
        },
        // {
        //   className: 'col-xl-4 col-sm-6 col-12',
        //   key: 'user_admin_ids',
        //   type: 'multiselect',
        //   templateOptions: {
        //     multiSelectOptions: {
        //       label: 'User admin',
        //       bindLabel: 'username',
        //     },
        //     placeholder: 'Search admin',
        //     search$: (term: string = null): Observable<any[]> => {
        //       return this.roleService.getRole(this.role['user_group_id'])
        //         .pipe(
        //           map(
        //             admins => {
        //               this.admins.forEach(arrEmployee => {
        //                 // tslint:disable-next-line:max-line-length
        //                 admins = arrEmployee.admins.filter(admin => admin.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10);
        //                 }
        //               );
        //               return admins;
        //             }
        //           )
        //         );
        //     },
        //   },
        // }
      ],
    }
  ];
  public tableDefaultConfig = {
    useLocalTable: true,
    // data: this.rolePatternData,
    // tableTitle: 'List user',
    tableClass: 'table table-striped table-borderless mb-0 mt-2',
    // useSearch: true,
    // searchOptions: {
    //   inputVisible: true,
    //   placeHolderText: 'Search by name',
    // },
    pagingOptions: {
      showTotalRecords: true,
      pageSizes: [5, 10, 15, 20],
      pageSize: 10,
      currentPage: 1
    },
  };

  public users = [];
  tableOptions: CTableConfig = Object.assign({
    columnDefs: [
      {
        displayName: 'Tên tài khoản',
        field: 'username',
      },
      {
        displayName: 'Họ và tên',
        field: 'fullname',
      },
    ],
    refresh: (params, table) => {
      this.roleService.getRole(this.role['user_group_id']).subscribe((res: any) => {
        if (res.status === 1 && res.data) {
          this.admins = res.data;
          table.setData(res.data['user_admins'],res.data['user_admins'].length);
        }
      });
    }
  }, this.tableDefaultConfig);
  constructor(
    private roleService: RoleService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    // this.fields.splice(1, this.fields.length - 1);
    this.getPermisson();
    this.getRole();
    console.log(this.model);
    // this.role = AppHelper.deepClone(this.model);
    // console.log(this.role);
    // if (this.role.user_group_id) {
    //   this.listAdmin(this.role.user_group_id);
    //   // this.getRole(this.role.user_group_id);
    // }
    this.roleService.subscribe(RoleService.EVENT_REFRESH_ADMIN_LIST, (data) => {
      this.roleTableAssign.refresh();
    });
  }
  getPermisson() {
    this.roleService.getPermission().toPromise().then((res) => {
      if (Array.isArray(res['data'])) {
        this.fields = this.fields.concat(this.buildFields(res['data']));
      }
    });
  }

  getRole() {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.roleService.getRole(this.id).toPromise().then((res) => {
      if (res['data']) {
        this.model = res['data'];
        this.showPermission(this.model.permissions);
        // const result = this.mapDataUpdate(res['data']);
        // this.fields = this.fields.concat(this.buildFields(result.permissions));
      }
    });
  }
  createRole() {
    console.log(this.model);
    const params = this.mapData(this.role);
    const result = this.role.user_group_id !== 0 ?
        this.roleService.updateRole(params, this.role.user_group_id) :
        this.roleService.updateRole(params);
    result.toPromise().then((response) => {
        if (response.status === 1) {
          if (this.role.user_group_id !== 0) {
            Object.assign(this.model, this.role);
          }
          this.roleService.publish(RoleService.EVENT_REFRESH_ROLE_LIST, response.data);
          this.toastr.success(response.message, 'Thành công');
        } else {
          this.toastr.error(response.message);
        }
      });
    }
  private buildFields(permissions: Array<any>) {
    return Array.from(permissions, group => {
      return {
        type: 'multicheckbox',
        key: 'permissionData.' + group['module_id'],
        templateOptions: {
          label: group['module_name'] ? group['module_name'] : ' ',
          options: Array.from(group['actions'], item => {
            return {label: item['action_name'], value: item['action_id']};
          })
        },
        // hooks: {
        //   onInit: (field) => {
        //       this.roleService.getRole(group['module_id']).toPromise()
        //         .then((res) => {
        //           field['templateOptions'].options = Array.from(group['actions'], item => {
        //             const arr = res.data.permissions.filter(item1 => {
        //               return item1.module_id === group.module_id && item1.action_ids.find(f => f === item['action_id']);
        //             });
        //             return arr;
        //           });
        //           console.log(field.formControl.value);
        //           // field.formControl.setValue(field.model[field.key]);
        //         });
        //   },
        // },
      };
    });
  }

  private mapData(data) {
    if (data['permissionData']) {
      const arr = data['permissionData'].slice(1);
      return {
        user_group_id: data['user_group_id'],
        user_group_name: data['user_group_name'],
        permissions: Array.from(arr, (item, index) => {
          const obj = item !== null ? item : null;
          return {
            module_id: index + 1,
            actions: Array.from(Object.keys(obj), (item1, index1) => {
              return {action_id: index1 + 1, action_value: item1};
            }),
          };
        })
      };
    } else {
      return null;
    }
  }

  private mapDataUpdate(data) {
    return {
      user_group_id: data['user_group_id'],
      user_group_name: data['user_group_name'],
      permissions: Array.from(data['permissions'], (item, index) => {
        return {
          module_id: item['module_id'],
          actions: Array.from(item['action_ids'], (item1, index1) => {
            return {action_id: index1 + 1, action_value: item1};
          }),
        };
      })
    };
  }
  showPermission(data) {
    // const peopleArray = [
    //   { id: 123, name: 'dave', age: 23 },
    //   { id: 456, name: 'chris', age: 23 },
    //   { id: 789, name: 'bob', age: 23 },
    //   { id: 101, name: 'tom', age: 23 },
    //   { id: 102, name: 'tim', age: 23 }
    // ];
    const arrayToObject = (array) =>
      array.reduce((obj, item) => {
        const obj1 = item.action_ids.reduce((acc, cur, i) => {
          acc[i] = cur;
          return acc;
        }, {});
        Object.assign(true, obj1);
        obj[item.module_id] = obj1;
        return obj;
      }, {});
    const peopleObject = arrayToObject(data);
    console.log(peopleObject);
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  ngAfterViewInit() {
    if (this.roleTableAssign) {
      this.roleTableAssign.refresh();
    }
  }
}

