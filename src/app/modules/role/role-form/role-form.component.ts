import {Component, OnInit, ViewChild, ElementRef, OnDestroy, AfterViewInit, Input} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { RoleService } from 'src/app/services/role.service';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import AppHelper from 'src/app/helpers/app.helper';
import { ToastrService } from 'ngx-toastr';
import {map, startWith, takeUntil, tap} from 'rxjs/operators';
import {Observable, Subject} from 'rxjs';
import {Router} from '@angular/router';
import {CTableComponent} from '../../../core/ui/c-table/c-table.component';
import {CTableConfig} from '../../../core/ui/c-table/c-table-config';
import {UserActionHeaderColComponent, UserCheckColComponent} from '../../user/column/user-action-column.component';
import {UserImageColComponent} from '../../user/column/user-image-column.component';
import {UserNameColComponent} from '../../user/column/user-name-column.component';
import {UserStatusColComponent} from '../../user/column/user-status-column.component';
import {UserService} from '../../../services';

@Component({
  selector: 'app-role-form',
  templateUrl: './role-form.component.html'
})
export class RoleFormComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('roleTableAssign')
  roleTableAssign: CTableComponent;
  form = new FormGroup({});
  @Input() model;
  public admins = [] as any;
  public role;
  onDestroy$ = new Subject<void>();
  options: FormlyFormOptions = {};
  fields: FormlyFieldConfig[] = [
    {
      fieldGroupClassName: 'row',
      fieldGroup: [
        {
          className: 'col-xl-12 col-lg-12 col-12',
          type: 'input', key: 'user_group_name',
          templateOptions: {label: 'Tên nhóm user', placeholder: 'Nhập tên nhóm user', required: true},
        },
        // {
        //   className: 'col-xl-12 col-lg-12 col-12',
        //   type: 'multiselect',
        //   key: 'user_admin_ids',
        //   templateOptions: {
        //     multiSelectOptions: {
        //       bindLabel: 'username',
        //     },
        //     placeholder: 'Search admins',
        //     search$: (term: string = null): Observable<any[]> => {
        //       return this.roleService.getFreeAdmin()
        //         .pipe(
        //           map(
        //             res => {
        //               res.data.filter(v => v.username.indexOf(term.toLowerCase()) > -1).slice(0, 10);
        //               return res;
        //             }
        //           )
        //         );
        //     },
        //   },
        // },
      ],
    }
  ];
  public tableDefaultConfig = {
    useLocalTable: true,
    // data: this.rolePatternData,
    // tableTitle: 'List user',
    tableClass: 'table',
    // useSearch: true,
    // searchOptions: {
    //   inputVisible: true,
    //   placeHolderText: 'Search by name',
    // },
    pagingOptions: {
      showTotalRecords: true,
      pageSizes: [5, 10, 15, 20],
      pageSize: 10,
      currentPage: 1
    },
  };

  public users = [];
  tableOptions: CTableConfig = Object.assign({
    columnDefs: [
      {
        displayName: 'Tên tài khoản',
        field: 'username',
      },
      {
        displayName: 'Họ và tên',
        field: 'fullname',
      },
    ],
    refresh: (params, table) => {
      this.roleService.getRole(this.role['user_group_id']).subscribe((res: any) => {
        if (res.status === 1 && res.data) {
          this.admins = res.data;
          table.setData(res.data['user_admins'],res.data['user_admins'].length);
        }
      });
    }
  }, this.tableDefaultConfig);
  constructor(
    private roleService: RoleService,
    public activeModal: NgbActiveModal,
    private toastr: ToastrService,
    private router: Router,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.role = AppHelper.deepClone(this.model);
    if (this.role.id > 0 ) {
      this.roleTableAssign.refresh();
    }
    //   this.roleService.getRole(this.role.id).toPromise().then((res: any) => {
    //     if (res.Success && res.Data) {
    //       return res.Data;
    //     }
    //   }).then(data => {
    //     this.roleMembers = data;
    //     this.setTableData(data);
    //   });
    // }
  }
  // getPermisson() {
  //   this.roleService.getPermission().toPromise().then((res) => {
  //     if (Array.isArray(res['data'])) {
  //       this.fields = this.fields.concat(this.buildFields(res['data']));
  //       console.log(this.fields);
  //     }
  //   });
  // }
  createRole() {
    // console.log(this.model);
    const params = this.role;
    const result = this.role.user_group_id !== 0 ?
        this.roleService.updateRole(params, this.role.user_group_id) :
        this.roleService.updateRole(params);
    result.toPromise().then((response) => {
        if (response.status === 1) {
          if (this.role.user_group_id !== 0) {
            Object.assign(this.model, this.role);
          }
          this.roleService.publish(RoleService.EVENT_REFRESH_ROLE_LIST, response.data);
          this.toastr.success(response.message, 'Thành công');
        } else {
          this.toastr.error(response.message);
        }
        this.activeModal.close();
      });
    }
  // private buildFields(permissions: Array<any>) {
  //   return Array.from(permissions, group => {
  //     return {
  //       type: 'multicheckbox',
  //       key: 'permissionData.' + group['module_id'],
  //       templateOptions: {
  //         label: group['module_name'] ? group['module_name'] : ' ',
  //         options: Array.from(group['actions'], item => {
  //           return {label: item['action_name'], value: item['action_id']};
  //         })
  //       },
  //     };
  //   });
  // }

  // private mapData(data) {
  //   if (data['permissionData']) {
  //     const arr = data['permissionData'].slice(1);
  //     return {
  //       user_group_id: data['user_group_id'],
  //       user_group_name: data['user_group_name'],
  //       permissions: Array.from(arr, (item, index) => {
  //         const obj = item !== null ? item : null;
  //         return {
  //           module_id: index + 1,
  //           actions: Array.from(Object.keys(obj), (item1, index1) => {
  //             return {action_id: index1 + 1, action_value: item1};
  //           }),
  //         };
  //       })
  //     };
  //   } else {
  //     return null;
  //   }
  // }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  ngAfterViewInit() {
    if (this.roleTableAssign) {
      this.roleTableAssign.refresh();
    }
  }
}

