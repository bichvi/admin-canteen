import { Component, OnInit, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';
import { RoleService } from 'src/app/services/role.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RoleFormComponent } from '../role-form/role-form.component';
import { CTableComponent, CTableConfig } from 'src/app/core/ui/c-table/c-table';
// tslint:disable-next-line:max-line-length
import { RoleActionColComponent, RoleCheckColComponent, RoleActionHeaderColComponent } from '../columns/role-action-column.component';
import { RoleNameColComponent } from '../columns/role-name-column.component';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-role-list',
  templateUrl: './role-list.component.html'
})
export class RoleListComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('roleTable') roleTable: CTableComponent;
  actionList: any = [];
  roleList: any = [];
  result: any = {
    module_id: null,
    action_ids: [],
  };

  public tableDefaultConfig = {
    useLocalTable: true,
    tableTitle: 'Danh sách phân quyền',
    tableClass: 'table table-striped table-borderless mb-0',
    useSearch: true,
    searchOptions: {
      inputVisible: true,
      placeHolderText: 'Tìm kiếm theo tên',
    },
    pagingOptions: {
      showTotalRecords: true,
      pageSizes: [5, 10, 15, 20],
      pageSize: 10,
      currentPage: 1
    },
  };


  public roles = [];
  // tslint:disable-next-line:ban-types
  allChecked: Boolean = false;

  tableOptions: CTableConfig = Object.assign({
    columnDefs: [
      {
        displayName: '',
        cellComponent: RoleCheckColComponent,
        headerCellComponent: RoleActionHeaderColComponent,
      },
      {
        displayName: 'No',
        field: 'no',
      },
      {
        displayName: 'Role name',
        field: 'user_group_name',
        cellClass: 'font-weight',
        cellComponent: RoleNameColComponent,
      }
    ],
    extraData: {
      remove: () => {
          const listId = [];
          this.roles.forEach(obj => {
            if (obj.checkedAction) {
              listId.push(obj.user_group_id);
            }
          });

          if (!listId.length) {
            this.toastr.error('Please select role(s) to remove', 'Error');
            return;
          }

          this.roleService.deleteMultiRole({
            user_group_ids: listId,
          }).subscribe((e: any) => {
            this.roleTable.refresh();
            this.allChecked = false;
            this.toastr.success(e.message, 'Success');
          });
      },
      openDetails: (data) => {
        this.router.navigate(['/role/detail/' + data.entity['user_group_id']]).then(r => console.log(r));
      },
      setActionAll: () => {
        this.allChecked = !!!this.allChecked;
        this.roles.forEach(obj => {
          obj.checkedAction = this.allChecked;
        });
      },
      getAllCheckStatus: () => {
        return this.allChecked;
      }
    },
    refresh: (params, table) => {
      this.roleService.getRoleList().subscribe((res: any) => {
        if (res.status === 1 && res.data) {
          this.roles = res.data;
          let startNo = (params._page * 10) - 9;
          res.data.forEach((item: any) => {
            item.no = startNo;
            ++startNo ;
          });
          table.setData(res.data);
        }
      });
    }
  }, this.tableDefaultConfig);

  constructor(private roleService: RoleService,  private modalService: NgbModal,  private toastr: ToastrService, private router: Router) { }

  ngOnInit() {
    this.getActionList();
    this.getRoleList();
    this.roleService.subscribe(RoleService.EVENT_REFRESH_ROLE_LIST, () => {
      // this.roleTable.refresh();
    });
  }
  getRoleList() {
    this.roleService.getRoleList().toPromise().then((res) => {
      if (Array.isArray(res.data)) {
        this.roleList = res.data;
      }
    });
  }
  getActionList() {
    this.roleService.getPermission().toPromise().then((res) => {
      if (Array.isArray(res.data)) {
        this.actionList = res.data;
      }
    });
  }

  handleChecked(key) {
    const temp = [];
    this.roleList.forEach((role) => {
      if (Array.isArray(role.permissions)) {
        role.permissions.filter((action) => {
          if (Array.isArray(action.actions)) {
            action.actions.filter((item) => {
              temp.push({checkBoxId: role.user_group_id + '_' + action.module_id + '_' + item.action_id});
            });
          }
        });
      }
    });
    if (temp.find(f => f.checkBoxId === key)) {
      return true;
    }
  }
  ngAfterViewInit() {
    // this.roleTable.refresh();
  }
  openRoleModal(role = null) {
    // @ts-ignore
    const modalRef = this.modalService.open(RoleFormComponent, {size: 'lg', centered: true, backdrop: 'static', scrollable: true});
    modalRef.componentInstance.model = role ? role : {};
  }

  checkValue(roleId, moduleId, itemId, roleName, event: any) {
    console.log(event.target.value);
    const dataUpdatePermission = {
      module_id: moduleId,
      checked: event.target.checked,
      action_id: itemId
    };
    this.roleService.updateRolePermission(dataUpdatePermission, roleId).subscribe();
  }
  updateRole(role) {
    this.roleService.updateRole(role, role.user.user_group_id).subscribe();
  }

  ngOnDestroy() {
    this.roleService.unsubscribe(RoleService.EVENT_REFRESH_ROLE_LIST);
  }
}
