import { NgModule } from '@angular/core';

import { RoleRoutingModule } from './role-routing.module';
import { RoleListComponent } from './role-list/role-list.component';
import { RoleFormComponent } from './role-form/role-form.component';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { FormlyModule } from '@ngx-formly/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '../common/common.module';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { CoreModule } from 'src/app/core/core.module';
import { RoleActionColComponent, RoleCheckColComponent, RoleActionHeaderColComponent } from './columns/role-action-column.component';
import { RoleNameColComponent } from './columns/role-name-column.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import {FormlySelectModule} from '@ngx-formly/core/select';
import {RoleDetailComponent} from './role-detail/role-detail.component';
import {FormlyMultiCheckboxComponent} from './multicheckbox.type';
import {MultiSelectCheckboxComponent} from './multiselect.type';
import { NgSelectModule } from '@ng-select/ng-select';
@NgModule({
  // tslint:disable-next-line:max-line-length
  declarations: [RoleListComponent, RoleFormComponent, RoleDetailComponent, RoleActionColComponent, RoleNameColComponent, RoleCheckColComponent, RoleActionHeaderColComponent, FormlyMultiCheckboxComponent, MultiSelectCheckboxComponent],
  // tslint:disable-next-line:max-line-length
  entryComponents: [RoleListComponent, RoleFormComponent, RoleDetailComponent, RoleActionColComponent, RoleNameColComponent, RoleCheckColComponent, RoleActionHeaderColComponent, FormlyMultiCheckboxComponent, MultiSelectCheckboxComponent],
  imports: [
    RoleRoutingModule,
    CommonModule,
    BrowserModule,
    CoreModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    NgbModule,
    FormlyBootstrapModule,
    NgSelectModule,
    FormlyModule.forRoot({
      validationMessages: [
        {name: 'required', message: 'Trường này là bắt buộc'},
      ],
      types: [
        {name: 'multicheckbox', component: FormlyMultiCheckboxComponent},
        {name: 'multiselect', component: MultiSelectCheckboxComponent},
      ],
    }),
    ConfirmationPopoverModule.forRoot(),
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(),
    FormlySelectModule,
    // ToastrModule added
  ]
})
export class RoleModule { }
