import {Component} from '@angular/core';
import { CTableCell } from '../../../core/ui/c-table/c-table-config';
import {UserService} from '../../../services/user.service';
import AppHelper from '../../../helpers/app.helper';

@Component({
  selector: 'app-user-user-col',
  template: `
  <a title="{{getUser(row.entity.user_admin_id)}}">{{row.entity.user_id}}</a>
    `
})

export class UserColComponent implements CTableCell {
  extraData;
  row;
  column;
  constructor(private userService: UserService) { }
  getUser(id: number) {
    this.userService.getUser(id).toPromise()
      .then(res => {
      return res.data.username;
    });
  }
}


