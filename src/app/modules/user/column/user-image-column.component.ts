import { Component } from '@angular/core';
import { CTableCell } from '../../../core/ui/c-table/c-table-config';

@Component({
  selector: 'app-image-image-col',
  template: `
      <img class="rounded-circle shadow" height="38px" width="38px"
           src="{{row.entity.avatar ? link +  row.entity.avatar : \'./assets/assets/images/avatar-default-icon.png\'}}"
           onError="this.src='./assets/assets/images/avatar-default-icon.png';">
  `
})

export class UserImageColComponent implements CTableCell {
  extraData;
  row;
  column;
  link = 'https://uit-canteen.s3-ap-southeast-1.amazonaws.com';
}


