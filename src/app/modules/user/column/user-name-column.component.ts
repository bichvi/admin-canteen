import { Component } from '@angular/core';
import { CTableCell } from '../../../core/ui/c-table/c-table-config';

@Component({
  selector: 'app-user-name-col',
  template: `
  <a style=" color: #005192 ;" (click)="extraData.openDetails(row.entity)" title="{{row.entity.username}}">{{row.entity.username}}</a>
    `
})

export class UserNameColComponent implements CTableCell {
  extraData;
  row;
  column;
}


