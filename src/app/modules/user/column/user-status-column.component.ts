import {AfterViewChecked, AfterViewInit, Component, OnInit} from '@angular/core';
import { CTableCell } from '../../../core/ui/c-table/c-table-config';

@Component({
  selector: 'app-user-status-col',
  template: `
      <span title="{{row['entity'][column.field] | userStatus}}">
          <i class="{{getClassName(row['entity'][column.field])}} fa-lg"></i>
      </span>
    <div class="progress position-relative overflow-visible d-none">
      <div class="progress-bar user-status {{getClassName(row['entity'][column.field])}}" style="width: 25%"></div>
      <span class="position-absolute h6 font-italic font-weight-normal">{{row['entity'][column.field] | userStatus}}</span>
    </div>
  `
})

export class UserStatusColComponent implements CTableCell {
  extraData;
  row;
  column;

  constructor() {
  }

  getClassName(input) {
    let className = 'fa fa-user';
    if (input === 0) {
      className = 'fa fa-user-times';
    }
    return className;
  }
}
