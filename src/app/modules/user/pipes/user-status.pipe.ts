import { Pipe, PipeTransform } from '@angular/core';


/**
 * Pipe User Status
 */
@Pipe({name: 'userStatus'})
export class UserStatusPipe implements PipeTransform {
  transform(input: any): string {
    input = (input === 0) ? 'Inactive' : 'Active';
    return input;
  }
}
