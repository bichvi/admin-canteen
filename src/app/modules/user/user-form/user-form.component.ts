import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { UserService } from 'src/app/services/user.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import AppHelper from 'src/app/helpers/app.helper';
import {CategoryService} from '../../../services/category.service';
import {TemplateOptionsConstants} from '../../../core/constans/template-options-constants';
import {startWith, takeUntil, tap} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {ToastrService} from 'ngx-toastr';
import {RoleService} from '../../../services/role.service';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html'
})
export class UserFormComponent implements OnInit, OnDestroy {

  form = new FormGroup({});
  model: any = {};
  public user;
  public imageSrc: string;
  onDestroy$ = new Subject<void>();
  options: FormlyFormOptions = {};
  // @ts-ignore
  // @ts-ignore
  fields: FormlyFieldConfig[] = [
    {
      fieldGroupClassName: 'row',
      fieldGroup: [
        {
          className: 'col-xl-4 col-lg-6 col-12',
          type: 'input',
          key: 'username',
          templateOptions: {
            label: 'Tên tài khoản',
            placeholder: 'Nhập tên tài khoản',
            required: true
          },
        },
        {
          className: 'col-xl-4 col-lg-6 col-12',
          type: 'input',
          key: 'email',
          templateOptions: {
            type: 'email',
            label: 'Email',
            placeholder: 'Nhập email',
            required: true
          },
        },
        {
          className: 'col-xl-4 col-lg-6 col-12',
          type: 'input',
          key: 'fullname',
          templateOptions: {
            label: 'Họ và tên',
            placeholder: 'Nhập họ và tên',
            required: true
          },
        },
        {
          className: 'col-xl-4 col-lg-6 col-12',
          type: 'select',
          key: 'user_group_id',
          templateOptions: {
            label: 'Nhóm quyền',
            placeholder: 'Chọn nhóm quyền',
            required: true,
            options: [],
          },
          hooks: {
            onInit: (field) => {
              this.roleService.getRoleList().toPromise()
                  .then((res) => {
                    field['templateOptions'].options = Array.from(res.data, item => {
                      // this.category.push(item);
                      return {label: item['user_group_name'], value: item['user_group_id']};
                    });
                    field.formControl.setValue(field.model[field.key]);
                  });
            },
            onChanges: (field) => {
              field.formControl.valueChanges.pipe(
                  takeUntil(this.onDestroy$),
                  startWith(field.formControl.value),
                  tap(value => {
                  }),
              ).subscribe();
            },
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'row',
      fieldGroup: [
        {
          className: 'col-xl-4 col-lg-6 col-12',
          type: 'input',
          key: 'password',
          templateOptions: {
            type: 'password',
            label: 'Mật khẩu',
            placeholder: 'Mật khẩu có ít nhất 3 kí tự',
            required: true,
            minLength: 3
          },
          hideExpression: () => {
            return this.user.user_admin_id > 0;
          }
        },
        {
          className: 'col-xl-4 col-lg-6 col-12',
          type: 'input', key: 'passwordConfirm',
          templateOptions: {
            type: 'password',
            label: 'Nhập lại mật khẩu',
            placeholder: 'Nhập lại mật khẩu',
            minLength: 3
          },
          validators: {
            fieldMatch: {
              expression: (control) => control.value === this.model.password,
              message: 'Mật khẩu không khớp',
            },
          },
          expressionProperties: {
            'templateOptions.disabled': () => this.form.get('password') && !this.form.get('password').valid,
          },
          lifecycle: {
            onInit: (form, field) => {
              // @ts-ignore
              if (this.form.get('password') !== null) {
                form.get('password').valueChanges.pipe(
                  takeUntil(this.onDestroy$),
                  tap(() => {
                    field.formControl.updateValueAndValidity();
                  })
                ).subscribe();
              }
            }
          },
          hideExpression: () => {
            return this.user.user_admin_id > 0;
          }
        },
        // {
        //   className: 'col-xl-4 col-lg-6 col-12',
        //   key: 'is_active',
        //   type: 'radio',
        //   defaultValue: 0,
        //   templateOptions: {
        //     type: 'radio',
        //     label: 'Trạng thái',
        //     options: TemplateOptionsConstants.userStatusData
        //   }
        // },
      ],
    }
  ];
  constructor(
    private userService: UserService,
    private roleService: RoleService,
    public activeModal: NgbActiveModal,
    private toastr: ToastrService) { }

  ngOnInit() {
    if (this.model.avatar) {
      this.imageSrc = 'https://uit-canteen.s3-ap-southeast-1.amazonaws.com' + this.model.avatar;
      // this.model.avatar =  this.imageSrc;
    }
    this.user = AppHelper.deepClone(this.model);
  }

  handleInputChange(e) {
    const file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    const pattern = /image-*/;
    const reader = new FileReader();
    if (!file.type.match(pattern)) {
      this.toastr.error('Sai định dạng', 'Lỗi');
      return;
    }
    reader.onload = this._handleReaderLoaded.bind(this);
    reader.readAsDataURL(file);
  }
  _handleReaderLoaded(e) {
    const reader = e.target;
    // this.imageSrc = reader.result;
    // this.model.avatar =  this.imageSrc;
    this.imageSrc = reader.result;
    this.model.image_data = reader.result;
  }

  createUser() {
    if (this.form.valid) {
      const result = this.user.user_admin_id > 0 ?
        this.userService.updateUser(this.model, this.user.user_admin_id) :
        this.userService.createUser(this.model);
      result.toPromise().then((response) => {
        if (response.status === 1) {
          this.activeModal.close();
          this.userService.getUserList();
          this.toastr.success(response.message, 'Thành công');
          this.userService.publish(UserService.EVENT_REFRESH_USER_LIST);
          this.options.resetModel();
        } else {
          this.toastr.error(response.message, 'Lỗi', {
            enableHtml: true
          });
        }
      });
    }
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
