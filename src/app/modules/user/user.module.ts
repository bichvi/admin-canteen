import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {UserActionColComponent, UserActionHeaderColComponent, UserCheckColComponent} from './column/user-action-column.component';
import {UserNameColComponent} from './column/user-name-column.component';
import {UserColComponent} from './column/user-column.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {CoreModule} from '../../core/core.module';
import {HttpModule} from '@angular/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormlyBootstrapModule} from '@ngx-formly/bootstrap';
import {FormlyModule} from '@ngx-formly/core';
import {ConfirmationPopoverModule} from 'angular-confirmation-popover';
import {UserRoutingModule} from './user-routing.module';
import {UserListComponent} from './user-list/user-list.component';
import {UserFormComponent} from './user-form/user-form.component';
import {UserImageColComponent} from './column/user-image-column.component';
import {UserStatusPipe} from './pipes/user-status.pipe';
import {UserStatusColComponent} from './column/user-status-column.component';

@NgModule({
  // tslint:disable-next-line:max-line-length
  declarations: [
    UserListComponent,
    UserFormComponent,
    UserActionColComponent,
    UserNameColComponent,
    UserActionHeaderColComponent,
    UserCheckColComponent,
    UserColComponent,
    UserImageColComponent,
    UserStatusPipe,
    UserStatusColComponent
  ],
  // tslint:disable-next-line:max-line-length
  entryComponents: [
    UserListComponent,
    UserFormComponent,
    UserActionColComponent,
    UserNameColComponent,
    UserActionHeaderColComponent,
    UserCheckColComponent,
    UserColComponent,
    UserImageColComponent,
    UserStatusColComponent
  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    ReactiveFormsModule,
    BrowserModule,
    CoreModule,
    FormsModule,
    HttpModule,
    NgbModule,
    FormlyBootstrapModule,
    FormlyModule.forRoot({
      validationMessages: [
        { name: 'required', message: 'Trường này là bắt buộc' },
      ],
    }),
    ConfirmationPopoverModule.forRoot(),
  ],
  exports: [
    UserStatusPipe
  ]
})
export class UserModule { }
