import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../models/user';
import { environment } from 'src/environments/environment';
import {Response} from '@angular/http';
import {EventService} from './event.service';

@Injectable({ providedIn: 'root' })
export class AuthenticationService  extends EventService {
  public static EVENT_RESET_PASSWORD = 'password:reset';
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor(private http: HttpClient) {
    super();
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
    //   return JSON.parse(localStorage.getItem('currentUser'));
  }

  login(username: string, password: string) {
    return this.http.post<any>(`${environment.apiUrl}/login`, { username, password })
      .pipe(map(user => {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.currentUserSubject.next(user);
        return user;
      }));
  }
  authenticationLogin(username: string) {
    return this.http.post<any>(`${environment.apiUrl}/user-admin/forgot-password`, { username})
      .pipe(map(user => {
        return user;
      }));
    // return this.http.post(`${environment.apiUrl}/user-admin/forgot-password`, {username})
    //   .pipe(map((response: Response) => response.json()));
  }

  resetPass(accessToken: string, newPassword: string) {
    return this.http.post<any>(`${environment.apiUrl}/user-admin/reset-password`, { accessToken, newPassword})
      .pipe(map(user => {
        return user;
      }));
    // return this.http.post(`${environment.apiUrl}/user-admin/forgot-password`, {username})
    //   .pipe(map((response: Response) => response.json()));
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }
}
