import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { environment } from 'src/environments/environment';
import { EventService } from './event.service';

@Injectable({ providedIn: 'root' })
export class BankService extends EventService {

    public static EVENT_REFRESH_BANK_LIST = 'group:add';
    constructor(private http: Http) {
        super();
    }

    getBankList() {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
        let options = new RequestOptions({ headers: headers });
        return this.http.get(`${environment.apiBankServiceUrl}/bank/list`, options)
            .pipe(map((response: Response) => response.json()));
    }

    getBank(id: number) {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
        let options = new RequestOptions({ headers: headers });
        return this.http.get(`${environment.apiBankServiceUrl}/bank/${id}`, options)
            .pipe(map((response: Response) => response.json()));
    }

    updateBank(data: any, id = 0) {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(`${environment.apiBankServiceUrl}/bank/${id}/form`, data, options)
            .pipe(map((response: Response) => response.json()));
    }

    deleteBank(id: number) {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
        let options = new RequestOptions({ headers: headers });
        return this.http.delete(`${environment.apiBankServiceUrl}/bank/${id}`, options)
            .pipe(map((response: Response) => response.json()));
    }

    deleteMultiBank(data: any) {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(`${environment.apiBankServiceUrl}/bank/delete/multiple`, data, options)
            .pipe(map((response: Response) => response.json()));
    }
}
