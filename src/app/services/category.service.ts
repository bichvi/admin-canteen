import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { environment } from 'src/environments/environment';
import { EventService } from './event.service';

@Injectable({ providedIn: 'root' })
export class CategoryService extends EventService{

    public static EVENT_REFRESH_CATEGORY_LIST = 'group:add';
    constructor(private http: Http) {
      super();
    }

    getCategoryList() {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
        let options = new RequestOptions({ headers: headers });
        return this.http.get(`${environment.apiUrl}/food-category/list`, options)
            .pipe(map((response: Response) => response.json()));
    }

  getCategory(id: number) {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
    let options = new RequestOptions({ headers: headers });
    return this.http.get(`${environment.apiUrl}/food-category/${id}`, options)
      .pipe(map((response: Response) => response.json()));
  }

    updateCategory(data: any, id = 0) {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(`${environment.apiUrl}/food-category/${id}/form`, data, options)
      .pipe(map((response: Response) => response.json()));
  }

    deleteCategory(id: number) {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
        let options = new RequestOptions({ headers: headers });
        return this.http.delete(`${environment.apiUrl}/food-category/${id}`, options)
            .pipe(map((response: Response) => response.json()));
    }

    deleteMultiCategory(data: any) {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(`${environment.apiUrl}/food-category/delete/multiple`, data, options)
            .pipe(map((response: Response) => response.json()));
      }
}
