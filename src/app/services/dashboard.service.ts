import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { environment } from 'src/environments/environment';
import { EventService } from './event.service';

@Injectable({ providedIn: 'root' })
export class DashboardService extends EventService {

    public static EVENT_REFRESH_DASHBOARD_LIST = 'group:add';
    constructor(private http: Http) {
      super();
    }

  getTopFoods(data = null) {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
        let options = new RequestOptions({ headers: headers });
        return this.http.get(`${environment.apiUrl}/report/top-food?type=${data.type}&from=${data.from}&to=${data.to}`, options)
            .pipe(map((response: Response) => response.json()));
    }

  getTopCustomers(data = null) {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
    let options = new RequestOptions({ headers: headers });
    return this.http.get(`${environment.apiUrl}/report/top-customer?type=${data.type}&from=${data.from}&to=${data.to}`, options)
      .pipe(map((response: Response) => response.json()));
  }

    updateDashboard(data: any, id = 0) {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(`${environment.apiUrl}/dashboard/${id}/form`, data, options)
      .pipe(map((response: Response) => response.json()));
  }

    deleteDashboard(id: number) {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
        let options = new RequestOptions({ headers: headers });
        return this.http.delete(`${environment.apiUrl}/dashboard/${id}`, options)
            .pipe(map((response: Response) => response.json()));
    }

    deleteMultiDashboard(data: any) {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(`${environment.apiUrl}/dashboard/delete/multiple`, data, options)
            .pipe(map((response: Response) => response.json()));
      }
      loadPieChart(data = null) {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
        let options = new RequestOptions({ headers: headers });
        return this.http.get(`${environment.apiUrl}/report/pie-chart?type=${data.type}&from=${data.from}&to=${data.to}`, options)
          .pipe(map((response: Response) => response.json()));
      }
      loadBarChart(data = null) {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
        let options = new RequestOptions({ headers: headers });
        return this.http.get(`${environment.apiUrl}/report/bar-chart?type=${data.type}&from=${data.from}&to=${data.to}`, options)
          .pipe(map((response: Response) => response.json()));
      }
  loadMonitoringBarChart(data = null) {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
    let options = new RequestOptions({ headers: headers });
    return this.http.get(`${environment.apiUrl}/monitoring/bar-chart?month=${data.month}`, options)
      .pipe(map((response: Response) => response.json()));
  }
  getDetailMonitoring(data = null) {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
    let options = new RequestOptions({ headers: headers });
    return this.http.get(`${environment.apiUrl}/monitoring/detail?month=${data.month}`, options)
      .pipe(map((response: Response) => response.json()));
  }
    loadStatistics(data = null) {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
    let options = new RequestOptions({ headers: headers });
    return this.http.get(`${environment.apiUrl}/report/statistics?type=${data.type}&from=${data.from}&to=${data.to}`, options)
      .pipe(map((response: Response) => response.json()));
  }
  createMonitoring(data: any, id = 0) {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(`${environment.apiUrl}/monitoring/${id}/form`, data, options)
      .pipe(map((response: Response) => response.json()));
  }
}
