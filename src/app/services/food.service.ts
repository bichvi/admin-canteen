import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { environment } from 'src/environments/environment';
import { EventService } from './event.service';

@Injectable({ providedIn: 'root' })
export class FoodService extends EventService{

    public static EVENT_REFRESH_FOOD_LIST = 'food:refresh';
    public static EVENT_CHANGE_FOOD_LIST = 'food:change';
    public static EVENT_REFRESH_FOOD_TYPE_LIST = 'food-type:refresh';
    constructor(private http: Http) {
        super();
    }

    getFoodList(data: any) {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
        let options = new RequestOptions({ headers: headers });
        // @ts-ignore
      return this.http.get(`${environment.apiUrl}/food/list/${data}`, options)
            .pipe(map((response: Response) => response.json()));
    }

    updateFood(data: any, id = 0) {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(`${environment.apiUrl}/food/${id}/form`, data, options)
            .pipe(map((response: Response) => response.json()));
    }

    publishMultiFood(data: any) {
      let currentUser = JSON.parse(localStorage.getItem('currentUser'));
      let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
      let options = new RequestOptions({ headers: headers });
      return this.http.post(`${environment.apiUrl}/food/publish`, data, options)
        .pipe(map((response: Response) => response.json()));
    }
    deleteMultiFood(data: any) {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(`${environment.apiUrl}/food/delete/multiple`, data, options)
            .pipe(map((response: Response) => response.json()));
      }

      getFoodTypeList() {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
        let options = new RequestOptions({ headers: headers });
        return this.http.get(`${environment.apiUrl}/food-type/list`, options)
            .pipe(map((response: Response) => response.json()));
    }

    updateFoodType(data: any, id = 0) {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(`${environment.apiUrl}/food-type/${id}/form`, data, options)
            .pipe(map((response: Response) => response.json()));
    }

    deleteMultiFoodType(data: any) {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(`${environment.apiUrl}/food-type/delete/multiple`, data, options)
            .pipe(map((response: Response) => response.json()));
      }
}
