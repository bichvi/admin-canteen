import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { environment } from 'src/environments/environment';
import { EventService } from './event.service';

@Injectable({ providedIn: 'root' })
export class MaterialService extends EventService{

    public static EVENT_REFRESH_MATERIAL_LIST = 'material:refresh';
    public static EVENT_CHANGE_MATERIAL_LIST = 'material:change';
    public static EVENT_REFRESH_MATERIAL_TYPE_LIST = 'material-type:refresh';
    constructor(private http: Http) {
        super();
    }

    getMaterialList() {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
        let options = new RequestOptions({ headers: headers });
        // @ts-ignore
        return this.http.get(`${environment.apiUrl}/material/list`, options)
            .pipe(map((response: Response) => response.json()));
    }

    updateMaterial(data: any, id = 0) {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(`${environment.apiUrl}/material/${id}/form`, data, options)
            .pipe(map((response: Response) => response.json()));
    }

    deleteMultiMaterial(data: any) {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(`${environment.apiUrl}/material/delete/multiple`, data, options)
            .pipe(map((response: Response) => response.json()));
      }

      getMaterialTypeList() {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
        let options = new RequestOptions({ headers: headers });
        return this.http.get(`${environment.apiUrl}/material-type/list`, options)
            .pipe(map((response: Response) => response.json()));
    }

    updateMaterialType(data: any, id = 0) {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(`${environment.apiUrl}/material-type/${id}/form`, data, options)
            .pipe(map((response: Response) => response.json()));
    }

    deleteMultiMaterialType(data: any) {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(`${environment.apiUrl}/material-type/delete/multiple`, data, options)
            .pipe(map((response: Response) => response.json()));
      }
}
