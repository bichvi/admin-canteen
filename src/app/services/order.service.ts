import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { environment } from 'src/environments/environment';
import { EventService } from './event.service';

@Injectable({ providedIn: 'root' })
export class OrderService extends EventService {

    public static EVENT_REFRESH_ORDER_LIST = 'order:refresh';
    public static EVENT_CHANGE_ORDER_LIST = 'order:change';
    public static EVENT_REFRESH_ORDER_TYPE_LIST = 'order-type:refresh';

    public static EVENT_ORDER_MOVE = 'order:move';
    public static EVENT_ORDER_PUSH = 'order:push';

    public static EVENT_PENDING_ORDER_MOVE = 'pending-order:move';
    public static EVENT_PENDING_ORDER_PUSH = 'pending-order:push';

    public static EVENT_DONE_ORDER_MOVE = 'done-order:move';
    public static EVENT_DONE_ORDER_PUSH = 'done-order:push';

    constructor(private http: Http) {
        super();
    }

    getWaitingList() {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
        let options = new RequestOptions({ headers: headers });
        return this.http.get(`${environment.apiUrl}/order/waiting-list`, options)
            .pipe(map((response: Response) => response.json()));
    }

  getPendingList() {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
    let options = new RequestOptions({ headers: headers });
    return this.http.get(`${environment.apiUrl}/order/pending-list`, options)
      .pipe(map((response: Response) => response.json()));
  }

  getDoneList() {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
    let options = new RequestOptions({ headers: headers });
    // @ts-ignore
    return this.http.get(`${environment.apiUrl}/order/done-list`, options)
      .pipe(map((response: Response) => response.json()));
  }

    forceFinishOrder(data: any) {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(`${environment.apiUrl}/order/force-finish`, data, options)
            .pipe(map((response: Response) => response.json()));
    }
}
