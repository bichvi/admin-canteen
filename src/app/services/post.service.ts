import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { environment } from 'src/environments/environment';
import { EventService } from './event.service';

@Injectable({ providedIn: 'root' })
export class PostService extends EventService {

    public static EVENT_REFRESH_POST_LIST = 'post:add';
    constructor(private http: Http) {
      super();
    }

    getPostList() {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
        let options = new RequestOptions({ headers: headers });
        return this.http.get(`${environment.apiUrl}/post/list`, options)
            .pipe(map((response: Response) => response.json()));
    }

  getPost(id: number) {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
    let options = new RequestOptions({ headers: headers });
    return this.http.get(`${environment.apiUrl}/post/${id}`, options)
      .pipe(map((response: Response) => response.json()));
  }

    updatePost(data: any, id = 0) {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(`${environment.apiUrl}/post/${id}/form`, data, options)
      .pipe(map((response: Response) => response.json()));
  }

    deletePost(id: number) {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
        let options = new RequestOptions({ headers: headers });
        return this.http.delete(`${environment.apiUrl}/post/${id}`, options)
            .pipe(map((response: Response) => response.json()));
    }

    deleteMultiPost(data: any) {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(`${environment.apiUrl}/post/delete/multiple`, data, options)
            .pipe(map((response: Response) => response.json()));
      }

    publishPost(data: any) {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(`${environment.apiUrl}/post/publish`, data, options)
            .pipe(map((response: Response) => response.json()));
    }

    unpublishPost(data: any) {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(`${environment.apiUrl}/post/publish`, data, options)
            .pipe(map((response: Response) => response.json()));
    }
}
