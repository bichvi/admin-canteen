import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { environment } from 'src/environments/environment';
import { EventService } from './event.service';

@Injectable({ providedIn: 'root' })
export class RoleService extends EventService {

    public static EVENT_REFRESH_ROLE_LIST = 'role:list';
    public static EVENT_REFRESH_ADMIN_LIST = 'admin:list';
    constructor(private http: Http) {
        super();
    }

    getRoleList() {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
        let options = new RequestOptions({ headers: headers });
        return this.http.get(`${environment.apiUrl}/user-group/list`, options)
            .pipe(map((response: Response) => response.json()));
    }

    getRole(id: number) {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
        let options = new RequestOptions({ headers: headers });
        return this.http.get(`${environment.apiUrl}/user-group/${id}/detail`, options)
            .pipe(map((response: Response) => response.json()));
    }
    getPermission() {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
        let options = new RequestOptions({ headers: headers });
        return this.http.get(`${environment.apiUrl}/user-group/all-permissions`, options)
            .pipe(map((response: Response) => response.json()));
    }

    updateRole(data: any, id = 0) {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(`${environment.apiUrl}/user-group/${id}/form`, data, options)
            .pipe(map((response: Response) => response.json()));
    }

    updateRolePermission(data: any, id = 0) {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(`${environment.apiUrl}/user-group/${id}/permission`, data, options)
            .pipe(map((response: Response) => response.json()));
    }

    deleteRole(id: number) {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
        let options = new RequestOptions({ headers: headers });
        return this.http.delete(`${environment.apiUrl}/user-group/${id}`, options)
            .pipe(map((response: Response) => response.json()));
    }

    deleteMultiRole(data: any) {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(`${environment.apiUrl}/user-group/delete/multiple`, data, options)
            .pipe(map((response: Response) => response.json()));
    }

    getFreeAdmin() {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
        let options = new RequestOptions({ headers: headers });
        return this.http.get(`${environment.apiUrl}/user-group/free-admins`, options)
            .pipe(map((response: Response) => response.json()));
    }
}
