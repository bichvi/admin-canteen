import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { User } from '../models/user';
import { AuthenticationService } from './authentication.service';
import { environment } from 'src/environments/environment';
import {EventService} from './event.service';

@Injectable({ providedIn: 'root' })
export class UserService extends EventService {
  public static EVENT_REFRESH_USER_LIST = 'user:refresh';
  constructor(
        private http: Http) {
      super();
    }
    getUserInfo(): Observable<User> {
        // add authorization header with jwt token
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
        let options = new RequestOptions({ headers: headers });

        // get users from api
        return this.http.get(`${environment.apiUrl}/user-admin/info`, options)
            .pipe(map((response: Response) => response.json()));
    }

  getUser(id) {
    // add authorization header with jwt token
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    console.log(currentUser);
    let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
    let options = new RequestOptions({ headers: headers });

    // get users from api
    return this.http.get(`${environment.apiUrl}/user-admin/${id}`, options)
      .pipe(map((response: Response) => response.json()));
  }


  getUserList() {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
    let options = new RequestOptions({ headers: headers });
    return this.http.get(`${environment.apiUrl}/user-admin/list`, options)
      .pipe(map((response: Response) => response.json()));
  }
  updateUser(data: any, id) {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(`${environment.apiUrl}/user-admin/update/${id}`, data, options)
      .pipe(map((response: Response) => response.json()));
  }

  createUser(data: any) {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(`${environment.apiUrl}/user-admin/create`, data, options)
      .pipe(map((response: Response) => response.json()));
  }

  updateStatus(data: any) {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(`${environment.apiUrl}/user-admin/status`, data, options)
        .pipe(map((response: Response) => response.json()));
  }
}
