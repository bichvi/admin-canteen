export const environment = {
  production: true,
  apiUrl: 'https://htu7qum9nd.execute-api.ap-southeast-1.amazonaws.com/uit-canteen/canteen-service/api/admin',
  apiBankServiceUrl: 'https://htu7qum9nd.execute-api.ap-southeast-1.amazonaws.com/uit-canteen/bank-service/api/admin'
};
